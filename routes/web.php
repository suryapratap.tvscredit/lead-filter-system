<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\FreeDoorStepController;
use App\Http\Controllers\PersonalLoanController;
use App\Http\Controllers\FrontBlogController;

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubcategoryController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\SeriesController;
use App\Http\Controllers\Admin\ModelController;
use App\Http\Controllers\Admin\ImpelmentsController;
use App\Http\Controllers\Admin\HoursMasterController;
use App\Http\Controllers\Admin\PageController;

use App\Http\Controllers\Admin\OwnerMasterController;


use App\Http\Controllers\Admin\LocationMasterController;
use App\Http\Controllers\Admin\LeadController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BlogCategoryController;
use App\Http\Controllers\Admin\StateMasterController;
use App\Http\Controllers\Admin\DistrictMasterController;
use App\Http\Controllers\Admin\BrandMasterErrController;
use App\Http\Controllers\Admin\StateDistrictBrandMappingController;


// use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});
Auth::routes();
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::group(['middleware' => 'role'], function () {
        Route::get('/', [UserController::class, 'home'])->name('admin.dashboard');
        Route::get('/user', [UserController::class, 'list'])->name('admin.usermanagement');
        Route::get('/users/add', [UserController::class, 'add'])->name('users.add');
        Route::post('/users/create', [UserController::class, 'create'])->name('users.create');
        Route::get('/users/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
        Route::post('/users/update', [UserController::class, 'update'])->name('users.update');
        Route::get('/users/delete/{id}', [UserController::class, 'delete'])->name('users.delete');

        //Dumploeads
        //

        Route::get('/upload-dump', [LeadController::class, 'uploaddump'])->name('uploaddump');

        Route::get('/filter-data', [LeadController::class, 'filterData'])->name('filterData');
        Route::post('dumpimport', [LeadController::class, 'import'])->name('dumpimport');;
        Route::post('importdashboard', [LeadController::class, 'importdashboard'])->name('importdashboard');;
        Route::get('state-filter', [LeadController::class, 'stateFilter'])->name('stateFilter');;
        //end

        Route::get('/mapState', [LeadController::class, 'mapState'])->name('mapState');
        Route::post('/mapStateSubmit', [LeadController::class, 'mapStateSubmit'])->name('mapStateSubmit');

        Route::get('/mapDistrict', [LeadController::class, 'mapDistrict'])->name('mapDistrict');
        Route::post('/mapDistrictSubmit', [LeadController::class, 'mapDistrictSubmit'])->name('mapDistrictSubmit');

        Route::get('/districts-filter', [LeadController::class, 'districtsFilter'])->name('districtsFilter');


        //state master for error
        Route::get('/statemaster', [StateMasterController::class, 'list'])->name('statemaster');
        Route::get('/statemaster/add', [StateMasterController::class, 'add'])->name('statemaster.add');
        Route::get('/statemaster/edit/{id}', [StateMasterController::class, 'edit'])->name('statemaster.edit');
        Route::post('/statemaster/update', [StateMasterController::class, 'update'])->name('statemaster.update');
        Route::get('/statemaster/delete/{id}', [StateMasterController::class, 'delete'])->name('statemaster.delete');
        Route::post('/statemaster/create', [StateMasterController::class, 'create'])->name('statemaster.create');
        Route::get('/statemaster/delete-empty-state', [StateMasterController::class, 'deleteemptystate'])->name('statemaster.deleteemptystate');
        //state master for error
        //District master for error
        Route::get('/districtmaster', [DistrictMasterController::class, 'list'])->name('districtmaster');
        Route::get('/districtmaster/add', [DistrictMasterController::class, 'add'])->name('districtmaster.add');
        Route::get('/districtmaster/edit/{id}', [DistrictMasterController::class, 'edit'])->name('districtmaster.edit');
        Route::post('/districtmaster/update', [DistrictMasterController::class, 'update'])->name('districtmaster.update');
        Route::get('/districtmaster/delete/{id}', [DistrictMasterController::class, 'delete'])->name('districtmaster.delete');
        Route::post('/districtmaster/create', [DistrictMasterController::class, 'create'])->name('districtmaster.create');
        Route::get('delete-empty-district', [DistrictMasterController::class, 'deleteemptydistrict'])->name('districtmaster.deleteemptydistrict');
        //District master for error

        //State District Brand mapping
        Route::get('/state-district-brand-mapping', [StateDistrictBrandMappingController::class, 'list'])->name('statedistrictbrandmapping');
        Route::get('/state-district-brand-mapping/add', [StateDistrictBrandMappingController::class, 'add'])->name('statedistrictbrandmapping.add');
        Route::post('/state-district-brand-mapping/create', [StateDistrictBrandMappingController::class, 'create'])->name('statedistrictbrandmapping.create');
        Route::get('/state-district-brand-mapping/edit/{id}', [StateDistrictBrandMappingController::class, 'edit'])->name('statedistrictbrandmapping.edit');
        Route::post('/state-district-brand-mapping/update', [StateDistrictBrandMappingController::class, 'update'])->name('statedistrictbrandmapping.update');
        Route::get('/state-district-brand-mapping/delete/{id}', [StateDistrictBrandMappingController::class, 'delete'])->name('statedistrictbrandmapping.delete');
        //State District Brand mapping

        //Brand master for error
        Route::get('/brandmaster', [BrandMasterErrController::class, 'list'])->name('brandmaster');
        Route::get('/brandmaster/add', [BrandMasterErrController::class, 'add'])->name('brandmaster.add');
        Route::get('/brandmaster/edit/{id}', [BrandMasterErrController::class, 'edit'])->name('brandmaster.edit');
        Route::post('/brandmaster/update', [BrandMasterErrController::class, 'update'])->name('brandmaster.update');
        Route::get('/brandmaster/delete/{id}', [BrandMasterErrController::class, 'delete'])->name('brandmaster.delete');
        Route::post('/brandmaster/create', [BrandMasterErrController::class, 'create'])->name('brandmaster.create');
        Route::get('/brand-empty-delete', [BrandMasterErrController::class, 'deleteemptybrand'])->name('brandmaster.deleteemptybrand');
        //brand master for error

        Route::get('/brand-filter', [LeadController::class, 'brandFilter'])->name('brandFilter');
        Route::get('/mapBrand', [LeadController::class, 'mapBrand'])->name('mapBrand');
        Route::post('/mapBrandSubmit', [LeadController::class, 'mapBrandSubmit'])->name('mapBrandSubmit');
        // CATEGORY CONTROLLER


        Route::get('/category', [CategoryController::class, 'list'])->name('category');
        Route::get('/category/add', [CategoryController::class, 'add'])->name('category.add');
        Route::post('/category/create', [CategoryController::class, 'create'])->name('category.create');
        Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
        Route::post('/category/update', [CategoryController::class, 'update'])->name('category.update');
        Route::get('/category/delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');

        // SUB-CATEGORY CONTROLLER

        Route::get('/subcategory', [SubcategoryController::class, 'list'])->name('subcategory');
        Route::get('/subcategory/add', [SubcategoryController::class, 'add'])->name('subcategory.add');
        Route::post('/subcategory/create', [SubcategoryController::class, 'create'])->name('subcategory.create');
        Route::get('/subcategory/edit/{id}', [SubcategoryController::class, 'edit'])->name('subcategory.edit');
        Route::post('/subcategory/update', [SubcategoryController::class, 'update'])->name('subcategory.update');
        Route::get('/subcategory/delete/{id}', [SubcategoryController::class, 'delete'])->name('subcategory.delete');
        Route::post('/getsubcategory', [SubcategoryController::class, 'getsubcategory'])->name('getsubcategory');
        // Route::post('/getsubprocess', [SubcategoryController::class, 'getSubprocess'])->name('getSubprocess');
        // Testimonial Controller

        Route::get('/brand', [BrandController::class, 'list'])->name('brand');
        Route::get('/brand/add', [BrandController::class, 'add'])->name('brand.add');
        Route::get('/brand/edit/{id}', [BrandController::class, 'edit'])->name('brand.edit');
        Route::post('/brand/update', [BrandController::class, 'update'])->name('brand.update');
        Route::get('/brand/delete/{id}', [BrandController::class, 'delete'])->name('brand.delete');
        Route::post('/brand/create', [BrandController::class, 'create'])->name('brand.create');

        Route::post('brandimport', [BrandController::class, 'import'])->name('brand.brandimport');;

        Route::get('/series', [SeriesController::class, 'list'])->name('series');
        Route::get('/series/add', [SeriesController::class, 'add'])->name('series.add');
        Route::get('/series/edit/{id}', [SeriesController::class, 'edit'])->name('series.edit');
        Route::post('/series/update', [SeriesController::class, 'update'])->name('series.update');
        Route::get('/series/delete/{id}', [SeriesController::class, 'delete'])->name('series.delete');
        Route::post('/series/create', [SeriesController::class, 'create'])->name('series.create');


        Route::get('/seriesDetails/{id}', [BrandController::class, 'seriesDetails'])->name('seriesDetails.list');
        //Route::post('/brand/seriesDetailsSubmit', [BrandController::class, 'seriesDetailsSubmit'])->name('seriesDetailsSubmit.submit');
        ///
        // End
        Route::post('/seriesDetails/create', [BrandController::class, 'seriesDetailscreate'])->name('seriesDetails.create');
        Route::get('/seriesDetails/edit/{id}', [BrandController::class, 'seriesDetailsedit'])->name('seriesDetails.edit');
        Route::post('/seriesDetails/update', [BrandController::class, 'seriesDetailsupdate'])->name('seriesDetails.update');
        Route::get('/seriesDetails/delete/{id}', [BrandController::class, 'seriesDetailsdelete'])->name('seriesDetails.delete');
        // BLOG CONTROLLER
        Route::get('/blog', [BlogController::class, 'list'])->name('Blog');
        Route::get('/blog/add', [BlogController::class, 'add'])->name('Blog.add');
        Route::post('/blog/create', [BlogController::class, 'create'])->name('Blog.create');
        Route::get('/blog/edit/{id}', [BlogController::class, 'edit'])->name('Blog.edit');
        Route::post('/blog/update', [BlogController::class, 'update'])->name('Blog.update');
        Route::get('/blog/delete/{id}', [BlogController::class, 'delete'])->name('Blog.delete');

        //ENDBlogControllerBlogCategoryController

        // BLOG Category CONTROLLER
        Route::get('/blogcategory', [BlogCategoryController::class, 'list'])->name('BlogCategory');
        Route::post('/blogcategory/add', [BlogCategoryController::class, 'create'])->name('BlogCategory.create');
        Route::get('/blogcategory/edit/{id}', [BlogCategoryController::class, 'edit'])->name('BlogCategory.edit');
        Route::post('/blogcategory/update', [BlogCategoryController::class, 'update'])->name('BlogCategory.update');
        Route::get('/blogcategory/delete/{id}', [BlogCategoryController::class, 'delete'])->name('BlogCategory.delete');
        //END


        // models CONTROLLER tractor
        Route::get('/models', [ModelController::class, 'list'])->name('models');
        Route::get('/models/add', [ModelController::class, 'add'])->name('models.add');
        Route::post('/models/create', [ModelController::class, 'create'])->name('models.create');
        Route::get('/models/edit/{id}', [ModelController::class, 'edit'])->name('models.edit');
        Route::post('/models/update', [ModelController::class, 'update'])->name('models.update');
        Route::get('/models/delete/{id}', [ModelController::class, 'delete'])->name('models.delete');
        Route::post('/getbrandSerices', [ModelController::class, 'getbrandSerices'])->name('getbrandSerices');
        Route::get('/gallery/images/{id}', [ModelController::class, 'images'])->name('gallery.images');

        Route::get('/gallery/deleteimage/{id}', [ModelController::class, 'deleteimage'])->name('gallery.deleteimage');
        Route::post('/gallery/createimage', [ModelController::class, 'createimage'])->name('gallery.createimage');


        Route::post('import', [ModelController::class, 'import'])->name('models.import');;

        Route::get('export', [ModelController::class, 'export']);



        Route::post('modal-sortable', [ModelController::class, 'sortableData']);
        Route::post('updateOrder', [ModelController::class, 'updateOrder'])->name('updateOrder');;
        Route::get('sortall', [ModelController::class, 'sortall'])->name('sortall');;
        //END

        // models CONTROLLER tractor
        Route::get('/impelments', [ImpelmentsController::class, 'list'])->name('impelments');
        Route::get('/impelments/add', [ImpelmentsController::class, 'add'])->name('impelments.add');
        Route::post('/impelments/create', [ImpelmentsController::class, 'create'])->name('impelments.create');
        Route::get('/impelments/edit/{id}', [ImpelmentsController::class, 'edit'])->name('impelments.edit');
        Route::post('/impelments/update', [ImpelmentsController::class, 'update'])->name('impelments.update');
        Route::get('/impelments/delete/{id}', [ImpelmentsController::class, 'delete'])->name('impelments.delete');
        Route::post('/getbrandSerices', [ImpelmentsController::class, 'getbrandSerices'])->name('getbrandSerices');
        Route::get('/gallery/images/{id}', [ImpelmentsController::class, 'images'])->name('gallery.images');

        Route::get('/gallery/deleteimage/{id}', [ImpelmentsController::class, 'deleteimage'])->name('gallery.deleteimage');
        Route::post('/gallery/createimage', [ImpelmentsController::class, 'createimage'])->name('gallery.createimage');
        //END


        // hours_master CONTROLLER
        Route::get('/hours_master', [HoursMasterController::class, 'list'])->name('hours_master');
        Route::get('/hours_master/add', [HoursMasterController::class, 'add'])->name('hours_master.add');
        Route::post('/hours_master/create', [HoursMasterController::class, 'create'])->name('hours_master.create');
        Route::get('/hours_master/edit/{id}', [HoursMasterController::class, 'edit'])->name('hours_master.edit');
        Route::post('/hours_master/update', [HoursMasterController::class, 'update'])->name('hours_master.update');
        Route::get('/hours_master/delete/{id}', [HoursMasterController::class, 'delete'])->name('hours_master.delete');
        //END
        // hours_master CONTROLLER
        Route::get('/owners_master', [OwnerMasterController::class, 'list'])->name('owners_master');
        Route::get('/owners_master/add', [OwnerMasterController::class, 'add'])->name('owners_master.add');
        Route::post('/owners_master/create', [OwnerMasterController::class, 'create'])->name('owners_master.create');
        Route::get('/owners_master/edit/{id}', [OwnerMasterController::class, 'edit'])->name('owners_master.edit');
        Route::post('/owners_master/update', [OwnerMasterController::class, 'update'])->name('owners_master.update');
        Route::get('/owners_master/delete/{id}', [OwnerMasterController::class, 'delete'])->name('owners_master.delete');
        Route::post('post-sortable', [OwnerMasterController::class, 'sortableData']);

        //END

        //Location mster
        Route::post('/get_state_districts', [LocationMasterController::class, 'getStateDistricts'])->name('getStateDistricts');
        Route::post('/getStateDistricts_by_name', [LocationMasterController::class, 'getStateDistrictsbyName'])->name('getStateDistrictsbyName');
        Route::post('/get_districts_tahshil', [LocationMasterController::class, 'getDistrictsTahshil'])->name('getDistrictsTahshil');
        Route::get('/location_master', [LocationMasterController::class, 'list'])->name('location_master');

        Route::post('importState', [LocationMasterController::class, 'importState'])->name('importState');;

        Route::post('importDistrict', [LocationMasterController::class, 'importDistrict'])->name('importDistrict');

        Route::get('/states', [LocationMasterController::class, 'states'])->name('states');
        Route::post('/state/create', [LocationMasterController::class, 'createState'])->name('createState');
        Route::get('/state/edit/{id}', [LocationMasterController::class, 'editState'])->name('owners_master.editState');
        Route::post('/state/update', [LocationMasterController::class, 'updateState'])->name('updateState');
        Route::get('/state/delete/{id}', [LocationMasterController::class, 'deleteState'])->name('deleteState');

        Route::get('/districts', [LocationMasterController::class, 'districts'])->name('districts');
        Route::post('/district/create', [LocationMasterController::class, 'createDistrict'])->name('createDistrict');
        Route::get('/district/edit/{id}', [LocationMasterController::class, 'editDistrict'])->name('editDistrict');
        Route::post('/district/update', [LocationMasterController::class, 'updateDistrict'])->name('updateDistrict');
        Route::get('/district/delete/{id}', [LocationMasterController::class, 'deleteDistrict'])->name('deleteDistrict');

        Route::get('/tehsil', [LocationMasterController::class, 'tahshil'])->name('tahshil');
        Route::post('/tehsil/create', [LocationMasterController::class, 'createTahshil'])->name('createTahshil');
        Route::get('/tehsil/edit/{id}', [LocationMasterController::class, 'editTahshil'])->name('editTahshil');
        Route::post('/tehsil/update', [LocationMasterController::class, 'updateTahshil'])->name('updateTahshil');
        Route::get('/tehsil/delete/{id}', [LocationMasterController::class, 'deleteTahshil'])->name('deleteTahshil');

        Route::get('/tahshilajax', [LocationMasterController::class, 'tahshilajax'])->name('tahshilajax');
        Route::get('/gettahshil', [LocationMasterController::class, 'getTahshil'])->name('getTahshil');

        //end


        //FaqController mster
        Route::get('/faqs', [FaqController::class, 'list'])->name('faqs');
        Route::get('/faqpage/{id}', [FaqController::class, 'faqPages'])->name('faqPages');
        Route::post('/faq/create', [FaqController::class, 'createFaq'])->name('faqCreate');
        Route::post('/faq/update', [FaqController::class, 'faqUpdate'])->name('faqUpdate');
        Route::post('/faq/delete', [FaqController::class, 'faqDelete'])->name('faqDelete');

        //end

        //pages
        Route::get('/pages', [PageController::class, 'list'])->name('pages');
        Route::get('/pages/add', [PageController::class, 'add'])->name('pages.add');
        Route::get('/pages/edit/{id}', [PageController::class, 'edit'])->name('pages.edit');
        Route::post('/pages/update', [PageController::class, 'update'])->name('pages.update');
        Route::get('/pages/delete/{id}', [PageController::class, 'delete'])->name('pages.delete');
        Route::post('/pages/create', [PageController::class, 'create'])->name('pages.create');
        //end
    });
});
Route::get('/', [UserController::class, 'home'])->name('admin.dashboard');
// ADMIN ROUTES
