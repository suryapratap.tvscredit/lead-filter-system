
//Mean Menu

$(document).ready(function () {
 
//wow js
	new WOW().init();
    wow = new WOW({
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    })
    wow.init();

//Banner Slider
    $('.homeBanner').slick({
        dots: false,
        infinite: true,
        loop:true,
        autoplay:true,
        autoplaySpeed: 9000,
        arrows: false,
        speed:1500,
        fade: true,
        cssEase: 'ease-in-out',
        touchThreshold: 100,
        responsive: [
            
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                vertical: false,
                 verticalSwiping: false
              },
            },
          ],
    });


// search box

 $(".search-container img").click(function () {
    $(".searchBox").slideToggle();
  });
//excellence-slider
	$('.excellence-slider,.specSlider,.hosdocSlider').slick({
        dots: false,
        infinite: true,
        loop:true,
        arrows:true,
        speed: 1500,
        autoplay:true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                adaptiveHeight: true,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
          ],
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
    });
   
   //launchSlider-slider
	$('.launchSlider,.delightSlider').slick({
    dots: false,
    infinite: true,
    loop:true,
    arrows:true,
    speed: 1500,
    autoplay:true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
    nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
});
 //expert-slider
 $('.expert-slider').slick({
    dots: true,
    infinite: true,
    loop:true,
    arrows:false,
    speed: 1500,
    autoplay:true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
      prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
      nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
  });

   //expert-slider
    $('.mediaSlider').slick({
      dots: false,
      infinite: true,
      loop:true,
      arrows:true,
      speed: 1500,
      autoplay:true,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              adaptiveHeight: true,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
    });

    //Bundle-slider
	$('.bundleSlider').slick({
    dots: false,
    infinite: true,
    loop:true,
    arrows:true,
    speed: 1500,
    autoplay:true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
    nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
});

 //Bundle-slider2
 $('.bundleSlider2').slick({
  dots: false,
  infinite: true,
  loop:true,
  arrows:true,
  speed: 1500,
  autoplay:true,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          adaptiveHeight: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
  nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
});
    //park-viewSlider 
    $('.park-viewSlider').slick({
      dots: true,
      infinite: true,
      loop:true,
      autoplay:true,
      arrows: false,
      speed:1500,
      responsive: [
          
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              vertical: false,
              verticalSwiping: false
            },
          },
        ],
    });

     //Testimonial-Slider 
     $('.testimonialSlider').slick({
        dots: false,
        infinite: true,
        loop:true,
        autoplay:true,
        arrows: true,
        swipe: false,
        touchMove: true,
        vertical: true,
        verticalSwiping: true,
        speed: 1000,
        autoplaySpeed: 4000,
        useTransform: true,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
        responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              vertical: false,
              verticalSwiping: false,
              touchMove: true,
            },
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              vertical: false,
              verticalSwiping: false,
              touchMove: true,
            },
          },
        ],
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
    });

    // counter
    var a='';
    $(window).scroll(function() {
      var oTop = $('#counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
            $('.counter-value').each(function() {
            var $this = $(this),
                countTo = $this.attr('data-count');
                //alert(countTo);
            $({
                countNum: $this.text()
            }).animate({
                countNum: countTo
                },

                {

                duration: 4000,
                easing: 'swing',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(this.countNum);
                    //alert('finished');
                }

                });
            });
            a = 1;
        }

      });

     
     
        $('.patientSlider').slick({
          dots: false,
          infinite: true,
          loop:true,
          arrows:true,
          speed: 1500,
          autoplay:true,
          slidesToShow: 3,
          slidesToScroll: 1,
          responsive: [
              {
                breakpoint: 1025,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  adaptiveHeight: true,
                },
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
            ],
          prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
          nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
      });

      // management slider
      $('.motivatorSlider,.careerSlider').slick({
        dots: false,
        infinite: true,
        loop:true,
        arrows:true,
        speed: 1500,
        autoplay:true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                adaptiveHeight: true,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
    });

    // other doctor slider
    $('.otherDoctors').slick({
      dots: true,
      infinite: true,
      loop:true,
      arrows:false,
      speed: 1500,
      autoplay:true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              adaptiveHeight: true,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
      nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
  });

  // advance slider
  $('.advanceSlider').slick({
      dots: false,
      infinite: true,
      loop:true,
      arrows:true,
      speed: 1500,
      autoplay:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              adaptiveHeight: true,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
      nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
  });
 
  // Hospital slider
  $('.hospitalBanner').slick({
    dots: false,
    infinite: true,
    loop:true,
    arrows:true,
    speed: 1500,
    autoplay:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
    nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
});

 
   
     
      
     
    const $rootSingle = $('.cSlider--single');
    const $rootNav = $('.cSlider--nav');
    
     $rootSingle.slick({
        slide: '.cSlider__item',
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        autoplay:true,
        centerMode: true,
        centerPadding: '20px',
        speed: 1500,
        responsive: [{
              breakpoint: 640,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  autoplay:true
              }
          }, {
              breakpoint: 420,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  autoplay:true
          }
          }],
        cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
     });
    
     $rootNav.on('init', function(event, slick) {
            $(this).find('.slick-slide.slick-current').addClass('is-active');
        })
        .slick({
            slide: '.cSlider__item',
            slidesToShow: 7,
            slidesToScroll: 7,
            dots: false,
            autoplay:true,
            focusOnSelect: false,
            infinite: false,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    autoplay:true
                }
            }, {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay:true
                }
            }, {
                breakpoint: 420,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay:true
            }
            }]
        });
    
         $rootSingle.on('afterChange', function(event, slick, currentSlide) {
            $rootNav.slick('slickGoTo', currentSlide);
            $rootNav.find('.slick-slide.is-active').removeClass('is-active');
            $rootNav.find('.slick-slide[data-slick-index="' + currentSlide + '"]').addClass('is-active');
         });
    
         $rootNav.on('click', '.slick-slide', function(event) {
            event.preventDefault();
            var goToSingleSlide = $(this).data('slick-index');
    
             $rootSingle.slick('slickGoTo', goToSingleSlide);
         });
         
        

   //csblog-slider
	$('.csBlogSlider').slick({
    dots: false,
    infinite: true,
    loop:true,
    centerMode: true,
    centerPadding: '20px',
    arrows:true,
    speed: 1500,
    autoplay:true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            adaptiveHeight: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
    });

     //patientReviewSlider-slider
    $('.patientReviewSlider').slick({
      dots: true,
      infinite: true,
      loop:true,
      centerMode: true,
      centerPadding: '150px',
      arrows:false,
      speed: 1500,
      autoplay:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
          {
            breakpoint: 1025,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              adaptiveHeight: true,
            },
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerPadding: '10px',
              centerMode: false,
            },
          },
        ],
          prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
          nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
      });

    // if($(window).width()<767){
    // }
      $('.appintment-slider').slick({
        dots: false,
        infinite: true,
        loop:true,
        arrows:true,
        speed: 1500,
        autoplay:true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                adaptiveHeight: true,
              },
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
          ],
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
        });
    
        // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        //   $('.specSlider').slick('setPosition');
        // })
        

        $(function () {
          'use strict';
        
          var $swipeTabsContainer = $('.swipe-tabs'),
            $swipeTabs = $('.swipe-tab'),
            $swipeTabsContentContainer = $('.swipe-tabs-container'),
            currentIndex = 0,
            activeTabClassName = 'active-tab';
        
          $swipeTabsContainer.on('init', function(event, slick) {
            $swipeTabsContentContainer.removeClass('invisible');
            $swipeTabsContainer.removeClass('invisible');
        
            currentIndex = slick.getCurrent();
            $swipeTabs.removeClass(activeTabClassName);
                 $('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
          });
        
          $swipeTabsContainer.slick({
            //slidesToShow: 3.25,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            swipeToSlide: true,
            touchThreshold: 10,
            responsive: [
              {
                breakpoint: 1025,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  adaptiveHeight: true,
                },
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1,
                },
              },
            ],
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'
          });
        
          $swipeTabsContentContainer.slick({
            asNavFor: $swipeTabsContainer,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            swipeToSlide: true,
            draggable: false,
            touchThreshold: 10
          });
        
        
          $swipeTabs.on('click', function(event) {
                // gets index of clicked tab
                currentIndex = $(this).data('slick-index');
                $swipeTabs.removeClass(activeTabClassName);
                $('.swipe-tab[data-slick-index=' + currentIndex +']').addClass(activeTabClassName);
                $swipeTabsContainer.slick('slickGoTo', currentIndex);
                $swipeTabsContentContainer.slick('slickGoTo', currentIndex);
            });
        
            //initializes slick navigation tabs swipe handler
            $swipeTabsContentContainer.on('swipe', function(event, slick, direction) {
              currentIndex = $(this).slick('slickCurrentSlide');
            $swipeTabs.removeClass(activeTabClassName);
            $('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
          });





        });
        
})


