<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class StateDistrictBrandMapping extends Model
{
    use HasFactory;
    protected $table = 'state_district_brand_mappings';
    protected $fillable = [
        'state_name',
        'districts',
        'brands',
    ];

    /**
     * Write code on Method
     *
     * @return response()
     */
}
