<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FilterData extends Model
{
    use HasFactory;
    protected $table = 'filter_datas';
    protected $fillable = [
        'lead_source',
        'source',
        'lead_type',
        'enq_id',
        'mobile',
        'name',
        'tkstate',
        'tkdistrict',
        'tktehsil',
        'tkproduct_category',
        'tkbrand',
        'tkmodel',
        'website',
        'lead_date',
        'created_on',
        'zip',
        'pancard',
        'loan_type',
        'is_state_filter',
        'is_brand_filter',
        'working_district',
        'is_district_filter',

    ];

    /**
     * Write code on Method
     *
     * @return response()
     */
}
