<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BrandMasterErr extends Model
{
    use HasFactory;
    protected $table = 'brandmasterserr';
    protected $fillable = [
        'brand_name',
        'error_value',
    ];

    /**
     * Write code on Method
     *
     * @return response()
     */
}
