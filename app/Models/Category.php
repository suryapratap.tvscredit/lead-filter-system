<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;
    protected $table = 'category';
    protected $fillable = [
        'category',
        'status',
        'description',
        'description2',
        'type',
        'title',
        'content',
        'image',
        'menuicon',
        'caticon',
        'bannerimage',
        'sub_title',
        'banner_titile',
        'section2_content',
    ];
    protected static function boot()
    {
        parent::boot();

        static::created(function ($category) {
            $category->slug = $category->createSlug($category->category);
            $category->save();
        });
    }

    /** 
     * Write code on Method
     *
     * @return response()
     */
    private function createSlug($category)
    {
        if (static::whereSlug($slug = Str::slug($category))->exists()) {
            $max = static::whereTitle($category)->latest('id')->skip(1)->value('slug');

            if (is_numeric($max[-1])) {
                return preg_replace_callback('/(\d+)$/', function ($mathces) {
                    return $mathces[1] + 1;
                }, $max);
            }

            return "{$slug}-2";
        }

        return $slug;
    }
}
