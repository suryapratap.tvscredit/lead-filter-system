<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Subcategory extends Model
{
    use HasFactory;
    protected $table = 'subcategory';
    protected $fillable = [
        'subcategory',
        'category_id',
        'status',
        'sub_description',
        'sub_description2',
        'type',
        'title',
        'content',
        'image',
        'menuicon',
        'caticon',
       'slug',

        'banner_titile',

    ];
	
	  protected static function boot()
    {
        parent::boot();

        static::created(function ($subcategory) {
            $subcategory->slug = $subcategory->createSlug($subcategory->subcategory);
            $subcategory->save();
        });
    }

    /** 
     * Write code on Method
     *
     * @return response()
     */
    private function createSlug($subcategory)
    {
        if (static::whereSlug($slug = Str::slug($subcategory))->exists()) {
            $max = static::whereTitle($subcategory)->latest('id')->skip(1)->value('slug');

            if (is_numeric($max[-1])) {
                return preg_replace_callback('/(\d+)$/', function ($mathces) {
                    return $mathces[1] + 1;
                }, $max);
            }

            return "{$slug}-2";
        }

        return $slug;
    }
}
