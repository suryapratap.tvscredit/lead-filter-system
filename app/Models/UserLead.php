<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class UserLead extends Model
{
    use HasFactory;
    protected $table = 'user_leads';
    protected $fillable = [
       'mobile', 'otp', 'otp_status', 'brand_id', 'model_id', 'year_id','state_id','district_id', 'tahshil_id', 'owner_id', 'hour_id', 'engine', 'tyre', 'status', 'address', 'visit_date', 'visit_time','front_image','back_image','leftside_image','rightside_image','username','pan_number'
    ];
   
    /** 
     * Write code on Method
     *
     * @return response()
     */
  
   
}
