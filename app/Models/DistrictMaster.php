<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DistrictMaster extends Model
{
    use HasFactory;
    protected $table = 'districts';
    protected $fillable = [
        'district',
        'state_id',
    ];
   
  
}
