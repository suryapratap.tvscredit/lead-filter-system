<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TempData extends Model
{
    use HasFactory;
    protected $table = 'temp_datas';
    protected $fillable = [
        'lead_source',
        'source',
        'lead_type',
        'enq_id',
        'mobile',
        'name',
        'tkstate',
        'tkdistrict',
        'tktehsil',
        'tkproduct_category',
        'tkbrand',
        'tkmodel',
        'website',
        'lead_date',
        'created_on',
        'zip',
        'pancard',
        'loan_type',
        'is_state_filter',
        'is_brand_filter',
        'working_district',

    ];

    /**
     * Write code on Method
     *
     * @return response()
     */
}
