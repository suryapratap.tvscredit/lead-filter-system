<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DistrictMasterErr extends Model
{
    use HasFactory;
    protected $table = 'districtmastererrs';
    protected $fillable = [
        'state_name',
        'district_name',
        'error_value',
    ];

    /**
     * Write code on Method
     *
     * @return response()
     */
}
