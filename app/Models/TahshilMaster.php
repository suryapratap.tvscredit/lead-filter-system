<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TahshilMaster extends Model
{
    use HasFactory;
    protected $table = 'tahshil';
    protected $fillable = [
        'tahshil',
        'district_id',
    ];
   
  
   
}
