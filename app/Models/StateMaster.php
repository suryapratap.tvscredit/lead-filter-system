<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class StateMaster extends Model
{
    use HasFactory;
    protected $table = 'statemasters';
    protected $fillable = [
        'state_name',
        'error_value',
    ];

    /**
     * Write code on Method
     *
     * @return response()
     */
}
