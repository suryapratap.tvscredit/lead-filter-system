<?php

namespace App\Http\Controllers;


use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\StateMaster;
use App\Models\HourMaster;
use App\Models\OwnerMaster;
use App\Models\Year;
use App\Models\UserLead;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\S;
use Illuminate\Support\Facades\Session;

class EvaluationController extends Controller
{

    public function index()
    {
        $brand = Brand::where("status", "active")->get();
        $states = StateMaster::orderBy('name', 'ASC')->get();
        $hours = HourMaster::all();
        $owners = OwnerMaster::orderBy('id', 'ASC')->get();
        $year = Year::orderBy('id', 'desc')->get();

        return view("fronts.evaluation.index", compact('brand', 'states', 'hours', 'owners', 'year'));
    }

    public function getModelbyBrand(Request $request)
    {
        $subbrand = DB::table("models")->where("brand_id", $request->brand_id)->where("status", "active")->orderBy("order", "asc")->get();
        $data = '';
        foreach ($subbrand as $s) {
            $data .= '<div class="col-lg-2 col-md-3 col-12 g-2">
                <button  model_id = "' . $s->id . '" modelName = "' . $s->title . '" data-step-action="next" class="inner-btn step-btn border-0 p-2 year-border border-bottom bg-white text-left text-start getModelId"><p>' . $s->title . '</p></button>
                </div>';
        }

        // dd($data);
        return response()->json($data);
    }

    public function SubmitDetail(Request $request)
    {
        //dd($_FILES);
        // dd($request->all());
        $this->validate($request, [

            // 'brand' => 'required',
        ]);

        $userlead = UserLead::where("mobile", $request->userMobile)->first();
        //dd($filePath);
        if ($userlead) {

            if ($request->hasfile('frontSide')) {

                if ($request->file('frontSide')) {
                    $fileName = time() . '_' . $request->frontSide->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $frontSide = $request->file('frontSide')->store('public/upload');
                }
            } else {
                $frontSide = '';
            }

            if ($request->hasfile('backSide')) {

                if ($request->file('backSide')) {

                    $backSide = $request->file('backSide')->store('public/upload');
                }
            } else {
                $backSide = '';
            }

            if ($request->hasfile('leftSide')) {

                if ($request->file('leftSide')) {
                    //$fileName = time() . '_' . $request->leftSide->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $leftSide = $request->file('leftSide')->store('public/upload');
                }
            } else {
                $leftSide = '';
            }

            if ($request->hasfile('rightSide')) {

                if ($request->file('rightSide')) {
                    //$fileName = time() . '_' . $request->rightSide->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $rightSide = $request->file('rightSide')->store('public/upload');
                }
            } else {
                $rightSide = '';
            }
            UserLead::where('mobile', $request->userMobile)
                ->update([
                    'brand_id' => $request->brand_id,
                    'year_id' => $request->year_id,
                    'model_id' => $request->model_id,
                    'state_id' => $request->state_id,
                    'district_id' => $request->district_id,
                    'tahshil_id' => $request->tahshil_id,
                    'owner_id' => $request->owner_id,
                    'hour_id' => $request->hour_id,
                    'engine' => $request->engine,
                    'tyre' => $request->tyre,
                    'front_image' => $frontSide,
                    'back_image' => $backSide,
                    'leftside_image' =>  $leftSide,
                    'rightside_image' => $rightSide,
                    'otp_status' => "approved",

                ]);
        }


        Session::put('userid', $userlead->id);
        return redirect('/evaluation-detail');
    }
    public function evaluationDetail(Request $request)
    {
        $data = Session::get('userid');
        $userlead = UserLead::where("user_leads.id", $data)
            ->leftjoin('brands', 'brands.id', '=', 'user_leads.brand_id')
            ->leftjoin('models', 'models.id', '=', 'user_leads.model_id')
            ->leftjoin('year_master', 'year_master.id', '=', 'user_leads.year_id')
            ->leftjoin('states', 'states.id', '=', 'user_leads.state_id')
            ->leftjoin('districts', 'districts.id', '=', 'user_leads.district_id')
            ->leftjoin('tahshil', 'tahshil.id', '=', 'user_leads.tahshil_id')
            ->leftjoin('owners_masters', 'owners_masters.id', '=', 'user_leads.owner_id')
            ->leftjoin('hours_master', 'hours_master.id', '=', 'user_leads.hour_id')
            ->first([
                'user_leads.*',
                'brands.title as brand_title',
                'models.title as models_title',
                'year_master.year as year',
                'states.name as state_title',
                'districts.district as district',
                'tahshil.tahshil as tahshil',
                'owners_masters.title as owners_title',
                'hours_master.title as hours_title',
            ]);
        // dd($userlead);

        return view("fronts.evaluation.evaluationdetail", compact('userlead'));
    }
    public function getLoan(Request $request)
    {
        if (session('userid') != null) {
            $data = Session::get('userid');
            $userlead = UserLead::where("user_leads.id", $data)
                ->leftjoin('brands', 'brands.id', '=', 'user_leads.brand_id')
                ->leftjoin('models', 'models.id', '=', 'user_leads.model_id')
                ->leftjoin('year_master', 'year_master.id', '=', 'user_leads.year_id')
                ->leftjoin('states', 'states.id', '=', 'user_leads.state_id')
                ->leftjoin('districts', 'districts.id', '=', 'user_leads.district_id')
                ->leftjoin('tahshil', 'tahshil.id', '=', 'user_leads.tahshil_id')
                ->leftjoin('owners_masters', 'owners_masters.id', '=', 'user_leads.owner_id')
                ->leftjoin('hours_master', 'hours_master.id', '=', 'user_leads.hour_id')
                ->first([
                    'user_leads.*',
                    'brands.title as brand_title',
                    'models.title as models_title',
                    'year_master.year as year',
                    'states.name as state_title',
                    'districts.district as district',
                    'tahshil.tahshil as tahshil',
                    'owners_masters.title as owners_title',
                    'hours_master.title as hours_title',
                ]);
            return view("fronts.evaluation.getloan", compact('userlead'));
        } else {
            return redirect('/home');
        }
    }

    public function evaluationLoanSubmit(Request $request)
    {
        UserLead::where('id', $request->userlead_id)
            ->update([
                'username' => $request->username,
                'pan_number' => $request->pan_number,
            ]);
        return redirect('/evaluation-thankyou');
    }
    public function thankYou(Request $request)
    {
        if (session('userid') != null) {
            $data = Session::get('userid');
            $userlead = UserLead::where("user_leads.id", $data)
                ->leftjoin('brands', 'brands.id', '=', 'user_leads.brand_id')
                ->leftjoin('models', 'models.id', '=', 'user_leads.model_id')
                ->leftjoin('year_master', 'year_master.id', '=', 'user_leads.year_id')
                ->leftjoin('states', 'states.id', '=', 'user_leads.state_id')
                ->leftjoin('districts', 'districts.id', '=', 'user_leads.district_id')
                ->leftjoin('tahshil', 'tahshil.id', '=', 'user_leads.tahshil_id')
                ->leftjoin('owners_masters', 'owners_masters.id', '=', 'user_leads.owner_id')
                ->leftjoin('hours_master', 'hours_master.id', '=', 'user_leads.hour_id')
                ->first([
                    'user_leads.*',
                    'brands.title as brand_title',
                    'models.title as models_title',
                    'year_master.year as year',
                    'states.name as state_title',
                    'districts.district as district',
                    'tahshil.tahshil as tahshil',
                    'owners_masters.title as owners_title',
                    'hours_master.title as hours_title',
                ]);
            return view("fronts.evaluation.thankyou", compact('userlead'));
        } else {
            return redirect('/home');
        }
        //return view("fronts.evaluation.thankyou");
    }
    public function SubmitMobile(Request $request)
    {
        $check = UserLead::where("mobile", $request->mobileNumber)->first();
        //$otp = random_int(100000, 999999);
        $otp = 1234;

        if ($check) {
            $otpupdate = UserLead::find($check->id);
            $otpupdate->otp =  $otp;
            $otpupdate->update();
        } else {
            $cat = UserLead::Create([
                'mobile' => $request->mobileNumber,
                'otp' => $otp,
                'otp_status' => "pending",
            ]);
        }

        $data = array("message:success");
        return response()->json($data);
    }
    public function checkOtp(Request $request)
    {
        //$otp = random_int(100000, 999999);
        $check = UserLead::where("mobile", $request->mobileNumber)->where("otp", $request->mobileOtp)->first();
        if ($check && $check->id != "") {
            $data = "succcess";
        } else {
            $data = "error";
        }
        return response()->json($data);
    }
}
