<?php

namespace App\Http\Controllers;


use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\StateMaster;
use App\Models\HourMaster;
use App\Models\OwnerMaster;
use App\Models\Year;
use App\Models\Blog;
use App\Models\Blogcategory;
use DB;
use Session;

class FrontBlogController extends Controller
{

    public function index()
    {
        $blogcategory = Blogcategory::all();
        $blogs = Blog::leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory', 'blogcategory.slug as category_slug'])->paginate(2);

        return view("fronts.blogs.index", compact('blogs', 'blogcategory'));
    }
    public function blogDetail($slug)
    {
        $blogcategory = Blogcategory::all();
        $blogs = Blog::where('blog.slug', $slug)->leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory', 'blogcategory.slug as category_slug'])->first();

        $similer = Blog::where('blog.blogcategory_id', $blogs->blogcategory_id)->leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory', 'blogcategory.slug as category_slug'])->get();

        $populer = Blog::where('is_populer', 1)->leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory', 'blogcategory.slug as category_slug'])->get();

        $previous_record = Blog::where('id', '<', $blogs->id)->orderBy('id', 'desc')->first();
        $next_record = Blog::where('id', '>', $blogs->id)->orderBy('id')->first();

        return view("fronts.blogs.detail", compact('blogs', 'blogcategory', 'similer', 'populer', 'previous_record', 'next_record'));
    }
    public function categoryBlogs($slug)
    {
        $blogcategory = Blogcategory::all();
        $blogcategoryselected = Blogcategory::where('slug', $slug)->orderby('id', 'desc')->first();
        //$blogs = Blog::where('slug', $slug)->leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory'])->first();
        $blogs = Blog::where('blog.blogcategory_id', $blogcategoryselected->id)->leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory', 'blogcategory.slug as category_slug'])->get();

        $populer = Blog::where('is_populer', 1)->leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')->select(['blog.*', 'blogcategory.blogcategory', 'blogcategory.slug as category_slug'])->get();



        return view("fronts.blogs.category_blog", compact('blogs', 'blogcategory', 'blogcategoryselected', 'populer'));
    }
}
