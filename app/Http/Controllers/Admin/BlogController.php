<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Blog;
use App\Models\Blogcategory;

use App\Models\Hospital;
use App\Models\OurHospital;

use App\Models\Doctor;
use App\Models\Location;
use App\Models\Gallery;
use DB;

class BlogController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $blog = Blog::leftjoin('blogcategory', 'blog.blogcategory_id', '=', 'blogcategory.id')
            ->get(['blog.*', 'blogcategory.blogcategory']);
        $category = Category::all();
        return view('admin.blog.blog', compact('blog', 'category'));
    }


    public function add()
    {
        $blogcategory = Blogcategory::all();

        return view('admin.blog.add', compact('blogcategory'));
    }


    public function create(Request $request)

    {


        //$doc_id= implode(",", $request->doc_id)
        $this->validate($request, [

            'title' => 'required',

        ]);
        $filePath = '';
        if ($request->hasfile('blog_image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->blog_image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('blog_image')) {
                    $fileName = time() . '_' . $request->blog_image->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('blog_image')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        Blog::Create([
            'blogcategory_id' => $request->blogcategory,
            'title' => $request->title,
            'status' => $request->status,
            'shortdesc' => $request->shortdesc,
            'longdesc' => $request->longdesc,
            'metatitle' => $request->metatitle,
            'metakeyword' => $request->metakeyword,
            'metadescription' => $request->metadescription,
            'blog_image' =>  $filePath,
            'blog_date' => $request->blog_date,
            'author' => $request->author,
            'blog_type' => $request->blog_type,
            'is_populer' => $request->is_populer,
        ]);
        $request->session()->flash('success', 'Blog created Successfully');
        return redirect('/admin/blog');
    }

    public function edit($id)
    {
        // $Blog = Blog::find($id);
        // return response()->json($Blog);

        $where = array('id' => $id);
        $blog = Blog::where($where)->first();
        $blogcategory = Blogcategory::all();
        return view('admin.blog.edit')->with(compact('blog', 'blogcategory'));
    }
    public function update(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'blogcategory_id' => 'required',
            'title' => 'required',
            'longdesc' => 'required',
            'shortdesc' => 'required',
            'status' => 'required',
        ]);
        $blog = Blog::where('id', $request->id)->first();
        if ($request->hasfile('blog_image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->blog_image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('blog_image')) {
                    $fileName = time() . '_' . $request->blog_image->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('blog_image')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        } else {
            $filePath = $blog->blog_image;
        }

        Blog::where('id', $request->id)->update([
            'blogcategory_id' => $request->blogcategory_id,
            'title' => $request->title,
            'status' => $request->status,
            'shortdesc' => $request->shortdesc,
            'longdesc' => $request->longdesc,
            'metatitle' => $request->metatitle,
            'metakeyword' => $request->metakeyword,
            'metadescription' => $request->metadescription,
            'blog_image' =>  $filePath,
            'is_populer' => $request->is_populer,
            'blog_date' => $request->blog_date,
            'author' => $request->author,
            'blog_type' => $request->blog_type,

        ]);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/blog');
    }
    public function delete(Request $request, $id)
    {
        Blog::where('id', $id)->delete();
        $request->session()->flash('error', 'Blog Deleted Successfully');
        return redirect('/admin/blog');
    }
}
