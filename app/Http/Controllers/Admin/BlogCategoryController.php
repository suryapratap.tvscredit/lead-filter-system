<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Blogcategory;
use DB;

class BlogcategoryController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $Blogcategory = Blogcategory::all();

        return view('admin.blogcategory.blogcategory', compact('Blogcategory'));
    }


    public function create(Request $request)

    {

        $this->validate($request, [

            // 'category' => 'required',
        ]);
        Blogcategory::Create([
            'blogcategory' => $request->blogcategory,
            'status' => $request->status,

        ]);
        return response()->json(response());
    }

    public function edit($id)
    {
        $Blogcategory = Blogcategory::find($id);
        return response()->json($Blogcategory);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'blogcategory' => 'required',
            'status' => 'required',
        ]);
        Blogcategory::where('id', $request->id)->update([
            'blogcategory' => $request->blogcategory,
            'status' => $request->status,
        ]);
        return response()->json();
    }
    public function delete(Request $request, $id)
    {
        Blogcategory::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/blogcategory');
    }
}
