<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Brand;
use App\Models\Series;
use App\Models\Year;
use App\Models\State;
use App\Models\DistrictMaster;
use App\Models\TahshilMaster;
use DB;

class LocationMasterController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        //$series = Series::leftjoin('brands', 'brands.id', '=', 'series.brand_id')->get(['series.*', 'brands.title as brand_title']);
        $states = State::orderBy('name', 'ASC')->get();

        return view('admin.location_master.test', compact('states'));
    }
    public function getStateDistricts(Request $request)
    {
        $subbrand = DB::table("districts")->where("state_id", $request->state_id)->orderBy('district', 'ASC')->get();
        $data = '';
        if ($request->district_id != 0) {
            $data .= " <option value='NA'> Select District</option>";
            foreach ($subbrand as $s) {
                if ($request->district_id == $s->district) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->district . '" ' . $selected . '>' . $s->district . '</option>';
            }
        } else {
            $data .= " <option value='NA'> Select District</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->district . "'>" . $s->district . "</option>";
            }
        }
        //dd($data);
        return response()->json($data);
    }
    public function getStateDistrictsbyName(Request $request)
    {
        $states = State::where('name', $request->state_id)->first();
        $subbrand = DB::table("districts")->where("state_id", $states->id)->orderBy('district', 'ASC')->get();
        $data = '';
        if ($request->district_id != 0) {
            $data .= " <option value='NA'> Select District</option>";
            foreach ($subbrand as $s) {
                if ($request->district_id == $s->district) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->district . '" ' . $selected . '>' . $s->district . '</option>';
            }
        } else {
            $data .= " <option value='NA'> Select District</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->district . "'>" . $s->district . "</option>";
            }
        }
        //dd($data);
        return response()->json($data);
    }

    public function getDistrictsTahshil(Request $request)
    {
        $subbrand = DB::table("tahshil")->where("district_id", $request->district_id)->orderBy('tahshil', 'ASC')->get();
        $data = '';
        if ($request->tahshil_id != 0) {
            $data .= " <option value='NA'> Select Tehsil</option>";
            foreach ($subbrand as $s) {
                if ($request->tahshil_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->tahshil . '</option>';
            }
        } else {
            $data .= " <option value='NA'> Select Tehsil</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->tahshil . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }

    public function states()
    {
        $states = State::orderBy('name', 'ASC')->get();
        return view('admin.location_master.states', compact('states'));
    }
    public function createState(Request $request)
    {
        $cat = State::Create([
            // 'stateid' =>  $request->stateid,
            'name' =>  $request->name,
            'country_id' =>  105,
        ]);
        $request->session()->flash('success', 'State Added Successfully');
        return redirect('/admin/states');
    }
    public function editState($id)
    {
        $award = State::find($id);
        return response()->json($award);
    }
    public function updateState(Request $request)
    {
        $stateUpdate = State::find($request->uid);
        //$stateUpdate->stateid =      $request->stateid;
        $stateUpdate->name =     $request->uname;
        $stateUpdate->update();
        return redirect('/admin/states');
    }
    public function deleteState(Request $request, $id)
    {
        State::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/states');
    }

    public function importState(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();

        $customerArr = $this->csvToArray($path);
        // echo "<pre>";
        // print_r($customerArr);
        // exit;
        foreach ($customerArr as $c) {
            //echo $c['brand_id'] . "//" . $c['series_id'] . "</br>";
            $checkExist = State::Where('name',  $c['state'])->first();
            if ($checkExist) {
                $model = State::find($checkExist->id);
                $model->stateid =      $c['stateid'];
                $model->name =      $c['state'];
                $model->update();
            } else {
                $cat = State::Create([
                    'stateid' =>  $c['stateid'],
                    'name' =>  $c['state'],
                    'country_id' =>  105,
                ]);
            }
        }
        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/states');
    }
    public function districts()
    {
        $states = State::all();
        $district = DistrictMaster::leftjoin('states', 'states.id', '=', 'districts.state_id')->orderBy('district', 'ASC')->get([
            'districts.*',
            'states.name as state_title',

        ]);
        return view('admin.location_master.district', compact('district', 'states'));
    }
    public function createDistrict(Request $request)
    {
        $cat = DistrictMaster::Create([
            // 'stateid' =>  $request->stateid,
            'state_id' =>  $request->state_id,
            'district' =>  $request->district,
        ]);
        $request->session()->flash('success', 'State Added Successfully');
        return redirect('/admin/districts');
    }
    public function editDistrict($id)
    {
        $award = DistrictMaster::find($id);
        return response()->json($award);
    }
    public function updateDistrict(Request $request)
    {
        $districtUpdate = DistrictMaster::find($request->uid);
        //$stateUpdate->stateid =      $request->stateid;
        $districtUpdate->state_id =     $request->ustate_id;
        $districtUpdate->district =     $request->udistrict;
        $districtUpdate->update();
        return redirect('/admin/districts');
    }
    public function deleteDistrict(Request $request, $id)
    {

        DistrictMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/districts');
    }

    public function tahshil()
    {
        $district = DistrictMaster::all();
        $states = State::all();
        $tahshil = TahshilMaster::leftjoin('districts', 'districts.id', '=', 'tahshil.district_id')
            ->leftjoin('states', 'states.id', '=', 'districts.state_id')
            ->orderBy('district', 'ASC')->get([
                'tahshil.*',
                'districts.district as district_title',
                'states.name as state_title',

            ]);
        return view('admin.location_master.tahshil', compact('states', 'district', 'tahshil'));
    }
    public function createTahshil(Request $request)
    {
        $cat = TahshilMaster::Create([
            // 'stateid' =>  $request->stateid,
            'district_id' =>  $request->district_id,
            'tahshil' =>  $request->tahshil,
        ]);
        $request->session()->flash('success', 'State Added Successfully');
        return redirect('/admin/tehsil');
    }
    public function editTahshil($id)
    {
        $tahshil = TahshilMaster::find($id);
        $districtData = DistrictMaster::find($tahshil['district_id']);
        $tahshil['state_id'] = $districtData['state_id'];
        //dd($tahshil);
        return response()->json($tahshil);
    }
    public function updateTahshil(Request $request)
    {
        $tahshilUpdate = TahshilMaster::find($request->uid);
        //$stateUpdate->stateid =      $request->stateid;
        $tahshilUpdate->district_id =     $request->udistrict_id;
        $tahshilUpdate->tahshil =     $request->utahshil;
        $tahshilUpdate->update();
        return redirect('/admin/tehsil');
    }
    public function deleteTahshil(Request $request, $id)
    {
        TahshilMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/tehsil');
    }

    public function tahshilajax()
    {
        $district = DistrictMaster::all();
        $states = State::all();
        $tahshil = TahshilMaster::leftjoin('districts', 'districts.id', '=', 'tahshil.district_id')
            ->leftjoin('states', 'states.id', '=', 'districts.state_id')
            ->orderBy('district', 'ASC')->get([
                'tahshil.*',
                'districts.district as district_title',
                'states.name as state_title',

            ]);
        return view('admin.location_master.tahshilAjax', compact('states', 'district', 'tahshil'));
    }
    public function getTahshil(Request $request)
    {

        ## Read value
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Total records
        $totalRecords = TahshilMaster::select('count(*) as allcount')->count();
        $totalRecordswithFilter = TahshilMaster::select('count(*) as allcount')->where('tahshil', 'like', '%' . $searchValue . '%')->count();

        // Fetch records
        $records = TahshilMaster::orderBy($columnName, $columnSortOrder)
            ->where('tahshil.tahshil', 'like', '%' . $searchValue . '%')
            ->where('states.name', 'like', '%' . $searchValue . '%')
            ->where('districts.district', 'like', '%' . $searchValue . '%')
            ->leftjoin('districts', 'districts.id', '=', 'tahshil.district_id')
            ->leftjoin('states', 'states.id', '=', 'districts.state_id')
            ->select([
                'tahshil.*',
                'districts.district as district_title',
                'states.name as state_title',

            ])
            ->skip($start)
            ->take($rowperpage)
            ->get();

        $data_arr = array();

        foreach ($records as $record) {
            $id = $record->id;
            $tahshil = $record->tahshil;
            $state_title = $record->state_title;
            $district_title = $record->district_title;

            $data_arr[] = array(
                "id" => $id,
                "tahshil" => $tahshil,
                "state_title" => $state_title,
                "district_title" => $district_title,
                "action" =>  "<a href='javascript:void(0)' ><i class='fa fa-trash' style='color: #b91010;padding:5px'></i></a>"
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        return response()->json($response);
    }
    public function importDistrict(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();

        $customerArr = $this->csvToArray($path);
        // echo "<pre>";
        // print_r($customerArr);
        // exit;
        foreach ($customerArr as $c) {
            //echo $c['brand_id'] . "//" . $c['series_id'] . "</br>";
            $checkExist = DistrictMaster::Where('district',  $c['district'])->first();
            if ($checkExist) {
                $GetStateName = State::Where('name',  $c['state'])->first();
                if ($GetStateName) {
                    $state_id = $GetStateName->id;
                } else {
                    $state_id = "";
                }
                DistrictMaster::where('id', $checkExist->id)
                    ->update([
                        'state_id' =>  $state_id,
                        'district' =>  $c['district'],
                    ]);
            } else {
                $GetStateName = State::Where('name',  $c['state'])->first();
                if ($GetStateName) {
                    $state_id = $GetStateName->id;
                } else {
                    $state_id = "";
                }
                $cat = DistrictMaster::Create([
                    'state_id' =>   $state_id,
                    'district' =>  $c['district'],
                ]);
            }
        }
        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/districts');
    }
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {

                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
