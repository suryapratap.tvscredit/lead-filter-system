<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TempData;
use App\Models\StateMaster;
use App\Models\State;
use App\Models\DistrictMaster;
use App\Models\FilterData;
use App\Models\DistrictMasterErr;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TempImport;
use App\Models\Brand;
use App\Models\BrandMasterErr;

use DB;

class LeadController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function uploaddump()
    {
        $resultData = TempData::all();
        $platinum = TempData::where('is_state_filter', 1)->where('is_brand_filter', 1)->where('working_district', 1)->count();
        //dd($platinum);
        return view('admin.leads.uploaddump', compact('resultData'));
    }
    public function mapState()
    {
        // $resultData = TempData::all();
        $resultDataState = State::all();
        $resultData = StateMaster::where('state_name', '=', '')->orWhereNull('state_name')->get();
        //dd($checkerrorState);
        return view('admin.leads.mapstate', compact('resultData', 'resultDataState'));
    }
    public function mapStateSubmit(Request $request)
    {

        foreach ($request->input('id') as $i) {
            // echo $request->input("E" . $i);
            // echo "</br>";
            if ($request->input("S" . $i) != "") {
                //echo $request->input("E" . $i) . "////ok //////" . $request->input("S" . $i) . "</br>";
                StateMaster::where('id', $i)->update([
                    'state_name' => $request->input("S" . $i),
                ]);
            }
        }
        // exit;
        $getData = TempData::all();
        foreach ($getData as $g) {
            $checkerrorState = StateMaster::where('error_value', $g->tkstate)->first();
            if ($checkerrorState->state_name != "") {
                $cat = FilterData::Create([
                    'lead_source' => $g->lead_source,
                    'source' => $g->source,
                    'lead_type' => $g->lead_type,
                    'enq_id' => $g->enq_id,
                    'mobile' => $g->mobile,
                    'name' => $g->name,
                    'tkstate' => $checkerrorState->state_name,
                    'tkdistrict' => $g->tkdistrict,
                    'tktehsil' => $g->tktehsil,
                    'tkproduct_category' => $g->tkproduct_category,
                    'tkbrand' => $g->tkbrand,
                    'tkmodel' => $g->tkmodel,
                    'website' => $g->website,
                    'lead_date' => $g->website,
                    'created_on' =>  $g->created_on,
                    'zip' => $g->zip,
                    'pancard' => $g->pancard,
                    'loan_type' => $g->loan_type,
                ]);
                TempData::where('id', $g->id)->delete();
            }
        }
        return back()->with('success', 'Data Mapped Successfully successfully.');
    }
    public function mapDistrict()
    {
        // $resultData = TempData::all();
        $resultDataState = State::all();
        $resultData = DistrictMasterErr::where('district_name', '=', '')->orWhereNull('district_name')->where('error_value', '!=', '')->get();
        //dd($checkerrorState);
        return view('admin.leads.mapdistrict', compact('resultData', 'resultDataState'));
    }

    public function mapDistrictSubmit(Request $request)
    {
        //dd($request->input());
        foreach ($request->input('id') as $i) {
            // echo $request->input("E" . $i);
            // echo "</br>";
            if ($request->input("S" . $i) != "") {
                //echo $request->input("E" . $i) . "////ok //////" . $request->input("D" . $i) . "</br>";

                $statenewentry = DistrictMasterErr::where('id', $i)->update([
                    'district_name' => $request->input("D" . $i),

                ]);
            }
        }
        //exit;
        $getData = FilterData::where('tkstate', '!=', '')->get();
        foreach ($getData as $g) {
            $geterrData = DistrictMasterErr::where('error_value', $g->tkdistrict)->where('state_name', $g->tkstate)->first();
            if ($geterrData) {
                if ($geterrData->district_name != "") {
                    FilterData::where('id', $g->id)->update([
                        'tkdistrict' => $geterrData->district_name,
                        'is_district_filter' => 1,
                    ]);
                }
            }
        }
        return back()->with('success', 'Data Mapped Successfully successfully.');
    }
    public function mapBrand()
    {
        // $resultData = TempData::all();
        $resultBrand = Brand::all();
        $resultData = BrandMasterErr::where('brand_name', '=', '')->orWhereNull('brand_name')->where('error_value', '!=', '')->get();
        //dd($checkerrorState);
        return view('admin.leads.mapbrand', compact('resultData', 'resultBrand'));
    }

    public function mapBrandSubmit(Request $request)
    {
        //dd($request->input());
        foreach ($request->input('id') as $i) {
            // echo $request->input("E" . $i);
            // echo "</br>";
            if ($request->input("B" . $i) != "") {
                //echo $request->input("E" . $i) . "////ok //////" . $request->input("D" . $i) . "</br>";

                $statenewentry = BrandMasterErr::where('id', $i)->update([
                    'brand_name' => $request->input("B" . $i),

                ]);
            }
        }
        //exit;
        $getData = FilterData::where('tkbrand', '!=', '')->get();
        foreach ($getData as $g) {
            $geterrData = BrandMasterErr::where('error_value', $g->tkbrand)->first();
            if ($geterrData) {
                if ($geterrData->brand_name != "") {
                    FilterData::where('id', $g->id)->update([
                        'tkbrand' => $geterrData->brand_name,
                        'is_brand_filter' => 1,
                    ]);
                }
            }
        }
        return back()->with('success', 'Data Mapped Successfully successfully.');
    }
    public function filterData()
    {
        $resultData = FilterData::all();
        return view('admin.leads.filterdata', compact('resultData'));
    }
    function import(Request $request)
    {
        TempData::truncate();
        Excel::import(
            new TempImport,
            $request->file('select_file')->store('temp')
        );
        $getData = TempData::all();
        FilterData::truncate();
        foreach ($getData as $g) {
            //$checkState=StateMaster::where('s')
            //echo $g->tkstate . " ///";

            if ($g->tkstate != "") {
                $checkState = State::where('name', $g->tkstate)->first();
                if ($checkState != "") {
                    $cat = FilterData::Create([
                        'lead_source' => $g->lead_source,
                        'source' => $g->source,
                        'lead_type' => $g->lead_type,
                        'enq_id' => $g->enq_id,
                        'mobile' => $g->mobile,
                        'name' => $g->name,
                        'tkstate' => $checkState->name,
                        'tkdistrict' => $g->tkdistrict,
                        'tktehsil' => $g->tktehsil,
                        'tkproduct_category' => $g->tkproduct_category,
                        'tkbrand' => $g->tkbrand,
                        'tkmodel' => $g->tkmodel,
                        'website' => $g->website,
                        'lead_date' => $g->website,
                        'created_on' =>  $g->created_on,
                        'zip' => $g->zip,
                        'pancard' => $g->pancard,
                        'loan_type' => $g->loan_type,
                    ]);
                    TempData::where('id', $g->id)->delete();
                } else {
                    $checkerrorState = StateMaster::where('error_value', $g->tkstate)->first();
                    if ($checkerrorState != "") {
                        $cat = FilterData::Create([
                            'lead_source' => $g->lead_source,
                            'source' => $g->source,
                            'lead_type' => $g->lead_type,
                            'enq_id' => $g->enq_id,
                            'mobile' => $g->mobile,
                            'name' => $g->name,
                            'tkstate' => $checkerrorState->state_name,
                            'tkdistrict' => $g->tkdistrict,
                            'tktehsil' => $g->tktehsil,
                            'tkproduct_category' => $g->tkproduct_category,
                            'tkbrand' => $g->tkbrand,
                            'tkmodel' => $g->tkmodel,
                            'website' => $g->website,
                            'lead_date' => $g->website,
                            'created_on' =>  $g->created_on,
                            'zip' => $g->zip,
                            'pancard' => $g->pancard,
                            'loan_type' => $g->loan_type,
                        ]);
                        TempData::where('id', $g->id)->delete();
                    } else {
                        TempData::where('id', $g->id)->update([
                            'is_state_filter' => 1,
                            'is_brand_filter' => 1,
                            'working_district' => 1,

                        ]);
                    }
                }
            } else {
                $cat = FilterData::Create([
                    'lead_source' => $g->lead_source,
                    'source' => $g->source,
                    'lead_type' => $g->lead_type,
                    'enq_id' => $g->enq_id,
                    'mobile' => $g->mobile,
                    'name' => $g->name,
                    'tkstate' => $g->state_name,
                    'tkdistrict' => $g->tkdistrict,
                    'tktehsil' => $g->tktehsil,
                    'tkproduct_category' => $g->tkproduct_category,
                    'tkbrand' => $g->tkbrand,
                    'tkmodel' => $g->tkmodel,
                    'website' => $g->website,
                    'lead_date' => $g->website,
                    'created_on' =>  $g->created_on,
                    'zip' => $g->zip,
                    'pancard' => $g->pancard,
                    'loan_type' => $g->loan_type,
                ]);
                TempData::where('id', $g->id)->delete();
            }
        }
        return back()->with('success', 'Excel Data Imported successfully.');
    }
    function importdashboard(Request $request)
    {
        TempData::truncate();
        Excel::import(
            new TempImport,
            $request->file('select_file')->store('temp')
        );
        $getData = TempData::all();
        FilterData::truncate();
        foreach ($getData as $g) {
            //$checkState=StateMaster::where('s')
            //echo $g->tkstate . " ///";

            if ($g->tkstate != "") {
                //if state is not blank
                $checkState = State::where('name', $g->tkstate)->first();
                if ($checkState != "") {
                    $cat = FilterData::Create([
                        'lead_source' => $g->lead_source,
                        'source' => $g->source,
                        'lead_type' => $g->lead_type,
                        'enq_id' => $g->enq_id,
                        'mobile' => $g->mobile,
                        'name' => $g->name,
                        'tkstate' => $checkState->name,
                        'tkdistrict' => $g->tkdistrict,
                        'tktehsil' => $g->tktehsil,
                        'tkproduct_category' => $g->tkproduct_category,
                        'tkbrand' => $g->tkbrand,
                        'tkmodel' => $g->tkmodel,
                        'website' => $g->website,
                        'lead_date' => $g->website,
                        'created_on' =>  $g->created_on,
                        'zip' => $g->zip,
                        'pancard' => $g->pancard,
                        'loan_type' => $g->loan_type,
                    ]);
                    TempData::where('id', $g->id)->delete();
                } else {
                    $checkerrorState = StateMaster::where('error_value', $g->tkstate)->first();
                    if ($checkerrorState) {
                        if ($checkerrorState->state_name != "") {
                            $cat = FilterData::Create([
                                'lead_source' => $g->lead_source,
                                'source' => $g->source,
                                'lead_type' => $g->lead_type,
                                'enq_id' => $g->enq_id,
                                'mobile' => $g->mobile,
                                'name' => $g->name,
                                'tkstate' => $checkerrorState->state_name,
                                'tkdistrict' => $g->tkdistrict,
                                'tktehsil' => $g->tktehsil,
                                'tkproduct_category' => $g->tkproduct_category,
                                'tkbrand' => $g->tkbrand,
                                'tkmodel' => $g->tkmodel,
                                'website' => $g->website,
                                'lead_date' => $g->website,
                                'created_on' =>  $g->created_on,
                                'zip' => $g->zip,
                                'pancard' => $g->pancard,
                                'loan_type' => $g->loan_type,
                                'is_state_filter' => 1,
                                'working_district' => 0,
                            ]);
                            TempData::where('id', $g->id)->delete();
                        }
                    } else {

                        $statenewentry = StateMaster::Create([
                            'error_value' => $g->tkstate,

                        ]);
                        TempData::where('id', $g->id)->update([
                            'is_state_filter' => 1,
                            'is_brand_filter' => 1,
                            'working_district' => 1,

                        ]);
                    }
                }
            } else {
                //if state is blank
                $cat = FilterData::Create([
                    'lead_source' => $g->lead_source,
                    'source' => $g->source,
                    'lead_type' => $g->lead_type,
                    'enq_id' => $g->enq_id,
                    'mobile' => $g->mobile,
                    'name' => $g->name,
                    'tkstate' => $g->state_name,
                    'tkdistrict' => $g->tkdistrict,
                    'tktehsil' => $g->tktehsil,
                    'tkproduct_category' => $g->tkproduct_category,
                    'tkbrand' => $g->tkbrand,
                    'tkmodel' => $g->tkmodel,
                    'website' => $g->website,
                    'lead_date' => $g->website,
                    'created_on' =>  $g->created_on,
                    'zip' => $g->zip,
                    'pancard' => $g->pancard,
                    'loan_type' => $g->loan_type,
                ]);
                TempData::where('id', $g->id)->delete();
            }
        }
        return back()->with('success', 'Excel Data Imported successfully.');
    }
    public function districtsFilter()
    {
        $getData = FilterData::where('tkstate', '!=', '')->get();
        //dd($getData);
        foreach ($getData as $g) {
            $checkState = State::where('name', $g->tkstate)->first();
            //dd($checkState);
            $getDist = DistrictMaster::where('district', $g->tkdistrict)->where('state_id', $checkState->id)->first();
            if ($getDist) {
                FilterData::where('id', $g->id)->update([
                    'is_district_filter' => 1,
                ]);
            } else {
                $geterrData = DistrictMasterErr::where('error_value', $g->tkdistrict)->where('state_name', $g->tkstate)->first();
                if ($geterrData) {
                    if ($geterrData->district_name != "") {
                        FilterData::where('id', $g->id)->update([
                            'tkdistrict' => $geterrData->district_name,
                            'is_district_filter' => 1,
                        ]);
                    }
                } else {
                    if ($g->tkdistrict != "") {
                        $statenewentry = DistrictMasterErr::Create([
                            'state_name' => $g->tkstate,
                            'error_value' => $g->tkdistrict,
                        ]);
                    }
                }
            }
        }
        return back()->with('success', 'District Mapped Succefully successfully.');
    }
    public function brandFilter()
    {
        $getData = FilterData::where('tkbrand', '!=', '')->get();
        //dd($getData);
        foreach ($getData as $g) {
            $checkBrand = Brand::where('brand_name', $g->tkbrand)->first();
            if ($checkBrand) {
                FilterData::where('id', $g->id)->update([
                    'is_district_filter' => 1,
                    'tkbrand' => $checkBrand->brand_name,
                ]);
            } else {
                $geterrData = BrandMasterErr::where('error_value', $g->tkbrand)->first();
                if ($geterrData) {
                    if ($geterrData->brand_name != "") {
                        FilterData::where('id', $g->id)->update([
                            'tkbrand' => $geterrData->brand_name,
                            'is_brand_filter' => 1,
                        ]);
                    }
                } else {
                    if ($g->tkbrand != "") {
                        $brandnewentry = BrandMasterErr::Create([
                            'error_value' => $g->tkbrand,
                        ]);
                    }
                }
            }
        }
        return back()->with('success', 'Brand Mapped Succefully successfully.');
    }
    public function stateFilter(Request $request)
    {
        $getData = TempData::all();
        foreach ($getData as $g) {
            //$checkState=StateMaster::where('s')
            //echo $g->tkstate . " ///";

            if ($g->tkstate != " ") {
                $checkState = State::where('name', $g->tkstate)->first();
                if ($checkState != "") {
                    $cat = FilterData::Create([
                        'lead_source' => $g->lead_source,
                        'source' => $g->source,
                        'lead_type' => $g->lead_type,
                        'enq_id' => $g->enq_id,
                        'mobile' => $g->mobile,
                        'name' => $g->name,
                        'tkstate' => $checkState->name,
                        'tkdistrict' => $g->tkdistrict,
                        'tktehsil' => $g->tktehsil,
                        'tkproduct_category' => $g->tkproduct_category,
                        'tkbrand' => $g->tkbrand,
                        'tkmodel' => $g->tkmodel,
                        'website' => $g->website,
                        'lead_date' => $g->website,
                        'created_on' =>  $g->created_on,
                        'zip' => $g->zip,
                        'pancard' => $g->pancard,
                        'loan_type' => $g->loan_type,
                    ]);
                    TempData::where('id', $g->id)->delete();
                } else {
                    $checkerrorState = StateMaster::where('error_value', $g->tkstate)->first();
                    if ($checkerrorState != "") {
                        $cat = FilterData::Create([
                            'lead_source' => $g->lead_source,
                            'source' => $g->source,
                            'lead_type' => $g->lead_type,
                            'enq_id' => $g->enq_id,
                            'mobile' => $g->mobile,
                            'name' => $g->name,
                            'tkstate' => $checkerrorState->state_name,
                            'tkdistrict' => $g->tkdistrict,
                            'tktehsil' => $g->tktehsil,
                            'tkproduct_category' => $g->tkproduct_category,
                            'tkbrand' => $g->tkbrand,
                            'tkmodel' => $g->tkmodel,
                            'website' => $g->website,
                            'lead_date' => $g->website,
                            'created_on' =>  $g->created_on,
                            'zip' => $g->zip,
                            'pancard' => $g->pancard,
                            'loan_type' => $g->loan_type,
                        ]);
                        TempData::where('id', $g->id)->delete();
                    } else {
                        TempData::where('id', $g->id)->update([
                            'is_state_filter' => 1,
                            'is_brand_filter' => 1,
                            'working_district' => 1,

                        ]);
                    }
                }
            } else {
                $cat = FilterData::Create([
                    'lead_source' => $g->lead_source,
                    'source' => $g->source,
                    'lead_type' => $g->lead_type,
                    'enq_id' => $g->enq_id,
                    'mobile' => $g->mobile,
                    'name' => $g->name,
                    'tkstate' => $g->state_name,
                    'tkdistrict' => $g->tkdistrict,
                    'tktehsil' => $g->tktehsil,
                    'tkproduct_category' => $g->tkproduct_category,
                    'tkbrand' => $g->tkbrand,
                    'tkmodel' => $g->tkmodel,
                    'website' => $g->website,
                    'lead_date' => $g->website,
                    'created_on' =>  $g->created_on,
                    'zip' => $g->zip,
                    'pancard' => $g->pancard,
                    'loan_type' => $g->loan_type,
                ]);
                TempData::where('id', $g->id)->delete();
            }
        }
        return back()->with('success', 'Data filter Successfully Done.');
    }
    public function import1(Request $request)
    {


        $theArray = Excel::toArray(new TempData(), $request->file('select_file'));
        // //$theCollection = Excel::toCollection(collect([]), $request->file('select_file'));
        dd($theArray);
        // foreach ($theArray as $c) {
        //     foreach ($c as $b) {
        //         print_r($b);
        //     }
        // }
        exit;
        // /$customerArr = $this->csvToArray($path);
        // echo "<pre>";
        // print_r($customerArr);
        // exit;
        // foreach ($customerArr as $c) {

        //     $cat = TempData::Create([
        //         'lead_source' =>  $c['Lead Source'],
        //         'source' =>  $c['Source'],
        //         'lead_type' =>  $c['Lead Type'],
        //         'enq_id' =>  $c['Enq ID'],
        //         'mobile' =>  $c['Mobile'],
        //         'name' =>  $c['Name'],
        //         'tkstate' =>  $c['TKState'],
        //         'tkdistrict' =>  $c['TKDistrict'],
        //         'tktehsil' =>  $c['TKTehsil'],
        //         'tkproduct_category' =>  $c['TkProduct Category'],
        //         'tkbrand' =>  $c['TKBrand'],
        //         'tkmodel' =>  $c['TKModel'],
        //         'website' =>  $c['Website'],
        //         'lead_date' =>  date('Y-m-d', strtotime($c['Lead Date'])),
        //         'created_on' => date('Y-m-d', strtotime($c['Created On'])),
        //         'zip' =>  $c['Zip'],
        //         'pancard' =>  $c['Pancard'],
        //         'loan_type' =>  $c['Loan Type'],
        //     ]);
        // }
        $request->session()->flash('success', 'Dump Added Successfully');
        return redirect('admin/upload-dump');
    }

    function csvToArray($filename = '', $delimiter = ',')
    {
        header('Content-Type: text/html; charset=utf-8');
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($checkState->fgetcsv($handle, 1000, $delimiter)) !== false) {

                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
