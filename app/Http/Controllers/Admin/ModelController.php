<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Brand;
use App\Models\Series;
use App\Models\ModelMaster;
use App\Models\Galleryimages;
use App\Models\Year;
use DB;
use Excel;

class ModelController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        //$models = ModelMaster::all();
        $brand = Brand::all();
        $year = Year::all();
        $models = ModelMaster::leftjoin('brands', 'brands.id', '=', 'models.brand_id')
            //->orderBy('models.title', 'ASC')
            // ->orderBy('models.order', 'asc')
            ->orderBy('brands.title', 'asc')
            ->orderBy('models.brand_id', 'asc')
            ->orderBy('models.id', 'asc')


            ->get(['models.*', 'brands.title as brand_title']);
        $brandTotal = [];
        foreach ($models as $m) {
            $brands = ModelMaster::where("brand_id", $m->brand_id)->get();
            $count = $brands->count();
            $brandTotal[$m->id] = $count;
        }
        //dd($brandTotal);
        return view('admin.models.models', compact('models', 'brand', 'year', 'brandTotal'));
    }


    public function add()
    {
        $brand = Brand::all();
        $year = Year::all();
        return view('admin.models.add', compact('brand', 'year'));
    }


    public function create(Request $request)

    {

        $brands = ModelMaster::where("brand_id", $request->brand_id)->get();
        $count = $brands->count();
        $cat = ModelMaster::Create([
            'brand_id' => $request->brand_id,
            'status' => $request->status,
            'year_id' => $request->year_id,
            'title' => $request->title,
            'description' => $request->description,
            'short_description' => $request->short_description,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,
            'order' =>  $count + 1,

        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/models');
    }

    public function edit($id)
    {
        $model = ModelMaster::find($id);
        $brand = Brand::all();
        $year = Year::all();
        return view('admin.models.edit', compact('model', 'brand', 'year'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
        $model = ModelMaster::find($request->id);
        // $filePath = '';

        $model->brand_id =     $request->brand_id;
        $model->year_id =     $request->year_id;
        $model->status =     $request->status;
        $model->title =     $request->title;
        $model->description =     $request->description;
        $model->short_description =     $request->short_description;
        $model->meta_title = $request->meta_title;
        $model->meta_keyword = $request->meta_keyword;
        $model->meta_description = $request->meta_description;
        $model->update();
        // return response()->json($brand);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/models');
    }
    public function delete(Request $request, $id)
    {
        ModelMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/models');
    }


    public function getbrandSerices(Request $request)
    {
        $subbrand = DB::table("series")->where("brand_id", $request->brand_id)->get();
        $data = '';
        if ($request->series_id != 0) {
            $data .= " <option value='NA'> Select series</option>";
            foreach ($subbrand as $s) {
                if ($request->series_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->title . '</option>';
            }
        } else {
            $data .= " <option value='NA'> Select series</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->title . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }

    public function images($id)
    {
        $where = array('model_id' => $id);
        $gallery_id = $id;
        $galleryimages  = Galleryimages::where($where)->get();
        return view('admin.models.images', compact('galleryimages', 'gallery_id'));
        // dd($galleryimages);
    }
    public function deleteimage(Request $request, $id)
    {
        Galleryimages::where('id', $id)->delete();
        // $request->session()->flash('error', 'Deleted Succefully');
        return back()->with('error', ' Deleted Successfully');
    }
    public function createimage(request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            // 'category' => 'required',
        ]);
        // dd($request->all());
        // dd($_FILES);
        // $where = array('title_id' => $id);
        if ($request->hasFile('images')) {
            $allowedfileExtension = ['pdf', 'jpg', 'png', 'docx', 'xlsx', 'xls', 'csv', 'tsv', 'msg', 'jpeg', 'doc', 'txt', 'eml'];
            $files = $request->file('images');
            foreach ($files as $file => $value) {
                $filenamefull = $value->getClientOriginalName();
                $extension = $value->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check == true) {
                    if (in_array($extension, $allowedfileExtension)) {
                        $filename = $value->store('public/images');
                        $f = explode('/', $filename);
                        if (array_key_exists("2", $f)) {
                            Galleryimages::create([
                                'model_id' => $request->id,
                                'images' => $filename,
                            ]);
                        }
                    }
                }
            }
        }
        // return back()->with('Success', 'Succefully Inserted');
        $request->session()->flash('success', 'Successfully Inserted');
        return back();
    }

    public function import1(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();
        // //$data = Excel::import($path, null, Maatwebsite\Excel\Excel::CSV)->get();
        // $data = Excel::toCollection(collect([]), $path);
        // //$data = Excel::toArray(new stdClass(), $path);
        if (($open = fopen($path, "r")) !== FALSE) {

            while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                //echo "<pre>";
                //print_r($data);
                $students[] = $data;
            }

            fclose($open);
        }
        unset($students[0]);
        echo "<pre>";
        // print_r($students);
        foreach ($students as $r) {

            echo $r[0] . $r[1] . "</br>";
        }
    }
    public function import(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();

        $customerArr = $this->csvToArray($path);
        //echo "<pre>";
        // print_r($customerArr);
        foreach ($customerArr as $c) {
            //echo $c['brand_id'] . "//" . $c['series_id'] . "</br>";
            $checkExist = ModelMaster::Where('title',  $c['model'])->first();
            if ($checkExist) {
                $model = ModelMaster::find($checkExist->id);
                $GetBrandName = Brand::Where('title', $c['brand'])->first();
                if ($GetBrandName) {
                    $model->brand_id = $GetBrandName->id;
                } else {
                    $model->brand_id = "";
                }

                $model->status =      "active";
                $model->title =      $c['model'];
                $model->update();
            } else {

                $GetBrandName = Brand::Where('title', $c['brand'])->first();
                if ($GetBrandName) {
                    $brand_id = $GetBrandName->id;
                } else {
                    $brand_id = "";
                }
                $cat = ModelMaster::Create([
                    'brand_id' => $brand_id,
                    'status' => "active",
                    'title' =>  $c['model'],
                ]);
            }
        }
        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/models');
    }
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {

                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    public function sortableData(Request $request)
    {
        //$posts = ModelMaster::where("brand_id", $request->brand_id)->get();
        $posts = ModelMaster::all();
        //dd($request->input());
        foreach ($posts as $post) {
            foreach ($request->order as $order) {
                if ($order['id'] == $post->id) {
                    $model = ModelMaster::where("brand_id", $order['brand_id'])->where("id", $order['id'])->first();
                    $model->order =     $order['position'];
                    $model->update();
                }
            }
        }

        return response('Update Successfully.', 200);
    }

    public function updateOrder(Request $request)
    {
        $checkOrder = ModelMaster::where('brand_id', $request->brand_id)->where('order', $request->neworder)->first();
        if ($checkOrder) {
            ModelMaster::where('id', $checkOrder->id)
                ->update([
                    'order' => $request->current_order,
                ]);
            ModelMaster::where('id', $request->model_id)
                ->update([
                    'order' => $request->neworder,
                ]);
        } else {
            ModelMaster::where('id', $request->model_id)
                ->update([
                    'order' => $request->neworder,
                ]);
        }
        return response()->json("success");
    }

    public function sortall()
    {
        $brands = Brand::all();

        foreach ($brands as $b) {
            $order = 1;
            $brandModels = ModelMaster::where('brand_id', $b->id)->orderby('id', "asc")->get();
            foreach ($brandModels as $m) {
                ModelMaster::where('id', $m->id)
                    ->update([
                        'order' => $order,
                    ]);
                $order = $order + 1;
            }
        }
        echo "sorted";
    }
}
