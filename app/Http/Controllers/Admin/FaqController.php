<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Faq;
use App\Models\FaqPage;
use DB;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class FaqController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $faqpage = FaqPage::all();

        return view('admin.faq.faqpage', compact('faqpage'));
    }
    public function faqPages($id)
    {
        $faq_page_id = $id;
        $faq = Faq::where("faq_page_id", $id)->orderBy('id', 'asc')->get();
        $faq_page = FaqPage::where("id", $id)->first();
        if ($faq->count() > 0) {
            return view('admin.faq.faqedit', compact('faq', 'faq_page'));;
        } else {
            return view('admin.faq.faqadd', compact('faq', 'faq_page'));;
        }
    }

    public function faqUpdate(Request $request)
    {
        $brand = Faq::find($request->id);
        $brand->question = $request->qus;
        $brand->answer = $request->ans;
        $brand->update();
        $data = "success";
        return response()->json($data);
    }
    public function faqDelete(Request $request)
    {
        Faq::where('id', $request->id)->delete();
        $data = "success";
        return response()->json($data);
    }


    public function createFaq(Request $request)
    {

        //dd($_POST);
        //Faq::where('faq_page_id', $request->faq_page_id)->delete();

        if ($request->has('question') && count($request->question) > 0) {

            foreach ($_POST['question'] as $field_type => $key_value) {
                if ($key_value != "") {
                    $faqData = new Faq();
                    $faqData->question = $key_value;
                    $faqData->faq_page_id = $request->faq_page_id;
                    $faqData->answer = $_POST['answer'][$field_type];
                    $faqData->save();
                }
            }
        }

        $request->session()->flash('success', 'Faq added succefully');
        $url = url()->previous();
        return redirect($url);
    }
    public function add()
    {
        return view('admin.brand.add');
    }


    public function create(Request $request)

    {
        // dd($request->all());
        $this->validate($request, [

            // 'brand' => 'required',
        ]);

        $filePath = '';
        if ($request->hasfile('logo')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->logo->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('logo')) {
                    $fileName = time() . '_' . $request->logo->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('logo')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        //dd($filePath);
        $cat = Brand::Create([
            'title' => $request->title,
            'status' => $request->status,
            'description' => $request->description,

            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,
            'logo' => $filePath,
        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/brand');
    }

    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('admin.brand.edit', compact('brand'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
        $brand = Brand::find($request->id);
        // $filePath = '';
        if ($request->hasfile('logo')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->logo->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('logo')) {
                    $fileName = time() . '_' . $request->logo->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('logo')->store('public/upload');
                    $brand->logo = $filePath;
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }



        $brand->title = $request->title;
        $brand->status = $request->status;
        $brand->description = $request->description;

        $brand->meta_title = $request->meta_title;
        $brand->meta_keyword = $request->meta_keyword;
        $brand->meta_description = $request->meta_description;
        $brand->update();
        // return response()->json($brand);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/brand');
    }
    public function delete(Request $request, $id)
    {
        Brand::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/brand');
    }


    public function getsubbrand(Request $request)
    {
        $subbrand = DB::table("subbrand")->where("brand_id", $request->brand_id)->get();
        $data = '';
        if ($request->subbrand_id != 0) {
            $data .= " <option value=''> Select Sub-brand</option>";
            foreach ($subbrand as $s) {
                if ($request->subbrand_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->sub_categories . '</option>';
            }
        } else {
            $data .= " <option value=''> Select Sub-brand</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->sub_categories . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }
    public function seriesDetails($id)
    {
        $brand_id = $id;
        // $sericesDetails = Series::where("brand_id", $id)->get();
        $sericesDetails = Series::leftjoin('brands', 'series.brand_id', '=', 'brands.id')
            ->where("series.brand_id", $id)
            ->get(['series.*', 'brands.title as brand_name']);
        return view('admin.brand.seriesdetails', compact('brand_id', 'sericesDetails'));;
    }

    public function seriesDetailsSubmit(Request $request)
    {
        // dd($request);
        Series::where('brand_id', $request->brand_id)->delete();

        if ($request->has('title') && count($request->title) > 0) {

            foreach ($_POST['title'] as $field_type => $key_value) {
                if ($key_value != "") {
                    $sericesDetails = new Series();
                    $sericesDetails->title = $key_value;
                    $sericesDetails->brand_id = $request->brand_id;
                    $sericesDetails->save();
                }
            }
        }

        $request->session()->flash('success', 'Series added succefully');
        $url = url()->previous();
        return redirect($url);
    }
    public function seriesDetailscreate(Request $request)
    {
        //dd($request->brand_id);
        Series::Create([
            'title' => $request->title,
            'brand_id' => $request->brand_id,
            'description' => $request->description,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,

        ]);
        $request->session()->flash('success', 'Successfully Inserted');
        $url = url()->previous();
        return redirect($url);
    }

    public function seriesDetailsedit($id)
    {
        $series = Series::find($id);
        return response()->json($series);
    }
    public function seriesDetailsupdate(Request $request)
    {
        //dd($request->udescription);
        Series::where('id', $request->uid)->update([
            'title' => $request->utitle,
            'brand_id' => $request->ubrand_id,
            'description' => $request->udescription,
            'meta_title' => $request->umeta_title,
            'meta_keyword' => $request->umeta_keyword,
            'meta_description' => $request->umeta_description,
        ]);
        // return response()->json();
        $request->session()->flash('success', 'Successfully Updated');
        $url = url()->previous();
        return redirect($url);
    }
    public function seriesDetailsdelete(Request $request, $id)
    {
        Series::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        $url = url()->previous();
        return redirect($url);
    }

    public function import(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();

        $customerArr = $this->csvToArray($path);
        //echo "<pre>";
        // print_r($customerArr);
        foreach ($customerArr as $c) {
            $GetBrandName = Brand::Where('title',  $c['brand'])->first();
            if ($GetBrandName) {
                $GetBrandName->status =      "active";
                $GetBrandName->title =      $c['title'];
                $GetBrandName->update();
            } else {
                $cat = Brand::Create([
                    'status' => "active",
                    'title' =>  $c['brand'],
                    'logo' =>  'public/upload/' . $c['logo'],
                ]);
            }
        }
        $request->session()->flash('success', 'Added Successfully');
        return redirect('admin/brand');
    }
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {

                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
