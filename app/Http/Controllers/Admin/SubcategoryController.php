<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Subcategory;
use DB;

class SubcategoryController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $subcategory = Subcategory::leftjoin('category', 'subcategory.category_id', '=', 'category.id')
            ->get(['subcategory.*', 'category.category']);
        $category = Category::all();
        return view('admin.subcategory.subcategory', compact('subcategory', 'category'));
    }

    public function add()
    {

        $subcategory = Subcategory::leftjoin('category', 'subcategory.category_id', '=', 'category.id')
            ->get(['subcategory.*', 'category.category']);
        $category = Category::all();
        return view('admin.subcategory.add', compact('subcategory', 'category'));
    }


    public function create(Request $request)

    {

        $this->validate($request, [

            'category' => 'required',
            'subcategory' => 'required',
            'status' => 'required',
        ]);
        $filePath = '';
        if ($request->hasfile('menuicon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->menuicon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('menuicon')) {
                    $fileName = time() . '_' . $request->menuicon->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('menuicon')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        $filePath1 = '';
        if ($request->hasfile('image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('image')) {
                    $fileName = time() . '_' . $request->image->getClientOriginalName();
                    // $filePath1 = resize(300, 300);
                    $filePath1 = $request->file('image')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        $filePath2 = '';
        if ($request->hasfile('caticon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->caticon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('caticon')) {
                    $fileName = time() . '_' . $request->caticon->getClientOriginalName();
                    // $filePath2 = resize(300, 300);
                    $filePath2 = $request->file('caticon')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        Subcategory::Create([
            'subcategory' => $request->subcategory,
            'category_id' => $request->category,
            'status' => $request->status,
            'sub_description' => $request->sub_description,
            'sub_description2' => $request->sub_description2,
            'type' => $request->type,
            'content' => $request->content,
            'title' => $request->title,
            'caticon' => $filePath2,
            'image' => $filePath1,
            'menuicon' => $filePath,
            'banner_titile' => $request->banner_titile,
        ]);
        // return response()->json(response());
        $request->session()->flash('success', 'Added Successfully',);
        return redirect("admin/subcategory");
    }

    public function edit($id)
    {
        $subcategory = Subcategory::find($id);
        $category = Category::all();
        return view('admin.subcategory.edit', compact('subcategory', 'category'));
        // return response()->json($subcategory);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'subcategory' => 'required',
            'category' => 'required',

        ]);
        $subcategoryData = Subcategory::where('id', $request->id)->first();

        if ($request->hasfile('menuicon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->menuicon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('menuicon')) {
                    $fileName = time() . '_' . $request->menuicon->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('menuicon')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        } else {
            $filePath = $subcategoryData->menuicon;
        }

        if ($request->hasfile('image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('image')) {
                    $fileName = time() . '_' . $request->image->getClientOriginalName();
                    // $filePath1 = resize(300, 300);
                    $filePath1 = $request->file('image')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        } else {
            $filePath1 = $subcategoryData->image;
        }
        $filePath2 = '';
        if ($request->hasfile('caticon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->caticon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('caticon')) {
                    $fileName = time() . '_' . $request->caticon->getClientOriginalName();
                    // $filePath2 = resize(300, 300);
                    $filePath2 = $request->file('caticon')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        } else {
            $filePath2 = $subcategoryData->caticon;
        }

        $subcategory = Subcategory::find($request->id);
        $subcategory->subcategory = $request->subcategory;
        $subcategory->category_id = $request->category;
        $subcategory->status = $request->status;
        $subcategory->sub_description = $request->sub_description;
        $subcategory->sub_description2 = $request->sub_description2;
        $subcategory->type = $request->type;
        $subcategory->content = $request->content;
        $subcategory->title = $request->title;
        $subcategory->banner_titile = $request->banner_titile;
        $subcategory->caticon = $filePath2;
        $subcategory->image = $filePath1;
        $subcategory->menuicon = $filePath;

        $subcategory->update();
        $request->session()->flash('success', 'Updated Successfully',);
        return redirect('/admin/subcategory');
    }
    public function delete(Request $request, $id)
    {
        Subcategory::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/subcategory');
    }


    public function getsubcategory(Request $request)
    {
        $subcategory = DB::table("subcategory")->where("category_id", $request->category)->get();
        $data = '';
        if ($request->subcategory_id != 0) {

            $data .= " <option value='0'> Select Sub-Category</option>";
            foreach ($subcategory as $s) {
                if ($request->subcategory_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->subcategory . '</option>';
            }
        } else {

            $data .= " <option> Select Sub-Category</option>";
            foreach ($subcategory as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->subcategory . "</option>";
            }
        }
        return response()->json($data);
    }
}
