<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Brand;
use App\Models\OwnerMaster;
use DB;

class OwnerMasterController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $owners = OwnerMaster::orderBy('order', 'ASC')->get();

        return view('admin.owners_master.index', compact('owners'));
    }


    public function add()
    {
        return view('admin.owners_master.add');
    }


    public function create(Request $request)

    {
        // dd($request->all());
        $this->validate($request, [

            // 'brand' => 'required',
        ]);


        $cat = OwnerMaster::Create([
            'title' => $request->title,
        ]);
        // dd($cat);
        // return response()->json(response());
        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/owners_master');
    }

    public function edit($id)
    {
        $owner = OwnerMaster::find($id);
        return view('admin.owners_master.edit', compact('owner'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
        $owner = OwnerMaster::find($request->id);



        $owner->title = $request->title;

        $owner->update();
        // return response()->json($brand);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/owners_master');
    }
    public function delete(Request $request, $id)
    {
        OwnerMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/owners_master');
    }


    public function sortableData(Request $request)
    {
        $posts = OwnerMaster::all();
        //dd($request->input());
        foreach ($posts as $post) {
            foreach ($request->order as $order) {
                if ($order['id'] == $post->id) {
                    $post->update(['order' => $order['position']]);
                }
            }
        }

        return response('Update Successfully.', 200);
    }
}
