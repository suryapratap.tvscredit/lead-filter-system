<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\TempData;
use App\Models\StateMaster;
use App\Models\FilterData;
use App\Models\DistrictMasterErr;
use App\Models\BrandMasterErr;

use DB;


class UserController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function home()
    {
        $users = User::all();
        $notmatchedCount = TempData::count();
        //$notmatchedCount = TempData::where('is_state_filter', 1)->where('is_brand_filter', 1)->where('working_district', 1)->count();
        //dd($notmatchedCount);

        $matchedCount = FilterData::count();
        $statenotmatchedCount = StateMaster::where('state_name', '=', null)->count();
        $districtnotmatchedCount = DistrictMasterErr::where('district_name', '=', null)->count();
        $brandnotmatchedCount = BrandMasterErr::where('brand_name', '=', null)->count();
        //dd($statenotmatchedCount);
        return view('admin.dashboard', compact('users', 'notmatchedCount', 'matchedCount', 'statenotmatchedCount', 'districtnotmatchedCount', 'brandnotmatchedCount'));
        // return view('admin.dashboard', compact('user'));
    }


    public function list()
    {
        $user = User::all();
        return view('admin.users.usermanagement', compact('user'));
    }

    public function add(request $id)
    {

        $type = Auth::user()->role;

        if ($type == 'agent') {
            session()->flash('error', 'You are not authorise for this operation',);
            return redirect('/agent');
        }
        $where = array('id' =>  Auth::user()->id);
        $users  = User::where($where)->first();

        return view('admin.users.add', compact('users'));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',

            'password' => 'required',
            'status' => 'required',
        ]);

        User::Create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,

            'password' => Hash::make($request->password),
            'status' => $request->status,
        ]);
        $request->session()->flash('success', 'User Added Successfully');
        return redirect('/admin/user');
    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $users  = User::where($where)->first();

        return view('admin.users.edit')->with(compact('users'));
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',

            'status' => 'required',

        ]);
        if ($request->password) {
            User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,

                'role' => $request->role,
                'status' => $request->status,
                'password' => Hash::make($request->password),
            ]);
            $request->session()->flash('success', 'User Updated Successfully');
            return redirect('/admin/user');
        } else {
            User::where('id', $request->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'role' => $request->role,
                'status' => $request->status,

            ]);
            $request->session()->flash('success', 'User Updated Successfully');
            return redirect('/admin/user');
        }
    }
    public function delete(Request $request, $id)
    {
        User::where('id', $id)->delete();
        $request->session()->flash('error', 'User Deleted Successfully');
        return redirect('/admin/user');
    }
}
