<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Page;
use DB;

class PageController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }


    public function add()
    {
        return view('admin.pages.add');
    }


    public function create(Request $request)

    {

        $page = Page::Create([
            'title' => $request->title,
            'status' => $request->status,
            'description' => $request->description,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,

        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/pages');
    }

    public function edit($id)
    {
        $pages = Page::find($id);
        return view('admin.pages.edit', compact('pages'));
        // return response()->json($category);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
        $page = Page::find($request->id);
        // $filePath = '';

        $page->title = $request->title;
        $page->description = $request->description;
        $page->status = $request->status;
        $page->meta_title = $request->meta_title;
        $page->meta_keyword = $request->meta_keyword;
        $page->meta_description = $request->meta_description;

        $page->update();
        // return response()->json($category);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/pages');
    }
    public function delete(Request $request, $id)
    {
        Page::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/pages');
    }
}
