<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Subcategory;
use DB;

class CategoryController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $category = Category::all();

        return view('admin.category.category', compact('category'));
    }


    public function add()
    {
        return view('admin.category.add');
    }


    public function create(Request $request)

    {
        // dd($request->all());
        $this->validate($request, [

            // 'category' => 'required',
        ]);

        $filePath = '';
        if ($request->hasfile('menuicon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->menuicon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('menuicon')) {
                    $fileName = time() . '_' . $request->menuicon->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('menuicon')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        $filePath1 = '';
        if ($request->hasfile('image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('image')) {
                    $fileName = time() . '_' . $request->image->getClientOriginalName();
                    // $filePath1 = resize(300, 300);
                    $filePath1 = $request->file('image')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        $filePath2 = '';
        if ($request->hasfile('caticon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->caticon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('caticon')) {
                    $fileName = time() . '_' . $request->caticon->getClientOriginalName();
                    // $filePath2 = resize(300, 300);
                    $filePath2 = $request->file('caticon')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }

        $filePath3 = '';
        if ($request->hasfile('bannerimage')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->bannerimage->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('bannerimage')) {
                    $fileName = time() . '_' . $request->bannerimage->getClientOriginalName();
                    // $filePath3 = resize(300, 300);
                    $filePath3 = $request->file('bannerimage')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        $cat = Category::Create([
            'category' => $request->category,
            'status' => $request->status,
            'description' => $request->description,
            'description2' => $request->description2,
            'sub_title' => $request->sub_title,
            'type' => $request->type,
            'content' => $request->content,
            'title' => $request->title,
            'banner_titile' => $request->banner_titile,
            'section2_content' => $request->section2_content,
            'caticon' => $filePath2,
            'image' => $filePath1,
            'menuicon' => $filePath,
            'bannerimage' => $filePath3,

        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/category');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.category.edit', compact('category'));
        // return response()->json($category);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',

        ]);
        $category = Category::find($request->id);
        // $filePath = '';
        if ($request->hasfile('menuicon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->menuicon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('menuicon')) {
                    $fileName = time() . '_' . $request->menuicon->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('menuicon')->store('public/upload');
                    $category->menuicon = $filePath;
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        // $filePath1 = '';
        if ($request->hasfile('image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('image')) {
                    $fileName = time() . '_' . $request->image->getClientOriginalName();
                    // $filePath1 = resize(300, 300);
                    $filePath1 = $request->file('image')->store('public/upload');
                    $category->image = $filePath1;
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        // $filePath2 = '';
        if ($request->hasfile('caticon')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->caticon->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('caticon')) {
                    $fileName = time() . '_' . $request->caticon->getClientOriginalName();
                    // $filePath2 = resize(300, 300);
                    $filePath2 = $request->file('caticon')->store('public/upload');
                    $category->caticon = $filePath2;
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        // $filePath3 = '';
        if ($request->hasfile('bannerimage')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->bannerimage->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('bannerimage')) {
                    $fileName = time() . '_' . $request->bannerimage->getClientOriginalName();
                    // $filePath3 = resize(300, 300);
                    $filePath3 = $request->file('bannerimage')->store('public/upload');
                    $category->bannerimage = $filePath3;
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }

        $category->category = $request->category;
        $category->status = $request->status;
        $category->description = $request->description;
        $category->description2 = $request->description2;
        $category->type = $request->type;
        $category->content = $request->content;
        $category->title = $request->title;
        $category->sub_title = $request->sub_title;
        $category->banner_titile = $request->banner_titile;
        $category->section2_content = $request->section2_content;

        $category->update();
        // return response()->json($category);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/category');
    }
    public function delete(Request $request, $id)
    {
        Category::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/category');
    }


    public function getsubcategory(Request $request)
    {
        $subcategory = DB::table("subcategory")->where("category_id", $request->category_id)->get();
        $data = '';
        if ($request->subcategory_id != 0) {
            $data .= " <option value=''> Select Sub-Category</option>";
            foreach ($subcategory as $s) {
                if ($request->subcategory_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->sub_categories . '</option>';
            }
        } else {
            $data .= " <option value=''> Select Sub-Category</option>";
            foreach ($subcategory as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->sub_categories . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }
}
