<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Brand;
use App\Models\HourMaster;
use DB;

class HoursMasterController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $hours = HourMaster::all();

        return view('admin.hours_master.index', compact('hours'));
    }


    public function add()
    {
        return view('admin.hours_master.add');
    }


    public function create(Request $request)

    {
        // dd($request->all());
        $this->validate($request, [

            // 'brand' => 'required',
        ]);

     
        $cat = HourMaster::Create([
            'title' => $request->title,
        ]);
        // dd($cat);
        // return response()->json(response());
        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/hours_master');
    }

    public function edit($id)
    {
        $hour = HourMaster::find($id);
        return view('admin.hours_master.edit', compact('hour'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
        $hour = HourMaster::find($request->id);
       


        $hour->title = $request->title;
       
        $hour->update();
        // return response()->json($brand);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/hours_master');
    }
    public function delete(Request $request, $id)
    {
        HourMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/hours_master');
    }


   
   
}
