<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Models\StateMaster;
use App\Models\State;

use DB;

class StateMasterController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $statemaster = StateMaster::all();

        return view('admin.statemaster.statemaster', compact('statemaster'));
    }


    public function add()
    {
        $resultDataState = State::all();
        return view('admin.statemaster.add', compact('resultDataState'));
    }


    public function create(Request $request)

    {
        // dd($request->all());
        $this->validate($request, [

            // 'brand' => 'required',
        ]);

        $cat = StateMaster::Create([
            'state_name' => $request->state_name,
            'error_value' => $request->error_value,

        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/statemaster');
    }

    public function edit($id)
    {
        $statemaster = StateMaster::find($id);
        $resultDataState = State::all();
        return view('admin.statemaster.edit', compact('statemaster', 'resultDataState'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        //dd($request->input());
        $statemaster = StateMaster::find($request->id);

        $statemaster->state_name = $request->state_name;
        $statemaster->error_value = $request->error_value;

        $statemaster->update();

        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/statemaster');
    }
    public function delete(Request $request, $id)
    {
        StateMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/statemaster');
    }

    public function deleteemptystate(Request $request)
    {
        //$s = StateMaster::where('state_name', Null)->get();
        StateMaster::where('state_name', Null)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/statemaster');
    }


    public function getsubbrand(Request $request)
    {
        $subbrand = DB::table("subbrand")->where("brand_id", $request->brand_id)->get();
        $data = '';
        if ($request->subbrand_id != 0) {
            $data .= " <option value=''> Select Sub-brand</option>";
            foreach ($subbrand as $s) {
                if ($request->subbrand_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->sub_categories . '</option>';
            }
        } else {
            $data .= " <option value=''> Select Sub-brand</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->sub_categories . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }
    public function seriesDetails($id)
    {
        $brand_id = $id;
        // $sericesDetails = Series::where("brand_id", $id)->get();
        $sericesDetails = Series::leftjoin('brands', 'series.brand_id', '=', 'brands.id')
            ->where("series.brand_id", $id)
            ->get(['series.*', 'brands.title as brand_name']);
        return view('admin.brand.seriesdetails', compact('brand_id', 'sericesDetails'));;
    }

    public function seriesDetailsSubmit(Request $request)
    {
        // dd($request);
        Series::where('brand_id', $request->brand_id)->delete();

        if ($request->has('title') && count($request->title) > 0) {

            foreach ($_POST['title'] as $field_type => $key_value) {
                if ($key_value != "") {
                    $sericesDetails = new Series();
                    $sericesDetails->title = $key_value;
                    $sericesDetails->brand_id = $request->brand_id;
                    $sericesDetails->save();
                }
            }
        }

        $request->session()->flash('success', 'Series added succefully');
        $url = url()->previous();
        return redirect($url);
    }
    public function seriesDetailscreate(Request $request)
    {
        //dd($request->brand_id);
        Series::Create([
            'title' => $request->title,
            'brand_id' => $request->brand_id,
            'description' => $request->description,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,

        ]);
        $request->session()->flash('success', 'Successfully Inserted');
        $url = url()->previous();
        return redirect($url);
    }

    public function seriesDetailsedit($id)
    {
        $series = Series::find($id);
        return response()->json($series);
    }
    public function seriesDetailsupdate(Request $request)
    {
        //dd($request->udescription);
        Series::where('id', $request->uid)->update([
            'title' => $request->utitle,
            'brand_id' => $request->ubrand_id,
            'description' => $request->udescription,
            'meta_title' => $request->umeta_title,
            'meta_keyword' => $request->umeta_keyword,
            'meta_description' => $request->umeta_description,
        ]);
        // return response()->json();
        $request->session()->flash('success', 'Successfully Updated');
        $url = url()->previous();
        return redirect($url);
    }
    public function seriesDetailsdelete(Request $request, $id)
    {
        Series::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        $url = url()->previous();
        return redirect($url);
    }

    public function import(Request $request)
    {
        $path = $request->file('select_file')->getRealPath();

        $customerArr = $this->csvToArray($path);
        //echo "<pre>";
        // print_r($customerArr);
        foreach ($customerArr as $c) {
            $GetBrandName = Brand::Where('title',  $c['brand'])->first();
            if ($GetBrandName) {
                $GetBrandName->status =      "active";
                $GetBrandName->title =      $c['title'];
                $GetBrandName->update();
            } else {
                $cat = Brand::Create([
                    'status' => "active",
                    'title' =>  $c['brand'],
                    'logo' =>  'public/upload/' . $c['logo'],
                ]);
            }
        }
        $request->session()->flash('success', 'Added Successfully');
        return redirect('admin/brand');
    }
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {

                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
