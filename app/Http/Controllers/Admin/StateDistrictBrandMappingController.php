<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Models\StateMaster;
use App\Models\State;
use App\Models\StateDistrictBrandMapping;
use App\Models\Brand;

use DB;

class StateDistrictBrandMappingController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $statedistrictbrandmapping = StateDistrictBrandMapping::all();

        return view('admin.statedistrictbrandmapping.statedistrictbrandmapping', compact('statedistrictbrandmapping'));
    }


    public function add()
    {
        $resultDataState = State::all();
        $brands = Brand::get();

        return view('admin.statedistrictbrandmapping.add', compact('resultDataState', 'brands'));
    }


    public function create(Request $request)

    {
        // dd($request->all());
        $this->validate($request, [

            // 'brand' => 'required',
        ]);
        $district = explode(',', $request->district);
        $brand = explode(',', $request->brand);

        $cat = StateDistrictBrandMapping::Create([
            'states' => $request->states,
            'district' => $request->district,
            'brand' => $request->brand,

        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/state-district-brand-mapping');
    }

    public function edit($id)
    {
        $statedistrictbrandmapping = StateDistrictBrandMapping::find($id);
        $resultDataState = State::all();
        $brands = Brand::get();
        return view('admin.statedistrictbrandmapping.edit', compact('statedistrictbrandmapping', 'resultDataState', 'brands'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        //dd($request->input());
        $statemaster = StateMaster::find($request->id);

        $statemaster->state_name = $request->state_name;
        $statemaster->error_value = $request->error_value;

        $statemaster->update();

        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/state-district-brand-mapping');
    }
    public function delete(Request $request, $id)
    {
        StateMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/state-district-brand-mapping');
    }
}
