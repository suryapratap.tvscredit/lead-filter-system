<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Brand;
use App\Models\Series;
use App\Models\Year;
use DB;

class SeriesController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        //$series = Series::leftjoin('brands', 'brands.id', '=', 'series.brand_id')->get(['series.*', 'brands.title as brand_title']);
        $series = Series::all();

        return view('admin.series.series', compact('series'));
    }


    public function add()
    {
        $brand = Brand::all();
        $years = Year::all();
        return view('admin.series.add',compact('brand','years'));
    }


    public function create(Request $request)

    {
       
        $cat = Series::Create([
            'title' => $request->title,
            'status' => $request->status,
            'description' => $request->description,
            'brand_id' => $request->brand_id,
            'year_id' => $request->year_id,
            'meta_title' => $request->meta_title,
            'meta_keyword' => $request->meta_keyword,
            'meta_description' => $request->meta_description,
        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/series');
    }

    public function edit($id)
    {
        //dd($id);
        $series = Series::where('id',$id)->first();
       // dd( $series);
        $brand = Brand::all();
        $years = Year::all();
        return view('admin.series.edit', compact('brand','series','years'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        $series = Series::find($request->id);
        $series->title = $request->title;
        $series->status = $request->status;
        $series->description = $request->description;
        $series->brand_id = $request->brand_id;
        $series->year_id = $request->year_id;
       
        $series->meta_title = $request->meta_title;
        $series->meta_keyword = $request->meta_keyword;
        $series->meta_description = $request->meta_description;
        $series->update();
        // return response()->json($brand);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/series');
    }
    public function delete(Request $request, $id)
    {
        Series::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/series');
    }


   
}
