<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductMaster;
use App\Models\Brand;
use App\Models\Series;
use App\Models\ModelMaster;
use App\Models\Galleryimages;
use App\Models\Implement;
use DB;

class ImpelmentsController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function list()
    {
        $implement = Implement::all();

        return view('admin.implements.implement', compact('implement'));
    }


    public function add()
    {
        $brand = Brand::all();
        return view('admin.implements.add', compact('brand'));
    }


    public function create(Request $request)

    {

        $filePath = '';
        if ($request->hasfile('primary_image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->primary_image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('primary_image')) {
                    $fileName = time() . '_' . $request->primary_image->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('primary_image')->store('public/upload');
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        //dd($filePath);
        $cat = ModelMaster::Create([
            'primary_image' => $filePath,
            'brand_id' => $request->brand_id,
            'status' => $request->status,
            'model_type' => $request->model_type,
            'series_id' => $request->series_id,
            'title' => $request->title,
            'description' => $request->description,
            'short_description' => $request->short_description,
            'noofcylinder' => $request->noofcylinder,
            'hp_category' => $request->hp_category,
            'engine_type' => $request->engine_type,
            'engine_rated_rpm' => $request->engine_rated_rpm,
            'max_torque' => $request->max_torque,
            'capacity_cc' => $request->capacity_cc,
            'air_filter' => $request->air_filter,
            'cooling_system' => $request->cooling_system,
            'fule_type' => $request->fule_type,
            'bore_stroke' => $request->bore_stroke,
            'fule_pump_type' => $request->fule_pump_type,
            'emission_standard' => $request->emission_standard,
            'clutch' => $request->clutch,
            'gearbox' => $request->gearbox,
            'gear_speed' => $request->gear_speed,
            'forward_speed' => $request->forward_speed,
            'reverse_speed' => $request->reverse_speed,
            'gear_lever_position' => $request->gear_lever_position,
            'brakes' => $request->brakes,
            'rear_axle' => $request->rear_axle,
            'steering_type' => $request->steering_type,
            'steering_adjustment' => $request->steering_adjustment,
            'pto_hp' => $request->pto_hp,
            'pto_speed' => $request->pto_speed,
            'rpm' => $request->rpm,
            'fule_capacity' => $request->fule_capacity,
            'lifting_capacity' => $request->lifting_capacity,
            'point3_linkage' => $request->point3_linkage,
            'hydraulic_controls' => $request->hydraulic_controls,
            'remote_auxillaryvalve' => $request->remote_auxillaryvalve,
            'wheel_drive' => $request->wheel_drive,
            'tyre_front' => $request->tyre_front,
            'tyre_rear' => $request->tyre_rear,
            'total_weight' => $request->total_weight,
            'wheel_base' => $request->wheel_base,
            'overall_length' => $request->overall_length,
            'overall_width' => $request->overall_width,
            'ground_clearance' => $request->ground_clearance,
            'turning_radius_with_brake' => $request->turning_radius_with_brake,
            'battery' => $request->battery,
            'alternator' => $request->alternator,
            'neutral_safety_switch' => $request->neutral_safety_switch,
            'differential_lock' => $request->differential_lock,
            'rops' => $request->rops,
            'clutch_safety_lock' => $request->clutch_safety_lock,
            'warranty' => $request->warranty,
            'instrument_cluster' => $request->instrument_cluster,
            'platform' => $request->platform,
            'driver_seat' => $request->driver_seat,
            'accessories' => $request->accessories,
            'additional_features' => $request->additional_features,
            'is_new' => $request->is_new,
            'is_populer' => $request->is_populer,
            'is_upcoming' => $request->is_upcoming,
            'is_mini_tractor' => $request->is_mini_tractor,
            'is_upcoming' => $request->is_upcoming,
            'is_4wd_tractor' => $request->is_4wd_tractor,
            'is_ac_cabin_tractor' => $request->is_ac_cabin_tractor,

        ]);
        // dd($cat);
        // return response()->json(response());

        $request->session()->flash('success', 'Added Successfully');
        return redirect('/admin/models');
    }

    public function edit($id)
    {
        $model = ModelMaster::find($id);
        $brand = Brand::all();
        // echo $model->series_id;
        // exit;
        return view('admin.models.edit', compact('model', 'brand'));
        // return response()->json($brand);
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',

        ]);
        $model = ModelMaster::find($request->id);
        // $filePath = '';
        if ($request->hasfile('primary_image')) {
            $allowedextention = ['jpeg', 'png', 'jpg'];
            $extention = $request->primary_image->getClientOriginalExtension();
            $check =  in_array($extention, $allowedextention);
            if ($check) {
                if ($request->file('primary_image')) {
                    $fileName = time() . '_' . $request->primary_image->getClientOriginalName();
                    // $filePath = resize(300, 300);
                    $filePath = $request->file('primary_image')->store('public/upload');
                    $model->primary_image = $filePath;
                }
            } else {
                $request->session()->flash('error', 'Only jpeg,jpg,png are allowed');
                // return redirect('/walloffame/add');
            }
        }
        $model->brand_id =     $request->brand_id;
        $model->series_id =     $request->series_id;
        $model->status =     $request->status;
        $model->title =     $request->title;
        $model->description =     $request->description;
        $model->short_description =     $request->short_description;
        $model->noofcylinder =     $request->noofcylinder;
        $model->hp_category =     $request->hp_category;
        $model->engine_type =     $request->engine_type;
        $model->engine_rated_rpm =     $request->engine_rated_rpm;
        $model->max_torque =     $request->max_torque;
        $model->capacity_cc =     $request->capacity_cc;
        $model->air_filter =     $request->air_filter;
        $model->cooling_system =     $request->cooling_system;
        $model->fule_type =     $request->fule_type;
        $model->bore_stroke =     $request->bore_stroke;
        $model->fule_pump_type =     $request->fule_pump_type;
        $model->emission_standard =     $request->emission_standard;
        $model->clutch =     $request->clutch;
        $model->gearbox =     $request->gearbox;
        $model->gear_speed =     $request->gear_speed;
        $model->forward_speed =     $request->forward_speed;
        $model->reverse_speed =     $request->reverse_speed;
        $model->gear_lever_position =     $request->gear_lever_position;
        $model->brakes =     $request->brakes;
        $model->rear_axle =     $request->rear_axle;
        $model->steering_type =     $request->steering_type;
        $model->steering_adjustment =     $request->steering_adjustment;
        $model->pto_hp =     $request->pto_hp;
        $model->pto_speed =     $request->pto_speed;
        $model->rpm =     $request->rpm;
        $model->fule_capacity =     $request->fule_capacity;
        $model->lifting_capacity =     $request->lifting_capacity;
        $model->point3_linkage =     $request->point3_linkage;
        $model->hydraulic_controls =     $request->hydraulic_controls;
        $model->remote_auxillaryvalve =     $request->remote_auxillaryvalve;
        $model->wheel_drive =     $request->wheel_drive;
        $model->tyre_front =     $request->tyre_front;
        $model->tyre_rear =     $request->tyre_rear;
        $model->total_weight =     $request->total_weight;
        $model->wheel_base =     $request->wheel_base;
        $model->overall_length =     $request->overall_length;
        $model->overall_width =     $request->overall_width;
        $model->ground_clearance =     $request->ground_clearance;
        $model->turning_radius_with_brake =     $request->turning_radius_with_brake;
        $model->battery =     $request->battery;
        $model->alternator =     $request->alternator;
        $model->neutral_safety_switch =     $request->neutral_safety_switch;
        $model->differential_lock =     $request->differential_lock;
        $model->rops =     $request->rops;
        $model->clutch_safety_lock =     $request->clutch_safety_lock;
        $model->warranty =     $request->warranty;
        $model->instrument_cluster =     $request->instrument_cluster;
        $model->platform =     $request->platform;
        $model->driver_seat =     $request->driver_seat;
        $model->accessories =     $request->accessories;
        $model->additional_features =     $request->additional_features;
        $model->is_new =     $request->is_new;
        $model->is_populer =     $request->is_populer;
        $model->is_upcoming =     $request->is_upcoming;
        $model->is_mini_tractor =     $request->is_mini_tractor;
        $model->is_upcoming =     $request->is_upcoming;
        $model->is_4wd_tractor =     $request->is_4wd_tractor;
        $model->is_ac_cabin_tractor =     $request->is_ac_cabin_tractor;
        $model->update();
        // return response()->json($brand);
        $request->session()->flash('success', 'Updated Successfully');
        return redirect('/admin/models');
    }
    public function delete(Request $request, $id)
    {
        ModelMaster::where('id', $id)->delete();
        $request->session()->flash('error', 'Deleted Successfully');
        return redirect('/admin/models');
    }


    public function getbrandSerices(Request $request)
    {
        $subbrand = DB::table("series")->where("brand_id", $request->brand_id)->get();
        $data = '';
        if ($request->series_id != 0) {
            $data .= " <option value='NA'> Select series</option>";
            foreach ($subbrand as $s) {
                if ($request->series_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->title . '</option>';
            }
        } else {
            $data .= " <option value='NA'> Select series</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->title . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }
    public function seriesDetails($id)
    {
        $brand_id = $id;
        $sericesDetails = Series::where("brand_id", $id)->get();
        return view('admin.brand.seriesdetails', compact('brand_id', 'sericesDetails'));;
    }

    public function seriesDetailsSubmit(Request $request)
    {
        // dd($request);
        Series::where('brand_id', $request->brand_id)->delete();

        if ($request->has('title') && count($request->title) > 0) {

            foreach ($_POST['title'] as $field_type => $key_value) {
                if ($key_value != "") {
                    $sericesDetails = new Series();
                    $sericesDetails->title = $key_value;
                    $sericesDetails->brand_id = $request->brand_id;
                    $sericesDetails->save();
                }
            }
        }

        $request->session()->flash('success', 'Series added succefully');
        $url = url()->previous();
        return redirect($url);
    }
    public function images($id)
    {
        $where = array('model_id' => $id);
        $gallery_id = $id;
        $galleryimages  = Galleryimages::where($where)->get();
        return view('admin.models.images', compact('galleryimages', 'gallery_id'));
        // dd($galleryimages);
    }
    public function deleteimage(Request $request, $id)
    {
        Galleryimages::where('id', $id)->delete();
        // $request->session()->flash('error', 'Deleted Succefully');
        return back()->with('error', ' Deleted Successfully');
    }
    public function createimage(request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            // 'category' => 'required',
        ]);
        // dd($request->all());
        // dd($_FILES);
        // $where = array('title_id' => $id);
        if ($request->hasFile('images')) {
            $allowedfileExtension = ['pdf', 'jpg', 'png', 'docx', 'xlsx', 'xls', 'csv', 'tsv', 'msg', 'jpeg', 'doc', 'txt', 'eml'];
            $files = $request->file('images');
            foreach ($files as $file => $value) {
                $filenamefull = $value->getClientOriginalName();
                $extension = $value->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                if ($check == true) {
                    if (in_array($extension, $allowedfileExtension)) {
                        $filename = $value->store('public/images');
                        $f = explode('/', $filename);
                        if (array_key_exists("2", $f)) {
                            Galleryimages::create([
                                'model_id' => $request->id,
                                'images' => $filename,
                            ]);
                        }
                    }
                }
            }
        }
        // return back()->with('Success', 'Succefully Inserted');
        $request->session()->flash('success', 'Successfully Inserted');
        return back();
    }
}
