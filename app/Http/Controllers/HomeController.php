<?php

namespace App\Http\Controllers;

use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\StateMaster;
use App\Models\DistrictMaster;
use App\Models\TahshilMaster;
use App\Models\Faq;
use App\Models\FaqPage;
use App\Models\Page;
use App\Models\ContactUs;
use DB;

class HomeController extends Controller
{

    public function index()
    {
        $faq_page = Page::where("slug", 'home')->orderBy('id', 'asc')->first();
        $faqs = Faq::where("faq_page_id", $faq_page->id)->orderBy('id', 'asc')->get();
        return view("fronts.home", compact('faqs'));
    }

    public function pageShow($slug)
    {
        $page = Page::where("slug", $slug)->orderBy('id', 'asc')->first();
        if ($page) {
            $faqs = Faq::where("faq_page_id", $page->id)->orderBy('id', 'asc')->get();
            return view("fronts.pages.index", compact('faqs', 'page'));
        } else {
            //$faqs = Faq::where("faq_page_id", $page->id)->orderBy('id', 'asc')->get();
            return view("errors.404");
        }
    }

    public function emiCalculater()
    {

        return view("fronts.emi_calculater");
    }
    public function getStateDistricts(Request $request)
    {
        $subbrand = DB::table("districts")->where("state_id", $request->state_id)->orderBy('district', 'ASC')->get();
        $data = '';
        if ($request->state_id != 0) {
            $data .= " <option value=''> Select District</option>";
            foreach ($subbrand as $s) {
                if ($request->district_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->district . '</option>';
            }
        } else {
            $data .= " <option value=''> Select District</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->district . "</option>";
            }
        }
        // dd($data);
        return response()->json($data);
    }
    public function getDistrictsTahshil(Request $request)
    {
        //dd("test");

        $subbrand = DB::table("tahshil")->where("district_id", $request->district_id)->orderBy('tahshil', 'ASC')->get();
        $data = '';
        if ($request->tahshil_id != 0) {
            $data .= " <option value=''> Select Tehsil</option>";
            foreach ($subbrand as $s) {
                if ($request->tahshil_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->tahshil . '</option>';
            }
        } else {
            $data .= " <option value=''> Select Tehsil</option>";
            foreach ($subbrand as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->tahshil . "</option>";
            }
        }
        //dd($data);
        return response()->json($data);
    }


    public function aboutUs()
    {

        $faq_page = FaqPage::where("slug", 'about-us')->orderBy('id', 'asc')->first();
        $faqs = Faq::where("faq_page_id", $faq_page->id)->orderBy('id', 'asc')->get();
        return view("fronts.about_us", compact('faqs'));
    }
    public function contactUs()
    {

        return view("fronts.contact_us");
    }
    public function contactusSubmit(Request $request)
    {

        $cat = ContactUs::Create([
            'query_type' => $request->query_type,
            'name' => $request->name,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'message' => $request->message,
        ]);
        return redirect('/thankyou');
    }
    public function thankyou()
    {

        return view("fronts.thankyou");
    }
    public function termsAndConditions()
    {

        return view("fronts.termsandcondition");
    }
}
