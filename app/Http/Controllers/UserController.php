<?php

namespace App\Http\Controllers;

use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Subcategory;
use DB;

class UserController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function list()
    {
        $type = Auth::user()->role;

        // if ($type == 'agent') {
        //     session()->flash('error', 'You are not authorise for this operation',);
        //     return redirect('/agent');
        // }

        // $type = Auth::user()->role;
        // if ($type == 'superadmin') {
        //     // $user = User::all();

        $user = User::leftjoin('category', 'users.category_id', '=', 'category.id')
            ->leftjoin('subcategory', 'users.subcategory_id', '=', 'subcategory.id')
            ->get(['users.*', 'category.category', 'subcategory.subcategory']);
        // dd($user);


        // } elseif ($type == 'admin') {

        //     $user = User::leftjoin('process', 'users.process_id', '=', 'process.id')
        //         ->where('users.process_id', Auth::user()->process_id)
        //         ->get(['users.*', 'process.process']);
        // } else {
        //     return redirect('/');
        // }
        return view('usermanagement.usermanagement', compact('user'));
    }

    public function add(request $id)
    {

        $type = Auth::user()->role;
        if ($type == 'superadmin' || $type == 'admin') {
            $subprocess = Subprocess::leftjoin('process', 'sub_process.process_id', '=', 'process.id')
                ->get(['sub_process.*', 'process.process']);
        } else {

            $subprocess = Subprocess::leftjoin('process', 'sub_process.process_id', '=', 'process.id')
                ->where('sub_process.process_id', Auth::user()->process_id)
                ->get(['sub_process.*', 'process.process']);
        }
        $where = array('id' =>  Auth::user()->id);
        $users  = User::where($where)->first();
        // $users = User::all();
        // dd($users);
        $process = Process::all();
        return view('users.add', compact('subprocess', 'users',  'process'));
        // return view('users.add', compact('process'));


    }



    public function create(Request $request)

    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required',
        //     'role' => 'required',
        //     'process_id' => 'required',
        //     // 'password' => 'required',
        // ]);

        User::Create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'category_id' => $request->category,
            'subcategory_id' => $request->subcategory,
        ]);
        $request->session()->flash('success', 'User Updated Succefully');
        return redirect('/user');
    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $users  = User::where($where)->first();
        $category = Category::all();
        $subcategory = Subcategory::where('category_id', Auth::user()->category_id)->get();;
        // $process = Process::all();
        // $subprocess = array();
        // $type = Auth::user()->role;
        // if ($type == 'admin') {
        //     $subprocess = Subprocess::where('process_id', Auth::user()->process_id)->get();
        //     $process = Process::all();
        // } else {
        //     $process = Process::all();
        // }

        // dd($category);
        return view('users.edit')->with(compact('users', 'category', 'subcategory'));
    }
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'category' => 'required',
            'subcategory' => 'required',
            // 'password' => 'required',
        ]);
        if ($request->password) {
            User::where('id', $request->id)->Create([
                'name' => $request->name,
                'email' => $request->email,
                'category_id' => $request->category,
                // 'process_id' => $request->process_id,
                'subcategory_id' => $request->subcategory,
                'password' => Hash::make($request->password),
            ]);
            $request->session()->flash('update', 'User Updated Succefully');
            return redirect('/user');
        } else {
            User::where('id', $request->id)->Create([
                'name' => $request->name,
                'email' => $request->email,
                // 'role' => $request->ro,
                'category_id' => $request->category,
                'subcategory_id' => $request->subcategory,
            ]);
            $request->session()->flash('update', 'User Updated Succefully');
            return redirect('/user');
        }
    }
    public function delete(Request $request, $id)
    {
        User::where('id', $id)->delete();
        $request->session()->flash('error', 'User Deleted Succefully');
        return redirect('/user');
    }

    public function getSubprocess(Request $request)
    {
        $subcategory = DB::table("subcategory")->where("category_id", $request->category)->get();
        $data = '';
        if ($request->sub_process_id != 0) {

            $data .= " <option> Select Sub-Category</option>";
            foreach ($subcategory as $s) {
                if ($request->sub_process_id == $s->id) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $data .= '<option value="' . $s->id . '" ' . $selected . '>' . $s->subcategory . '</option>';
            }
        } else {

            $data .= " <option> Select Sub-Category</option>";
            foreach ($subcategory as $s) {
                $data .= "<option value='" . $s->id . "'>" . $s->subcategory . "</option>";
            }
        }

        // echo $data;

        return response()->json($data);
    }
}
