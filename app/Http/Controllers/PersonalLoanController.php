<?php

namespace App\Http\Controllers;


use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\StateMaster;
use App\Models\HourMaster;
use App\Models\OwnerMaster;
use App\Models\Year;
use App\Models\UserLead;
use App\Models\FreeDoorstep;
use App\Models\PersonalLoan;
use DB;
use Session;

class PersonalLoanController extends Controller
{

    public function index()
    {
        $brand = Brand::all();
        $states = StateMaster::orderBy('name', 'ASC')->get();
        $hours = HourMaster::all();
        $owners = OwnerMaster::orderBy('id', 'ASC')->get();
        $year = Year::all();

        return view("fronts.personalloan.index", compact('brand', 'states', 'hours', 'owners', 'year'));
    }




    public function submitmobilePersonalLoan(Request $request)
    {
        $check = PersonalLoan::where("mobile", $request->mobileNumber)->first();
        //$otp = random_int(100000, 999999);
        $otp = 1234;

        if ($check) {
            $otpupdate = PersonalLoan::find($check->id);
            $otpupdate->otp =  $otp;
            $otpupdate->update();
        } else {
            $cat = PersonalLoan::Create([
                'mobile' => $request->mobileNumber,
                'otp' => $otp,
                'otp_status' => "pending",
            ]);
        }

        $data = array("message:success");
        return response()->json($data);
    }
    public function checkotpPersonalLoan(Request $request)
    {
        //$otp = random_int(100000, 999999);
        $check = PersonalLoan::where("mobile", $request->mobileNumber)->where("otp", $request->mobileOtp)->first();
        if ($check && $check->id != "") {
            $data = "succcess";
        } else {
            $data = "error";
        }
        return response()->json($data);
    }

    public function SubmitDetailPersonalLoan(Request $request)
    {
        //dd($request->input());
        $userlead = PersonalLoan::where("mobile", $request->mobileNumber)->first();
        //dd($filePath);
        if ($userlead) {
            PersonalLoan::where('mobile', $request->mobileNumber)
                ->update([
                    'fullname' => $request->fullname,
                    'pan_number' => $request->pan_number,
                    'state_id' => $request->state_id,
                    'district_id' => $request->district_id,
                    'tahshil_id' => $request->tahshil_id,
                    'is_tractor' => $request->is_tractor,
                    'is_loan' => $request->is_loan,
                    'otp_verify' => "approved",
                ]);
        }
        return redirect('/personal-loan-thankyou');
    }
    public function thankYou(Request $request)
    {
        return view("fronts.personalloan.thankyou");
    }
}
