<?php

namespace App\Http\Controllers;


use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\StateMaster;
use App\Models\HourMaster;
use App\Models\OwnerMaster;
use App\Models\Year;
use App\Models\UserLead;
use App\Models\FreeDoorstep;
use DB;
use Session;

class FreeDoorStepController extends Controller
{

    public function index()
    {
        $brand = Brand::all();
        $states = StateMaster::orderBy('name', 'ASC')->get();
        $hours = HourMaster::all();
        $owners = OwnerMaster::orderBy('id', 'ASC')->get();
        $year = Year::all();

        return view("fronts.freedoorstep.index", compact('brand', 'states', 'hours', 'owners', 'year'));
    }




    public function SubmitMobileFreedoorstep(Request $request)
    {
        $check = FreeDoorstep::where("mobile", $request->mobileNumber)->first();
        //$otp = random_int(100000, 999999);
        $otp = 1234;

        if ($check) {
            $otpupdate = FreeDoorstep::find($check->id);
            $otpupdate->otp =  $otp;
            $otpupdate->update();
        } else {
            $cat = FreeDoorstep::Create([
                'mobile' => $request->mobileNumber,
                'otp' => $otp,
                'otp_status' => "pending",
            ]);
        }

        $data = array("message:success");
        return response()->json($data);
    }
    public function checkOtpFreedoorstep(Request $request)
    {
        //$otp = random_int(100000, 999999);
        $check = FreeDoorstep::where("mobile", $request->mobileNumber)->where("otp", $request->mobileOtp)->first();
        if ($check && $check->id != "") {
            $data = "succcess";
        } else {
            $data = "error";
        }
        return response()->json($data);
    }

    public function SubmitDetailFreeDoorstep(Request $request)
    {
        $userlead = FreeDoorstep::where("mobile", $request->mobileNumber)->first();
        //dd($filePath);
        if ($userlead) {
            FreeDoorstep::where('mobile', $request->mobileNumber)
                ->update([
                    'fullname' => $request->fullname,
                    'pan_number' => $request->pan_number,
                    'state_id' => $request->state_id,
                    'district_id' => $request->district_id,
                    'tahshil_id' => $request->tahshil_id,
                    'otp_verify' => "approved",
                ]);
        }
        return redirect('/free-doorstep-thankyou');
    }
    public function thankYou(Request $request)
    {
        return view("fronts.freedoorstep.thankyou");
    }
}
