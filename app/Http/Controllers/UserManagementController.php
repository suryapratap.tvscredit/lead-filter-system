<?php

namespace App\Http\Controllers;

use App\Models\Process;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Auth;

class ProcessController extends Controller
{
    /**
     * Create a new controller instance.
     *  
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $type = Auth::user()->role;

        if ($type == 'agent') {
            session()->flash('error', 'You are not authorise for this operation',);
            return redirect('/agent');
        }

        return view('admin.process');
    }
    public function list()
    {
        $process = Process::all();
        //  dd($crmdetails);
        return view('admin.process', compact('process'));
    }
    public function storeprocess(Request $request)
    {
        $process = new Process();
        $process->process = $request->process;
        $process->user_id = Auth::user()->id;
        $process->save();
        return response()->json($process);
    }
    public function getByid($id)
    {
        $process = Process::find($id);
        return response()->json($process);
    }
    public function updateprocess(Request $request)
    {
        $this->validate($request, [
            'process' => 'required',
        ]);
        $process = Process::find($request->uid);
        $process->process = $request->process;
        $process->update();
        return response()->json($process);
    }
    public function destroy($id)
    {
        $processs = process::find($id);
        $processs->delete();
        return redirect()->route('admin.process')->with('message', 'Data has been deleted succesfully');
    }
}
