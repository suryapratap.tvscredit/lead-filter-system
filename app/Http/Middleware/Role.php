<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Role as Middleware;
use Illuminate\Support\Facades\Auth;

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;

class Role
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $checkAuth = auth()->user()->role == 'superadmin';
        $checkAuth1 = auth()->user()->role == 'admin';
        if ($checkAuth == TRUE || $checkAuth1 == TRUE) {
            return $next($request);
        }
        abort(403);
        return back()->with('message', 'You are not ADMIN');
    }
}
