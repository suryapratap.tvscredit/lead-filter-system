<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\TempData;

class TempImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        // dd($row);
        if (!empty($row)) {
            return new TempData([
                'lead_source' => $row['lead_source'],
                'source' => $row['source'],
                'lead_type' => $row['lead_type'],
                'enq_id' => $row['enq_id'],
                'mobile' => $row['mobile'],
                'name' => $row['name'],
                'tkstate' => $row['tkstate'],
                'tkdistrict' => $row['tkdistrict'],
                'tktehsil' => $row['tktehsil'],
                'tkproduct_category' => $row['tkproduct_category'],
                'tkbrand' => $row['tkbrand'],
                'tkmodel' => $row['tkmodel'],
                'website' => $row['website'],
                'lead_date' => $row['website'],
                'created_on' =>  $row['created_on'],
                'zip' => $row['zip'],
                'pancard' => $row['pancard'],
                'loan_type' => $row['loan_type'],

            ]);
        }
    }
}
