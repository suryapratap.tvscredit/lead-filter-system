@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <!-- /.card-header -->

                                        <div class="container-fluid ">
                                            <div class="row ">
                                                <div class="col-sm-12 ">
                                                    <a href="{{route('series.add')}}">
                                                        <button class="buttonplay"> <i class="fa fa-plus icon-white"><span></i> Add Series </span> </button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr no.</th>
                                                <th>Brand</th>
                                                <th>Series</th>


                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php $sno = 1 ?>
                                            @foreach($series as $cat)
                                            <tr class="gradeX">
                                                <td>{{$sno++}}</td>
                                                <td>{!!$cat->brand->title!!}</td>
                                                <td>{!!$cat->title!!}</td>
                                                <td>
                                                    <div class="btn-group" style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                        <a href="{{ route('series.edit',$cat->id)}}"> <i class=" fa fa-edit" style="padding:5px"></i></a>
                                                        <a href="{{ route('series.delete',$cat->id)}}"><i class="fa fa-trash" style="color: #b91010;padding:5px"></i></a>
                                    

                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</div>
</div>
</div>
</div>
<script>
    $('#datatable').dataTable();
</script>
@endsection