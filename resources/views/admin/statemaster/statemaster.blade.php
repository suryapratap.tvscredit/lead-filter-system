@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->

                                            <div class="container-fluid ">
                                                <div class="row ">
                                                    <div class="col-sm-3 ">
                                                        <a href="{{ route('statemaster.add') }}">
                                                            <button class="buttonplay"> <i
                                                                    class="fa fa-plus icon-white"><span></i> Add State
                                                                master
                                                                </span> </button></a>
                                                    </div>
                                                    <div class="col-sm-3 ">
                                                        <a href="{{ route('statemaster.deleteemptystate') }}">
                                                            <button class="btn btn-danger buttonplay"> <i
                                                                    class="fa fa-trash icon-white"><span></i> Delete Empty
                                                                State
                                                                </span> </button></a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>State</th>
                                                    <th>Wrong Value</th>


                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($statemaster as $cat)
                                                    <tr class="gradeX">
                                                        <td>{{ $sno++ }}</td>

                                                        <td>{!! $cat->state_name !!}</td>
                                                        <td>{!! $cat->error_value !!}</td>

                                                        <td>
                                                            <div class="btn-group"
                                                                style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                <a href="{{ route('statemaster.edit', $cat->id) }}"> <i
                                                                        class=" fa fa-edit" style="padding:5px"></i></a>
                                                                <a href="{{ route('statemaster.delete', $cat->id) }}"><i
                                                                        class="fa fa-trash"
                                                                        style="color: #b91010;padding:5px"></i></a>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('brand.brandimport') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" name="select_file"
                                    id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $('#datatable').dataTable();
    </script>
@endsection
