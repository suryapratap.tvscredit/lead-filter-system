@extends('admin.include.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h4>ADD USER </h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />
                                            <form role="form" id="quickForm" action="{{ route('users.create') }}"
                                                method="POST">
                                                <div class="card-body">
                                                    @csrf
                                                    <div class="row">
                                                        <input type="hidden" name="id" value="{{ $users->id }}">

                                                        <div class="form-group  col-md-6">
                                                            <label>Name</label>
                                                            <input type="text" class="form-control" id="name"
                                                                name="name" placeholder="Enter Name" required>

                                                        </div>
                                                        <div class="form-group  col-md-6">
                                                            <label>Email address</label>
                                                            <input type="email" class="form-control" name="email"
                                                                id="email" placeholder="Enter email" required>
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>Password</label>
                                                            <input type="password" class="form-control" id="password"
                                                                name="password" placeholder="Password" required>
                                                        </div>


                                                        <div class="form-group  col-md-6">
                                                            <label>User Type:</label>
                                                            <select class="form-control" name="role" id="role"
                                                                required>
                                                                <option value=""> Select Role</option>
                                                                <option value="user"> User</option>
                                                                <option value="admin"> Admin</option>

                                                                <?php $type = Auth::user()->role;
                                                            if ($type == 'superadmin') { ?>
                                                                <option value="superadmin"> Super Admin</option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>



                                                        <div class="form-group  col-md-6">
                                                            <label for="title">Status:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4" name="status"
                                                                id="status" required>
                                                                <option value=""> Select Status</option>
                                                                <option value="active"> Active</option>
                                                                <option value="inactive"> Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <!-- /.card-body -->
                                                        <button type="submit" class="btn btn-primary"
                                                            style="margin-left: 40%;margin-top: 20px;">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>


        <script>
            function getsubcategory(category, subcategory) {
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('getsubcategory') }}",
                    type: "POST",
                    data: {
                        "category": category,
                        "subcategory": subcategory,
                        "_token": CSRF_TOKEN
                    },
                    dataType: "json",
                    success: function(response) {
                        $('#subcategory').html(response);
                    }
                });
            }
            <?php if ($users->subcategory != "") { ?>
            getsubcategory(<?php echo $users->category; ?>, <?php echo $users->subcategory; ?>)
            <?php   } ?>

            $(document).ready(function() {
                var subid = 0;

                $('select[name="category"]').on('change', function() {
                    var pro_id = $(this).val();
                    getsubcategory(pro_id, 0)

                });
            });
        </script>
    @endsection
