@extends('admin.include.master')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h4>EDIT USER DETAILS </h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />
                                            <form role="form" id="quickForm" action="{{ route('users.update') }}"
                                                method="POST">
                                                <div class="card-body">
                                                    @csrf
                                                    <div class="row">
                                                        <input type="hidden" name="id" value="{{ $users->id }}">
                                                        <div class="form-group  col-md-6">
                                                            <label>Username</label>
                                                            <input type="text" class="form-control" id="name"
                                                                name="name" value="{{ $users->name }}"
                                                                placeholder="Enter email" required>
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>Email address</label>
                                                            <input type="email" class="form-control" name="email"
                                                                id="email" value="{{ $users->email }}"
                                                                placeholder="Enter email" required>
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>Password</label>
                                                            <input type="password" class="form-control"
                                                                style="box-shadow: 0 0 5px rgb(139 41 68);" id="password"
                                                                name="password"
                                                                placeholder="Password (Leave Blank For old password )">
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>User Type:</label>
                                                            <select class="form-control select2bs4" name="role"
                                                                id="role" required>
                                                                <option value="" selected>Select Employee Type
                                                                </option>
                                                                <?php $type = Auth::user()->role;
                                                            if ($type == 'superadmin') { ?>
                                                                <option value="superadmin"
                                                                    {{ $users->role == 'superadmin' ? 'selected' : '' }}>
                                                                    Super Admin</option>
                                                                <?php } ?>
                                                                <!-- <option value="superadmin" {{ $users->role == 'superadmin' ? 'selected' : '' }}>Superadmin</option> -->
                                                                <option value="admin"
                                                                    {{ $users->role == 'admin' ? 'selected' : '' }}>Admin
                                                                </option>
                                                                <option value="agent"
                                                                    {{ $users->role == 'agent' ? 'selected' : '' }}>Agent
                                                                </option>
                                                            </select>
                                                        </div>



                                                        <div class="form-group  col-md-6">
                                                            <label for="title">Status:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4" name="status"
                                                                id="status" required>
                                                                <option value=""> Select Status</option>
                                                                <option value="active"
                                                                    {{ $users->status == 'active' ? 'selected' : '' }}>
                                                                    Active</option>
                                                                <option value="inactive"
                                                                    {{ $users->status == 'inactive' ? 'selected' : '' }}>
                                                                    Inactive</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <!-- /.card-body -->
                                                        <button type="submit" class="btn btn-primary"
                                                            style="margin-left: 40%;margin-top: 20px;">Submit</button>
                                                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </body>


        <!--
        <script>
            function getsubcategory(category, subcategory) {
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('getsubcategory') }}",
                    type: "POST",
                    data: {
                        "category": category,
                        "subcategory": subcategory,
                        "_token": CSRF_TOKEN
                    },
                    dataType: "json",
                    success: function(response) {
                        $('#subcategory').html(response);
                    }
                });
            }

            <?php if ($users->subcategory != "") { ?>
            getsubcategory(<?php echo $users->category; ?>, <?php echo $users->subcategory; ?>)
            <?php   } ?>


            <?php if ($users->category != "") { ?>
            getsubcategory(<?php echo $users->category; ?>, 0)
            <?php   } ?>


            $(document).ready(function() {
                var sub_id = 0;

                $('select[name="category"]').on('change', function() {
                    var pro_id = $(this).val();
                    getsubcategory(pro_id, 0)

                });
            });
        </script> -->
    @endsection
