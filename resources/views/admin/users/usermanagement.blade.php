@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->
                                            @if (Session::has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('success') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="clearfix"></div>
                                            @endif
                                            @if (Session::has('update'))
                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('update') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            @if (Session::has('error'))
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('error') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            <div class="container-fluid ">
                                                <div class="row ">
                                                    <div class="col-sm-12 ">
                                                        <a style="color:#fff;" href="{{ route('users.add') }}">
                                                            <button class="buttonplay">
                                                                <i class="fa fa-plus" aria-hidden="true"><span
                                                                        style="color:#fff;"></i> Add User</span> </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            </br>
                                            <table id="datatable" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Sr no.</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <!-- <th>category</th>
                                                                <th>subcategory</th> -->
                                                        <th>User Role</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php $sno = 1; ?>
                                                    @foreach ($user as $users)
                                                        <tr class="gradeX">
                                                            <td>{{ $sno++ }}</td>
                                                            <td>{{ $users->name }}</td>
                                                            <td>{{ $users->email }}</td>

                                                            <td>{{ $users->role }}</td>
                                                            <td> <?php if ($users->status == 'active') {  ?>
                                                                <label class="label label-success">
                                                                    {{ ucfirst($users->status) }}</label>
                                                                <?php } else { ?>
                                                                <label class="label label-warning">
                                                                    {{ ucfirst($users->status) }}</label>
                                                                <?php } ?>
                                                            </td>
                                                            <td>
                                                                <div class="btn-group"
                                                                    style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                    <a href="{{ route('users.edit', $users->id) }}"> <i
                                                                            class=" fa fa-edit" style="padding:5px"></i></a>
                                                                    <a href="{{ route('users.delete', $users->id) }}"><i
                                                                            class="fa fa-trash"
                                                                            style="color: #b91010;padding:5px"></i></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    </div>

    <script>
        $('#datatable').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
            ],
            // scrollX: 300,
            responsive: true,

        });
    </script>
@endsection
