@extends('admin.include.master')

<!-- Content Wrapper. Contains page content -->
<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dashboard</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <h3> Welcome <span><strong> {{ Auth::user()->name }}
                                    </strong>
                                </span>
                            </h3>

                            <div class="">
                                <div class="row top_tiles">
                                    <a href="#">
                                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="icon"><i class="fa fa-list" style="margin-top:10px;"></i>
                                                </div>
                                                <div class="count">{{ $notmatchedCount + $matchedCount }}</div>
                                                <h3>Total Leads Import</h3>

                                            </div>
                                        </div>
                                    </a>
                                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">

                                        <a href="#">
                                            <div class="tile-stats">
                                                <div class="icon"><i class="fa fa-list" style="margin-top:10px;"></i>
                                                </div>
                                                <div class="count">{{ $matchedCount }}</div>
                                                <h3>Matching Leads</h3>

                                            </div>
                                        </a>
                                    </div>


                                </div>
                            </div>
                            <hr>
                            <h3> Mapped State,District,Brand names <span>
                                </span>
                            </h3>
                            <div class="">
                                <div class="row top_tiles">


                                    <a href="{{ route('mapState') }}">
                                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="icon"><i class="fa fa-list" style="margin-top:10px;"></i>
                                                </div>
                                                <div class="count">{{ $statenotmatchedCount }}</div>
                                                <h3>State Not Match</h3>

                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{ route('mapDistrict') }}">
                                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="icon"><i class="fa fa-list" style="margin-top:10px;"></i>
                                                </div>
                                                <div class="count">{{ $districtnotmatchedCount }}</div>
                                                <h3>District Not Match</h3>

                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{ route('mapBrand') }}">
                                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <div class="tile-stats">
                                                <div class="icon"><i class="fa fa-list" style="margin-top:10px;"></i>
                                                </div>
                                                <div class="count">{{ $brandnotmatchedCount }}</div>
                                                <h3>Brand Not Match</h3>

                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>

                            <hr>

                            <div class="row">

                                <div class="col-sm-3">
                                    <button class="btn btn-success" data-target="#addModal" data-toggle="modal">
                                        <i class="fa fa-plus icon-white"><span></i>
                                        Import dump </span> </button>
                                </div>
                                <div class="col-sm-3">
                                    <a href="{{ route('stateFilter') }}"><button class="btn btn-warning">
                                            <i class="fa fa-exchange icon-white"><span></i>
                                            Map State </span> </button> </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="{{ route('districtsFilter') }}"><button class="btn btn-warning">
                                            <i class="fa fa-exchange icon-white"><span></i>
                                            Map Districts </span> </button> </a>
                                </div>
                                <div class="col-sm-3">
                                    <a href="{{ route('brandFilter') }}"><button class="btn btn-warning">
                                            <i class="fa fa-exchange icon-white"><span></i>
                                            Map Brands </span> </button> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('importdashboard') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" name="select_file"
                                    id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection
