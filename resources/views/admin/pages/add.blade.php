@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Add Page</h3>
                                        </div>
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('success') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endif
                                        @if (Session::has('update'))
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('update') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if (Session::has('error'))
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('error') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        <div class="container-fluid ">

                                        </div>
                                        </br>
                                        <form method="post" role="form" action="{{ route('pages.create') }}"
                                            name="frmlead" id="frmlead" enctype="multipart/form-data"
                                            data-parsley-validate>

                                            <div class="body" id="form">
                                                @csrf


                                                <div class="row">


                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Title:</label><span class="err"
                                                                id="err_title"></span>
                                                            <input type="text" class="form-control" class="text"
                                                                placeholder="enter title" name="title" id="title"
                                                                autocomplete="off" required>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-md-4">
                                                        <label for="title">Status:</label><span class="err"
                                                            id="err_title"></span>
                                                        <select class="form-control select2bs4" name="status"
                                                            id="status" required>
                                                            <option value=""> Select Status</option>
                                                            <option value="active"> Active</option>
                                                            <option value="inactive"> Inactive</option>
                                                        </select>
                                                    </div>



                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <div class="controls">
                                                            <label for="title"> Description:</label><span class="err"
                                                                id="err_title"></span>
                                                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="description"
                                                                id="description" autocomplete="off"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Meta Title:</label><span class="err"
                                                                id="err_title"></span>
                                                            <input type="text" class="form-control" class="text"
                                                                placeholder="enter meta title" name="meta_title"
                                                                id="meta_title" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Meta Keyword:</label><span class="err"
                                                                id="err_title"></span>
                                                            <input type="text" class="form-control" class="text"
                                                                placeholder="enter meta keyword" name="meta_keyword"
                                                                id="meta_keyword" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <div class="controls">
                                                            <label for="title"> Meta Description:</label><span
                                                                class="err" id="err_title"></span>
                                                            <textarea type="text" class="form-control" class="text" placeholder="meta Description" name="meta_description"
                                                                id="meta_description" autocomplete="off"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer" align="center">
                                                <button type="submit" id="submit" name="submit"
                                                    class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $('input[type="checkbox"]').click(function() {
                var inputValue = $(this).attr("chk_type");
                $("#" + inputValue).toggle();
            });
        });
        $(document).ready(function() {


            $('#description').summernote({
                height: 300,
                //width: 500,
            });


        });
    </script>
    <!--  ADD USER MODAL -->
@endsection
