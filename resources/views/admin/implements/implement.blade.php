@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3><b>Implement</b></h3>
                                    </div>

                                    <div class="container-fluid ">
                                        <div class="row ">
                                            <div class="col-sm-12 ">
                                                <a href="{{route('impelments.add')}}">
                                                    <button class="buttonplay"> <i class="fa fa-plus icon-white"><span></i> Add Implement </span> </button></a>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr no.</th>
                                                <th>Image</th>
                                                <th>Implement</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php $sno = 1 ?>
                                            @foreach($implement as $cat)
                                            <tr class="gradeX">
                                                <td>{{$sno++}}</td>
                                                <td><?php if (isset($cat->primary_image)) { ?>
                                                        @if ($cat->primary_image !='')
                                                        <?php
                                                        $file = explode('/', $cat->primary_image);
                                                        $fileurl = $file['2']; ?>
                                                        <img style="width:100;height:80px;" src="{{env('APP_URL')}}/storage/app/public/upload/<?php echo $fileurl ?>">
                                                        @endif
                                                    <?php } ?>
                                                </td>
                                                <td>{!!$cat->title!!}</td>

                                                <td>
                                                    <div class="btn-group" style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                        <a href="{{ route('models.edit',$cat->id)}}"> <i class=" fa fa-edit" style="padding:5px"></i></a>
                                                        <a href="{{ route('models.delete',$cat->id)}}"><i class="fa fa-trash" style="color: #b91010;padding:5px"></i></a>
                                                        <a href="{{ route('gallery.images',$cat->id)}}"><i class="fa fa-picture-o" style="color: #b91010;padding:5px"></i></a>


                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</div>
</div>
</div>
</div>
<script>
    $('#datatable').dataTable();
</script>
@endsection