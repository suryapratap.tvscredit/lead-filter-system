@extends('admin.include.master')

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">


<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <button class="buttonplay" data-target="#addModal" data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i> Add Image </span> </button><br><br>

                    </div>
                    <div class="x_content">
                        <div class="col-md-12">
                            <div class="row">

                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr no.</th>
                                            <th>Title</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $sno = 1 ?>
                                        @foreach($galleryimages as $image)
                                        <tr class="gradeX">
                                            <td>{{$sno++}}</td>
                                            <td> @if ($image->images !='')
                                                <?php $file = explode('/', $image->images);
                                                $fileurl = $file['2']; ?>
                                                <img height="80px" width="120px" src="{{env('APP_URL')}}/storage/app/public/images/<?php echo $fileurl ?>"></img>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="btn-group" style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                    <a href="{{ route('gallery.deleteimage',$image->id)}}"><i class="fa fa-trash" style="color: #b91010;padding:5px"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="POST" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data" action="{{route('gallery.createimage')}}" onsubmit="return confirm('Do you really want to submit the form?');" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add </h4>
                </div>
                <div class="modal-body" id="form">
                    @csrf
                    <input type="hidden" name="id" value="{{$gallery_id}}">
                    <div class="form-group">
                        <label for="title"> Images</label><span class="err" id="err_title"></span>
                        <input type="file" multiple="multiple" class="form-control" class="text" placeholder="Images" name="images[]" id="images" autocomplete="off" required>
                    </div>
                    <div class="row">
                        <div class="modal-footer">
                            <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#datatable').dataTable({
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
        ],
        // scrollX: 300,
        responsive: true,
    });
</script>

@endsection