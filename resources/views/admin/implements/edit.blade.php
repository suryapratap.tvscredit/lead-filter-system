@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3>Edit Model</h3>
                                    </div>

                                    </br>
                                    <form method="post" role="form" action="{{route('models.update')}}" name="frmlead" id="frmlead" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to submit the form?');" data-parsley-validate>

                                        <div class="body" id="form">
                                            @csrf

                                            <input type="hidden" value="{{$model->id}}" name="id" id="id">

                                            <div class="row">
                                                <!-- <div class="form-group col-md-4">
                                                    <label for="title">Select Type:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="model_type" id="model_type" required>
                                                        <option value=""> Select type</option>
                                                        <option value="tractor" {{($model->model_type=="tractor") ? 'selected': ''}}> Tractor</option>
                                                        <option value="implemnts" {{($model->model_type=="implemnts") ? 'selected': ''}}> Implements</option>

                                                    </select>
                                                </div> -->
                                                <div class="form-group col-md-4">
                                                    <label for="title">Brand:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="brand_id" id="brand_id" required>
                                                        <option value=""> Select brand</option>
                                                        <?php foreach ($brand as $b) { ?>
                                                            <option value="{{$b->id}}" {{($model->brand_id==$b->id) ? 'selected': ''}}>{{$b->title}}</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4" id="series_Div">
                                                    <label for="title">Series:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="series_id" id="series_id">

                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <div class="controls">
                                                        <label for="title"> Title:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter title" name="title" id="title" autocomplete="off" required value="{{$model->title}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="title">Status:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="status" id="status" required>
                                                        <option value=""> Select Status</option>
                                                        <option value="active" {{($model->status=="active") ? 'selected': ''}}> Active</option>
                                                        <option value="inactive" {{($model->status=="inactive") ? 'selected': ''}}> Inactive</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Primary Image</label>
                                                    <input class="form-control " type="file" name="primary_image" id="primary_image" placeholder="File">
                                                    <?php if (isset($models->primary_image)) { ?>
                                                        @if ($models->primary_image !='')
                                                        <?php
                                                        $file = explode('/', $models->primary_image);
                                                        $fileurl = $file['2']; ?>
                                                        <img style="width:50;height:50px;" src="{{env('APP_URL')}}/storage/app/public/upload/<?php echo $fileurl ?>">
                                                        @endif
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <div class="controls">
                                                        <label for="title"> Description:</label><span class="err" id="err_title"></span>
                                                        <textarea type="text" class="form-control" class="text" placeholder="Description" name="description" id="description" autocomplete="off" required>{{$model->description}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <div class="controls">
                                                        <label for="title"> Unique ID:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter Unique ID" name="unique_id" id="unique_id" autocomplete="off">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <label>Category:</label>
                                                <p style="padding: 5px;">

                                                    <input type="checkbox" name="is_new" id="is_new" value="1" class="flat" {{($model->is_new=="1") ? 'checked': ''}} /> New Launch
                                                    <input type="checkbox" name="is_populer" id="is_populer" value="1" class="flat" {{($model->is_populer=="1") ? 'checked': ''}} /> Populer Tractor
                                                    <input type="checkbox" name="is_upcoming" id="is_upcoming" value="1" class="flat" {{($model->is_upcoming=="1") ? 'checked': ''}} /> Upcoming Tractor
                                                    <input type="checkbox" name="is_mini_tractor" id="is_mini_tractor" value="1" class="flat" {{($model->is_mini_tractor=="1") ? 'checked': ''}} /> Mini Tractor
                                                    <input type="checkbox" name="is_4wd_tractor" id="is_4wd_tractor" value="1" class="flat" {{($model->is_4wd_tractor=="1") ? 'checked': ''}} /> 4WD Tractor
                                                    <input type="checkbox" name="is_ac_cabin_tractor" id="is_ac_cabin_tractor" value="1" class="flat" {{($model->is_ac_cabin_tractor=="1") ? 'checked': ''}} /> AC Cabin Tractor
                                                    <input type="checkbox" name="is_discontinued" id="is_discontinued" value="1" class="flat" {{($model->is_discontinued=="1") ? 'checked': ''}} /> Discontinued

                                                <p>
                                            </div>
                                            <h3 align="center"><b>Features</b></h3>
                                            <h4><b>Engine</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> No of Cylinder:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter No of Cylinder" name="noofcylinder" id="title" autocomplete="off" value="{{$model->noofcylinder}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> HP :</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter hp category" name="hp_category" id="title" autocomplete="off" value="{{$model->hp_category}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Engine Type:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter engine type" name="engine_type" id="title" autocomplete="off" value="{{$model->engine_type}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Engine Rated RPM:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter engine rated rpm" name="engine_rated_rpm" id="title" autocomplete="off" value="{{$model->engine_rated_rpm}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Max Torque:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter max torque" name="max_torque" id="title" autocomplete="off" value="{{$model->max_torque}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Capacity CC:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter capacity cc" name="capacity_cc" id="title" autocomplete="off" value="{{$model->capacity_cc}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Air Filter:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter air filter" name="air_filter" id="title" autocomplete="off" value="{{$model->air_filter}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Cooling System:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter cooling system" name="cooling_system" id="title" autocomplete="off" value="{{$model->cooling_system}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Fuel Type:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter Fuel Type" name="fule_type" id="title" autocomplete="off" value="{{$model->fule_type}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Bore / Stroke:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter Bore / Stroke" name="bore_stroke" id="title" autocomplete="off" value="{{$model->bore_stroke}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Fuel Pump Type:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter Fuel Pump Type" name="fule_pump_type" id="title" autocomplete="off" value="{{$model->fule_pump_type}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title"> Emission Standard:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Enter Emission Standard" name="emission_standard" id="title" autocomplete="off" value="{{$model->emission_standard}}">
                                                    </div>
                                                </div>

                                            </div>
                                            <h4><b>Transmission</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Clutch:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Clutch" name="clutch" id="clutch" autocomplete="off" value="{{$model->clutch}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="gearbox"> Gear Box:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="gearbox" name="gearbox" id="gearbox" autocomplete="off" value="{{$model->gearbox}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Gear Speed:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="gear speed" name="gear_speed" id="gear_speed" autocomplete="off" value="{{$model->gear_speed}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Forward Speed:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="forward_speed" name="forward_speed" id="forward_speed" autocomplete="off" value="{{$model->forward_speed}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Reverse Speed:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="reverse_speed" name="reverse_speed" id="reverse_speed" autocomplete="off" value="{{$model->reverse_speed}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Gear Lever Position:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder=" Gear Lever Position" name="gear_lever_position" id="gear_lever_position" autocomplete="off" value="{{$model->gear_lever_position}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Brakes:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Brakes" name="brakes" id="brakes" autocomplete="off" value="{{$model->brakes}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="clutch"> Rear axle:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="rear_axle" name="rear_axle" id="rear_axle" autocomplete="off" value="{{$model->rear_axle}}">
                                                    </div>
                                                </div>

                                            </div>

                                            <h4><b>Steering</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Steering Type:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Steering Type" name="steering_type" id="title" autocomplete="off" value="{{$model->steering_type}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Steering Adjustment:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Steering Adjustment" name="steering_adjustment" id="title" autocomplete="off" value="{{$model->steering_adjustment}}">
                                                    </div>
                                                </div>


                                            </div>
                                            <h4><b>Power Take Off</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">PTO HP:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="PTO HP" name="pto_hp" id="title" autocomplete="off" value="{{$model->pto_hp}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">PTO Speed:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="PTO Speed" name="pto_speed" id="title" autocomplete="off" value="{{$model->pto_speed}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">RPM:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="rpm" name="rpm" id="title" autocomplete="off" value="{{$model->rpm}}">
                                                    </div>
                                                </div>


                                            </div>
                                            <h4><b>Fuel Capacity</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Capacity:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Capacity" name="fule_capacity" id="title" autocomplete="off" value="{{$model->fule_capacity}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4><b>Hydraulics</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Lifting Capacity:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Lifting Capacity" name="lifting_capacity" id="title" autocomplete="off" value="{{$model->lifting_capacity}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">3 point Linkage:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="3 point Linkage" name="point3_linkage" id="title" autocomplete="off" value="{{$model->point3_linkage}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Hydraulic Controls:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Hydraulic Controls" name="hydraulic_controls" id="title" autocomplete="off" value="{{$model->hydraulic_controls}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Remote / Auxillary Valve:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Remote / Auxillary Valve" name="remote_auxillaryvalve" id="title" autocomplete="off" value="{{$model->remote_auxillaryvalve}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4><b>Tyre Size</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Wheel drive:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Wheel drive" name="wheel_drive" id="title" autocomplete="off" value="{{$model->wheel_drive}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Front:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Front" name="tyre_front" id="title" autocomplete="off" value="{{$model->tyre_front}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Rear:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Rear" name="tyre_rear" id="title" autocomplete="off" value="{{$model->tyre_rear}}">
                                                    </div>
                                                </div>

                                            </div>
                                            <h4><b>Dimensions And Weight</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Total Weight:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Total Weight" name="total_weight" id="title" autocomplete="off" value="{{$model->total_weight}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Wheel Base:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Wheel Base" name="wheel_base" id="title" autocomplete="off" value="{{$model->wheel_base}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Overall Length:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Overall Length" name="overall_length" id="title" autocomplete="off" value="{{$model->overall_length}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Overall Width:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Overall Width" name="overall_width" id="title" autocomplete="off" value="{{$model->overall_width}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Ground Clearance:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Ground Clearance" name="ground_clearance" id="title" autocomplete="off" value="{{$model->ground_clearance}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Turning radius with brake:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Turning radius with brake" name="turning_radius_with_brake" id="title" autocomplete="off" value="{{$model->turning_radius_with_brake}}">
                                                    </div>
                                                </div>



                                            </div>
                                            <h4><b>Electrical</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Battery:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Battery " name="battery" id="title" autocomplete="off" value="{{$model->battery}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Alternator:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Alternator " name="alternator" id="title" autocomplete="off" value="{{$model->alternator}}">
                                                    </div>
                                                </div>


                                            </div>
                                            <h4><b>Safety Features</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Neutral Safety Switch:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Neutral Safety Switch " name="neutral_safety_switch" id="title" autocomplete="off" value="{{$model->neutral_safety_switch}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Differential Lock:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Differential Lock " name="differential_lock" id="title" autocomplete="off" value="{{$model->differential_lock}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">ROPS:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Rops" name="rops" id="title" autocomplete="off" value="{{$model->rops}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Clutch Safety Lock:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Clutch Safety Lock" name="clutch_safety_lock" id="title" autocomplete="off" value="{{$model->clutch_safety_lock}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <h4><b>Other</b></h4>
                                            <div class="row">
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Warranty:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Warranty" name="warranty" id="title" autocomplete="off" value="{{$model->warranty}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Instrument Cluster:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Instrument Cluster" name="instrument_cluster" id="title" autocomplete="off" value="{{$model->instrument_cluster}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Platform:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Platform" name="platform" id="title" autocomplete="off" value="{{$model->platform}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Driver Seat:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Driver Seat" name="driver_seat" id="title" autocomplete="off" value="{{$model->driver_seat}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Accessories:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Accessories" name="accessories" id="title" autocomplete="off" value="{{$model->accessories}}">
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <div class="controls">
                                                        <label for="title">Additional Features:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Additional Features" name="additional_features" id="title" autocomplete="off" value="{{$model->additional_features}}">
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="footer" align="center">
                                                <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</div>
<script>
    <?php if ($model->series_id != "" && $model->series_id != "NA") { ?>


        getData('<?php echo $model->brand_id ?>', '<?php echo $model->series_id ?>');
    <?php }  ?>

    $(document).ready(function() {


        //
        // $('#model_type').on('change', function() {
        //     //$('#allteam').hide(300);
        //     var model_type = $(this).val();
        //     if (model_type == "tractor") {
        //         $("#series_Div").show(300)
        //     } else {
        //         $("#series_Div").hide(300)
        //     }

        // });
        $('#brand_id').on('change', function() {
            //$('#allteam').hide(300);
            var brand_id = $(this).val();

            getData(brand_id, 0);

        });
    });

    function getData(brand_id, series_id) {
        var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
        $.ajax({
            url: "{{ route('getbrandSerices') }}",
            type: "POST",
            data: {
                "brand_id": brand_id,
                "series_id": series_id,
                "_token": CSRF_TOKEN
            },
            dataType: "json",
            success: function(response) {
                $('#series_id').html(response);

            }
        });
    }
</script>
<script>
    $(document).ready(function() {
        $('#description').summernote({
            height: 300,
        });
        $('#description2').summernote({
            height: 300,
        });
    });
</script>
<!--  ADD USER MODAL -->



@endsection