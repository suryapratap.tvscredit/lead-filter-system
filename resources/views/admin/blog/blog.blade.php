@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            Blog
                                        </div>

                                        <div class="container-fluid ">
                                            <div class="row ">
                                                <div class="col-sm-12 ">
                                                    <a href="{{ route('Blog.add') }}"> <button class="buttonplay"> <i
                                                                class="fa fa-plus icon-white"><span></i> Add </span>
                                                        </button></a>
                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Blog Category</th>
                                                    <th>Title</th>
                                                    <!-- <th>Short Description</th> -->
                                                    <!-- <th>Description</th> -->
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($blog as $cat)
                                                    <tr class="gradeX">
                                                        <td>{{ $sno++ }}</td>
                                                        <td>{{ $cat->blogcategory }}</td>
                                                        <td>{{ $cat->title }}</td>
                                                        <!-- <td>{{ $cat->shortdesc }}</td> -->
                                                        <!-- <td>{!! $cat->longdesc !!}</td> -->
                                                        <td> <?php if ($cat->status == 'active') {  ?>
                                                            <label class="label label-success">
                                                                {{ ucfirst($cat->status) }}</label> <?php } else { ?>
                                                            <label class="label label-warning">
                                                                {{ ucfirst($cat->status) }}</label>
                                                            <?php } ?>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group"
                                                                style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                <a href="{{ route('Blog.edit', $cat->id) }}"> <i
                                                                        class=" fa fa-edit" style="padding:5px"></i></a>
                                                                <a href="{{ route('Blog.delete', $cat->id) }}"><i
                                                                        class="fa fa-trash"
                                                                        style="color: #b91010;padding:5px"></i></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>
    </div>
    </div>
    </div>
    </div>
    <script>
        $('#datatable').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
            ],
            // scrollX: 300,
            responsive: true,
        });
    </script>
    <!--  ADD  MODAL -->

    <!-- END -->
    <!-- Edit USER MODAL -->
    <div id="edit-modal" name="edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="edit_modal" id="edit_modal" enctype="multipart/form-data"
                onsubmit="return confirm('Do you really want to update the form?');" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Category</h4>
                    </div>

                    <input type="hidden" id="uid" name="uid">
                    <div class="modal-body" id="form">
                        <div class="form-group">
                            <label>Category:</label>
                            <select class="form-control select2bs4" name="ucategory" id="ucategory" required>
                                <option value=""> Select Category</option>
                                @foreach ($category as $cat)
                                    <option value="{{ $cat->id }}"> {{ $cat->category }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Blog:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Blog" name="ublog"
                                    id="ublog" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label for="title">Title:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Title" name="utitle"
                                    id="utitle" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Short Description:</label><span class="err" id="err_title"></span>
                                <textarea type="text" class="form-control" class="text" placeholder="Short Description" name="ushortdesc"
                                    id="ushortdesc" autocomplete="off" required></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Description:</label><span class="err" id="err_title"></span>
                                <textarea type="text" class="form-control" class="text" placeholder="Description" name="ulongdesc"
                                    id="ulongdesc" autocomplete="off" required></textarea>
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="controls">
                                <label for="title">Status:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="ustatus" id="ustatus" required>
                                    <option value=""> Select Status</option>
                                    <option value="active"> Active</option>
                                    <option value="inactive"> Inactive</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="esubmit" name="esubmit" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
