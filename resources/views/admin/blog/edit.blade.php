@extends('admin.include.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('public/css/summernote.css') }}">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h4>ADD USER </h4>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <br />
                                            <form role="form" id="quickForm" action="{{ route('Blog.update') }}"
                                                enctype="multipart/form-data" method="POST">
                                                <div class="card-body">
                                                    @csrf
                                                    <div class="row">
                                                        <input type="hidden" value="{{ $blog->id }}" name="id"
                                                            id="id">
                                                        {{-- <div class="form-group  col-md-3">
                                                        <label for="title">Blog Type:</label><span class="err" id="err_title"></span>
                                                        <select class="form-control select2bs4" name="blog_type" id="blog_type" required>
                                                            <option value=""> Select Status</option>
                                                            <option value="blog" {{($blog->blog_type=="blog") ? 'selected': ''}}> Blogs & Articles</option>
                                                            <option value="updates_launch" {{($blog->blog_type=="updates_launch") ? 'selected': ''}}> Updates & Launches</option>
                                                            <option value="news_updates" {{($blog->blog_type=="news_updates") ? 'selected': ''}}> News Updates</option>
                                                        </select>
                                                        @error('status')
                                                        <span class="invalid-feedback" role="alert" style="color:red;">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div> --}}
                                                        <div class="form-group  col-md-3">

                                                            <label>Blog Category:</label>
                                                            <select class="form-control select2bs4" name="blogcategory_id"
                                                                id="blogcategory_id" required>
                                                                <option value=""> Select Blog Category</option>
                                                                @foreach ($blogcategory as $cat)
                                                                    <option value="{{ $cat->id }}"
                                                                        {{ $cat->id == $blog->blogcategory_id ? 'selected' : '' }}>
                                                                        {{ $cat->blogcategory }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('blogcategory')
                                                                <span class="invalid-feedback" role="alert"
                                                                    style="color:red;">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="form-group  col-md-3">
                                                            <label>Title</label>
                                                            <input type="text" value="{{ $blog->title }}"
                                                                class="form-control" name="title" id="title"
                                                                placeholder="Title" required>
                                                            @error('title')
                                                                <span class="invalid-feedback" role="alert"
                                                                    style="color:red;">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>


                                                        <div class="form-group  col-md-3">
                                                            <label for="title">Status:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4" name="status"
                                                                id="status" required>
                                                                <option value=""> Select Status</option>
                                                                <option value="active"
                                                                    {{ $blog->status == 'active' ? 'selected' : '' }}>
                                                                    Active</option>
                                                                <option value="inactive"
                                                                    {{ $blog->status == 'inactive' ? 'selected' : '' }}>
                                                                    Inactive</option>
                                                            </select>
                                                            @error('status')
                                                                <span class="invalid-feedback" role="alert"
                                                                    style="color:red;">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>

                                                        <div class="form-group  col-md-3">
                                                            <label>Author</label>
                                                            <input type="text" class="form-control" id="author"
                                                                name="author" placeholder="Author"
                                                                value="{{ $blog->author }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <div class="timepicker">
                                                                <label>Blog Date</label>
                                                                <div class='input-group' id=''>
                                                                    <input type="text" id="myDatepicker3"
                                                                        placeholder="Select blog date" name="blog_date"
                                                                        class="form-control Handover"
                                                                        value="{{ $blog->blog_date }}">
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-time"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label for="title"> Blog image</label><span class="err"
                                                                id="err_title"></span>
                                                            <input type="file" class="form-control" class="text"
                                                                placeholder="blog image" name="blog_image" id="blog_image"
                                                                autocomplete="off">

                                                        </div>


                                                        <?php if (isset($blog->blog_image)) { ?>
                                                        @if ($blog->blog_image != '')
                                                            <?php
                                                            $file = explode('/', $blog->blog_image);
                                                            $fileurl = $file['2']; ?>
                                                            <div class="form-group col-md-3">

                                                                <img style="width:100;height:80px;"
                                                                    src="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>">
                                                            </div>
                                                        @endif
                                                        <?php } ?>


                                                        <div class=" row">
                                                            <label>Category:</label>
                                                            <p style="padding: 5px;">

                                                                <input type="checkbox" name="is_populer" id="is_populer"
                                                                    value="1" class="flat"
                                                                    {{ $blog->is_populer == '1' ? 'checked' : '' }} />
                                                                Popular Stories


                                                            <p>
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>Short Description</label>
                                                            <textarea type="text" class="form-control" id="shortdesc" name="shortdesc" placeholder="Short Description"
                                                                required>{{ $blog->shortdesc }}</textarea>
                                                            @error('shortdesc')
                                                                <span class="invalid-feedback" role="alert"
                                                                    style="color:red;">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>




                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label> Description</label>
                                                                <textarea type="text" class="form-control" id="longdesc" name="longdesc" placeholder="Description" required>{{ $blog->longdesc }}</textarea>
                                                                @error('longdesc')
                                                                    <span class="invalid-feedback" role="alert"
                                                                        style="color:red;">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>


                                                        <div class="form-group  col-md-6">
                                                            <label>Meta Title</label>
                                                            <input type="text" value="{{ $blog->metatitle }}"
                                                                class="form-control" id="metatitle" name="metatitle"
                                                                placeholder="Meta Title">
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>Meta Keyword</label>
                                                            <input type="text" value="{{ $blog->metakeyword }}"
                                                                class="form-control" id="metakeyword" name="metakeyword"
                                                                placeholder="Meta Keyword">
                                                        </div>

                                                        <div class="form-group  col-md-6">
                                                            <label>Meta Description</label>
                                                            <textarea type="text" class="form-control" id="metadescription" name="metadescription"
                                                                placeholder="Meta Description">{{ $blog->metadescription }}</textarea>
                                                        </div>



                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- /.card-body -->
                                                    <button type="submit" class="btn btn-primary"
                                                        style="margin-left: 40%;margin-top: 20px;">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>


    <script>
        $(document).ready(function() {
            $('#longdesc').summernote({
                height: 300,
            });

        });
    </script>
@endsection
