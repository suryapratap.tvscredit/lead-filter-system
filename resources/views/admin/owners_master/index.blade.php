@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>


        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css" />
        <link rel="stylesheet" type="text/css"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->

                                            <div class="container-fluid ">
                                                <div class="row ">
                                                    <div class="col-sm-12 ">
                                                        <a href="{{ route('owners_master.add') }}">
                                                            <button class="buttonplay"> <i
                                                                    class="fa fa-plus icon-white"><span></i> Add Owners
                                                                </span> </button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Owners</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($owners as $cat)
                                                    <tr class="row1" data-id="{{ $cat->id }}">
                                                        <td>{{ $sno++ }}</td>

                                                        <td>{!! $cat->title !!}</td>

                                                        <td>
                                                            <div class="btn-group"
                                                                style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                <a href="{{ route('owners_master.edit', $cat->id) }}"> <i
                                                                        class=" fa fa-edit" style="padding:5px"></i></a>
                                                                <a href="{{ route('owners_master.delete', $cat->id) }}"><i
                                                                        class="fa fa-trash"
                                                                        style="color: #b91010;padding:5px"></i></a>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            {{-- <h5>Drag and Drop the table rows and <button class="btn btn-success btn-sm"
                                                    onclick="window.location.reload()">REFRESH</button> the page to check
                                                the Demo.</h5> --}}
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script>
        $(function() {
            $('#datatable').dataTable();
            // $("#datatable").sortable({
            //     items: "tr",
            //     cursor: 'move',
            //     opacity: 0.6,
            //     update: function() {
            //         sendOrderToServer();
            //     }
            // });

            // function sendOrderToServer() {
            //     var order = [];
            //     var token = $('meta[name="csrf-token"]').attr('content');
            //     $('tr.row1').each(function(index, element) {
            //         order.push({
            //             id: $(this).attr('data-id'),
            //             position: index + 1

            //         });
            //     });

            //     $.ajax({
            //         type: "POST",
            //         dataType: "json",
            //         url: "{{ url('/admin/post-sortable') }}",
            //         data: {
            //             order: order,
            //             _token: token,
            //             brand_id: $(this).attr('brand_id')
            //         },
            //         success: function(response) {
            //             if (response.status == "success") {
            //                 console.log(response);
            //             } else {
            //                 console.log(response);
            //             }
            //         }
            //     });
            // }
        });
    </script>
@endsection
