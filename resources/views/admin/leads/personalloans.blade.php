@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3><b>Personal Loans Leads</b></h3>
                                        </div>


                                        <div class="row">

                                            {{-- <div class="col-sm-4">
                                                <button class="btn btn-success" data-target="#addModal" data-toggle="modal">
                                                    <i class="fa fa-plus icon-white"><span></i> Add Leads </span>
                                                </button>
                                            </div> --}}
                                            {{-- <div class="col-sm-4">
                                                <a href="{{ asset('public/tractor.csv') }}" target="_BLANK"> <button
                                                        class="btn btn-warning"> <i
                                                            class="fa fa-download icon-white"><span></i> Download Upload
                                                        Format </span> </button> </a>
                                            </div> --}}

                                        </div>

                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Mobile</th>
                                                    <th>Name</th>
                                                    <th>Pan number</th>
                                                    <th>Location</th>
                                                    <th>Do you own a tractor?</th>
                                                    <th>Do you have an extisting loan?</th>

                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($personalloans as $cat)
                                                    <tr class="gradeX">
                                                        <td>{{ $sno++ }}</td>

                                                        <td>{!! $cat->mobile !!}</td>
                                                        <td>{!! $cat->fullname !!}</td>
                                                        <td>{!! $cat->pan_number !!}</td>

                                                        <td> {{ $cat->state_title }},{{ $cat->district }},{{ $cat->tahshil }}
                                                        </td>
                                                        <td>{!! $cat->is_tractor !!}</td>
                                                        <td>{!! $cat->is_loan !!}</td>

                                                        {{-- <td>
                                                            <div class="btn-group"
                                                                style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                <a href="{{ route('models.edit', $cat->id) }}"> <i
                                                                        class=" fa fa-edit" style="padding:5px"></i></a>
                                                                <a href="{{ route('models.delete', $cat->id) }}"><i
                                                                        class="fa fa-trash"
                                                                        style="color: #b91010;padding:5px"></i></a>
                                                                <a href="{{ route('gallery.images', $cat->id) }}"><i
                                                                        class="fa fa-picture-o"
                                                                        style="color: #b91010;padding:5px"></i></a>


                                                            </div>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('freedoorstepleadInsert') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Name:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Enter  name"
                                    name="customername" id="customername" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Mobile:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Enter mobile"
                                    name="mobile" id="mobile" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> PAN number:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Enter PAN number"
                                    name="pan_number" id="pan_number" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title">State:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="state_id" id="state_id" required>
                                <option value=""> Select State</option>
                                <?php foreach ($states as $b) { ?>
                                <option value="{{ $b->id }}">{{ $b->name }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">District:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="district_id" id="district_id" required>
                                <option value=""> Select District</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Tahshil:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="tahshil_id" id="tahshil_id" required>
                                <option value=""> Select Tahshil</option>

                            </select>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $('#datatable').dataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excelHtml5',
                title: 'Personal Loan Leads {{ date('d-m-Y') }}'
            }]
        });
        $('#state_id').on('change', function() {
            //$('#allteam').hide(300);
            var state_id = $(this).val();
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#district_id').html(response);

                }
            });
        });
        $('#district_id').on('change', function() {
            //$('#allteam').hide(300);
            var district_id = $(this).val();
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getDistrictsTahshil') }}",
                type: "POST",
                data: {
                    "district_id": district_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#tahshil_id').html(response);

                }
            });
        });
    </script>
@endsection
