@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3><b>Online Leads</b></h3>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-4">
                                                <button class="btn btn-success" data-target="#addModal" data-toggle="modal">
                                                    <i class="fa fa-plus icon-white"><span></i> Add Leads </span>
                                                </button>
                                            </div>
                                            {{-- <div class="col-sm-4">
                                                <a href="{{ asset('public/tractor.csv') }}" target="_BLANK"> <button
                                                        class="btn btn-warning"> <i
                                                            class="fa fa-download icon-white"><span></i> Download Upload
                                                        Format </span> </button> </a>
                                            </div> --}}

                                        </div>

                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Mobile</th>
                                                    <th>Name</th>
                                                    <th>PAN number</th>
                                                    <th>Brand</th>
                                                    <!-- <th>Serise</th> -->
                                                    <th>Model</th>
                                                    <th>Location</th>
                                                    <th>Year</th>
                                                    <th>Ownership</th>
                                                    <th> Hours driven</th>
                                                    <th> Engine</th>
                                                    <th> Tyre</th>
                                                    <th>Images</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($leads as $cat)
                                                    <tr class="gradeX">
                                                        <td>{{ $sno++ }}</td>

                                                        <td>{!! $cat->mobile !!}</td>
                                                        <td>{!! $cat->username !!}</td>
                                                        <td>{!! $cat->pan_number !!}</td>
                                                        <td>{!! $cat->brand_title !!}</td>
                                                        <td>{!! $cat->models_title !!}</td>

                                                        <td> {{ $cat->state_title }},{{ $cat->district }},{{ $cat->tahshil }}
                                                        </td>
                                                        <td>{!! $cat->year !!}</td>
                                                        <td>{!! $cat->owners_title !!}</td>
                                                        <td>{!! $cat->hours_title !!}</td>
                                                        <td>{!! $cat->engine !!}</td>
                                                        <td>{!! $cat->tyre !!}</td>
                                                        <td><?php if (isset($cat->front_image)) { ?>
                                                            @if ($cat->front_image != '')
                                                                <?php
                                                                $file = explode('/', $cat->front_image);
                                                                $fileurl = $file['2']; ?>
                                                                <a download="front_image"
                                                                    href="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                                                    class="label label-warning"><i
                                                                        class="fa fa-download"></i> Front
                                                                    Image</a>
                                                            @endif
                                                            <?php } ?>
                                                            <?php if (isset($cat->back_image)) { ?>
                                                            @if ($cat->back_image != '')
                                                                <?php
                                                                $file = explode('/', $cat->back_image);
                                                                $fileurl = $file['2']; ?>
                                                                <a download="back_image"
                                                                    href="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                                                    class="label label-warning"><i
                                                                        class="fa fa-download"></i> Back
                                                                    Image</a>
                                                            @endif
                                                            <?php } ?>
                                                            <?php if (isset($cat->leftside_image)) { ?>
                                                            @if ($cat->leftside_image != '')
                                                                <?php
                                                                $file = explode('/', $cat->leftside_image);
                                                                $fileurl = $file['2']; ?>
                                                                <a download="leftside_image"
                                                                    href="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                                                    class="label label-warning"><i
                                                                        class="fa fa-download"></i> Left Side
                                                                    Image</a>
                                                            @endif
                                                            <?php } ?>
                                                            <?php if (isset($cat->rightside_image)) { ?>
                                                            @if ($cat->rightside_image != '')
                                                                <?php
                                                                $file = explode('/', $cat->rightside_image);
                                                                $fileurl = $file['2']; ?>
                                                                <a download="rightside_image"
                                                                    href="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                                                    class="label label-warning"> <i
                                                                        class="fa fa-download"></i> Side
                                                                    Image</a>
                                                            @endif
                                                            <?php } ?>
                                                        </td>

                                                        {{-- <td>
                                                            <div class="btn-group"
                                                                style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                <a href="{{ route('models.edit', $cat->id) }}"> <i
                                                                        class=" fa fa-edit" style="padding:5px"></i></a>
                                                                <a href="{{ route('models.delete', $cat->id) }}"><i
                                                                        class="fa fa-trash"
                                                                        style="color: #b91010;padding:5px"></i></a>
                                                                <a href="{{ route('gallery.images', $cat->id) }}"><i
                                                                        class="fa fa-picture-o"
                                                                        style="color: #b91010;padding:5px"></i></a>


                                                            </div>
                                                        </td> --}}
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="addModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('leadInsert') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Lead </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf
                        <div class="form-group col-md-4">
                            <div class="controls">
                                <label for="title"> Name:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Enter  name"
                                    name="customername" id="customername" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="controls">
                                <label for="title"> Mobile:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Enter mobile"
                                    name="mobile" id="mobile" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <div class="controls">
                                <label for="title"> PAN number:</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" placeholder="Enter PAN number"
                                    name="pan_number" id="pan_number" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Brand:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4 getmodel" name="brand_id" id="brand_id" required>
                                <option value=""> Select Brand</option>
                                <?php foreach ($brand as $b) { ?>
                                <option value="{{ $b->id }}">{{ $b->title }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Year:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4 getmodel1" name="year_id" id="year_id" required>
                                <option value=""> Select year</option>
                                <?php foreach ($year as $b) { ?>
                                <option value="{{ $b->id }}">{{ $b->year }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Model:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="model_id" id="model_id" required>
                                <option value=""> Select Model</option>

                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">State:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="state_id" id="state_id" required>
                                <option value=""> Select State</option>
                                <?php foreach ($states as $b) { ?>
                                <option value="{{ $b->id }}">{{ $b->name }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">District:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="district_id" id="district_id" required>
                                <option value=""> Select District</option>

                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Tahshil:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="tahshil_id" id="tahshil_id" required>
                                <option value=""> Select Tahshil</option>

                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">owners:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="owner_id" id="owner_id" required>
                                <option value=""> Select owners</option>
                                <?php foreach ($owners as $b) { ?>
                                <option value="{{ $b->id }}">{{ $b->title }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Hours:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="hour_id" id="hour_id" required>
                                <option value=""> Select hours</option>
                                <?php foreach ($hours as $b) { ?>
                                <option value="{{ $b->id }}">{{ $b->title }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Engine Condition:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="engine" id="engine" required>
                                <option value=""> Select Engine Condition</option>

                                <option value="0-25% (Poor)">0-25% (Poor)</option>
                                <option value="26-50% (Average)">26-50% (Average)</option>
                                <option value="51-75% (Good)">51-75% (Good)</option>
                                <option value="76-100% (Very Good)">76-100% (Very Good)</option>

                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="title">Tyre Condition:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="tyre" id="tyre" required>
                                <option value=""> Select Tyre Condition</option>
                                <option value="0-25% (Poor)">0-25% (Poor)</option>
                                <option value="26-50% (Average)">26-50% (Average)</option>
                                <option value="51-75% (Good)">51-75% (Good)</option>
                                <option value="76-100% (Very Good)">76-100% (Very Good)</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $('.getmodel').on('change', function() {
            //$('#allteam').hide(300);
            var brand_id = $("#brand_id").val();
            //var year_id = $("#year_id").val();
            $.ajax({
                url: "{{ route('getModelbyBrandYear') }}",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: "POST",
                data: {
                    "brand_id": brand_id,
                    //"year_id": year_id,

                },
                dataType: "json",
                success: function(response) {
                    $('#model_id').html(response);
                }
            });

        });
        $('#datatable').dataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excelHtml5',
                title: 'Online Lead {{ date('d-m-Y') }}',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
                },
            }]
        });

        $('#state_id').on('change', function() {
            //$('#allteam').hide(300);
            var state_id = $(this).val();
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#district_id').html(response);

                }
            });
        });
        $('#district_id').on('change', function() {
            //$('#allteam').hide(300);
            var district_id = $(this).val();
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getDistrictsTahshil') }}",
                type: "POST",
                data: {
                    "district_id": district_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#tahshil_id').html(response);

                }
            });
        });
    </script>
@endsection
