@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->
                                            @if (Session::has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('success') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="clearfix"></div>
                                            @endif
                                            @if (Session::has('update'))
                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('update') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            @if (Session::has('error'))
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('error') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            <div class="container-fluid ">

                                            </div>
                                            </br>
                                            <form method="post" role="form" action="{{ route('mapBrandSubmit') }}"
                                                name="frmlead" id="frmlead" enctype="multipart/form-data"
                                                onsubmit="return confirm('Do you really want to submit the form?');"
                                                data-parsley-validate>

                                                <div class="body" id="form">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label>Wrong Brand Name:</label>

                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Correct Brand Value:</label>

                                                        </div>
                                                    </div>
                                                    @if (!empty($resultData) && $resultData->count())
                                                        @foreach ($resultData as $s)
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <input type="hidden" name="id[]"
                                                                        value="{{ $s->id }}">
                                                                    <input type="text" class="form-control"
                                                                        class="text" placeholder="content"
                                                                        name="E{{ $s->id }}"
                                                                        id="E{{ $s->id }}"
                                                                        value="{{ $s->error_value }}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-4">

                                                                    <select class="form-control select2bs4 stateValue"
                                                                        name="B{{ $s->id }}">
                                                                        <option value=""> Select Brand</option>
                                                                        @foreach ($resultBrand as $cat)
                                                                            <option value="{{ $cat->brand_name }}">
                                                                                {{ $cat->brand_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif

                                                </div>
                                                <div class="footer" align="center">
                                                    <button type="submit" id="submit" name="submit"
                                                        class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>

    </div>
    </div>
    </div>
    </div>
    <script></script>
    <!--  ADD USER MODAL -->
@endsection
