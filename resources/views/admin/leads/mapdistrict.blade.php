@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->
                                            @if (Session::has('success'))
                                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('success') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="clearfix"></div>
                                            @endif
                                            @if (Session::has('update'))
                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('update') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            @if (Session::has('error'))
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <strong>{{ Session::get('error') }}</strong>
                                                    <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            <div class="container-fluid ">

                                            </div>
                                            </br>
                                            <form method="post" role="form" action="{{ route('mapDistrictSubmit') }}"
                                                name="frmlead" id="frmlead" enctype="multipart/form-data"
                                                onsubmit="return confirm('Do you really want to submit the form?');"
                                                data-parsley-validate>

                                                <div class="body" id="form">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <label>Wrong District Name:</label>

                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label> State :</label>

                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Correct District Value:</label>

                                                        </div>
                                                    </div>
                                                    @if (!empty($resultData) && $resultData->count())
                                                        @foreach ($resultData as $s)
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <input type="hidden" name="id[]"
                                                                        value="{{ $s->id }}">
                                                                    <input type="text" class="form-control"
                                                                        class="text" placeholder="content"
                                                                        name="E{{ $s->id }}"
                                                                        id="E{{ $s->id }}"
                                                                        value="{{ $s->error_value }}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <select class="form-control select2bs4 stateValue"
                                                                        name="S{{ $s->id }}"
                                                                        uid={{ $s->id }}>
                                                                        <option value=""> Select State</option>
                                                                        @foreach ($resultDataState as $cat)
                                                                            <option value="{{ $cat->id }}">
                                                                                {{ $cat->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <select
                                                                        class="form-control select2bs4 districtvalue{{ $s->id }}"
                                                                        name="D{{ $s->id }}">
                                                                        <option value=""> Select District</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif

                                                </div>
                                                <div class="footer" align="center">
                                                    <button type="submit" id="submit" name="submit"
                                                        class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).on("change", ".stateValue", function(e) {
            e.preventDefault();

            var state_id = $(this).val();
            //alert(state_id);
            var uid = $(this).attr("uid");

            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('.districtvalue' + uid).html(response);

                }
            });
        });
    </script>
    <!--  ADD USER MODAL -->
@endsection
