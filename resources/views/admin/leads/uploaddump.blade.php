@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3><b>Upload Dump</b></h3>
                                        </div>


                                        <div class="row">

                                            <div class="col-sm-3">
                                                <button class="btn btn-success" data-target="#addModal" data-toggle="modal">
                                                    <i class="fa fa-plus icon-white"><span></i>
                                                    Import dump </span> </button>
                                            </div>
                                            @if (!empty($resultData) && $resultData->count())
                                                {{-- <div class="col-sm-4">
                                                    <button class="btn btn-success" data-target="#stateFilter"
                                                        data-toggle="modal">
                                                        <i class="fa fa-plus icon-white"><span></i>
                                                        State Filter</span> </button>
                                                </div> --}}
                                                {{-- <div class="col-sm-3">
                                                    <a href="{{ route('stateFilter') }}"><button class="btn btn-success">
                                                            <i class="fa fa-plus icon-white"><span></i>
                                                            State Filter</span> </button> </a>
                                                </div> --}}
                                                <div class="col-sm-3">
                                                    <a href="{{ route('mapnewdata') }}"><button class="btn btn-success">
                                                            <i class="fa fa-plus icon-white"><span></i>
                                                            Map New Value </span> </button> </a>
                                                </div>
                                            @endif

                                        </div>

                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th> S.no </th>
                                                    <th> Lead Source </th>
                                                    <th> Loan Type </th>
                                                    <th> Lead Type </th>
                                                    <th> Enq ID </th>
                                                    <th> Mobile </th>
                                                    <th> Name </th>
                                                    <th> State </th>
                                                    <th> District </th>
                                                    <th> Tehsil </th>
                                                    <th> Product Category </th>
                                                    <th> Brand </th>
                                                    <th> Model </th>
                                                    <th> Web Site </th>
                                                    <th> Lead Date </th>
                                                    <th> Created On </th>
                                                    <th> Zip </th>
                                                    <th> Pancard </th>




                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @if (!empty($resultData) && $resultData->count())
                                                    @foreach ($resultData as $cat)
                                                        <tr class="gradeX">
                                                            <td>{{ $sno++ }}</td>
                                                            <td>{!! $cat->lead_source !!}</td>
                                                            <td>{!! $cat->loan_type !!}</td>
                                                            <td>{!! $cat->lead_type !!}</td>
                                                            <td>{!! $cat->enq_id !!}</td>
                                                            <td>{!! $cat->mobile !!}</td>
                                                            <td>{!! $cat->name !!}</td>
                                                            <td>{!! $cat->tkstate !!}</td>
                                                            <td>{!! $cat->tkdistrict !!}</td>
                                                            <td>{!! $cat->tktehsil !!}</td>
                                                            <td>{!! $cat->tkproduct_category !!}</td>
                                                            <td>{!! $cat->tkbrand !!}</td>
                                                            <td>{!! $cat->tkmodel !!}</td>
                                                            <td>{!! $cat->website !!}</td>
                                                            <td>{!! $cat->lead_date !!}</td>
                                                            <td>{!! $cat->created_on !!}</td>
                                                            <td>{!! $cat->zip !!}</td>
                                                            <td>{!! $cat->pancard !!}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('dumpimport') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" name="select_file"
                                    id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <div id="stateFilter" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmstateFilter" id="frmstateFilter" enctype="multipart/form-data"
                action="{{ route('stateFilter') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" name="select_file"
                                    id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $('#datatable').dataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excelHtml5',
            }],
            "scrollX": true
        });
    </script>
@endsection
