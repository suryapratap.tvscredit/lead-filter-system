@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3>Location</h3>
                                    </div>
                                    
                                    <form method="post" role="form" action="#" name="frmlead" id="frmlead" enctype="multipart/form-data" data-parsley-validate>

                                        <div class="body" id="form">
                                            @csrf


                                            <div class="row">

                                            <div class="form-group col-md-4">
                                                    <label for="title">State:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="state_id" id="state_id" required>
                                                        <option value=""> Select State</option>
                                                        <?php foreach ($states as $b) { ?>
                                                            <option value="{{$b->id}}">{{$b->name}}</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="title">District:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="district_id" id="district_id" required>
                                                        <option value=""> Select District</option>
                                                        
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="title">Tahshil:</label><span class="err" id="err_title"></span>
                                                    <select class="form-control select2bs4" name="tahshil_id" id="tahshil_id" required>
                                                        <option value=""> Select Tahshil</option>
                                                        
                                                    </select>
                                                </div>
                                               


                                            </div>
                                           

                                           
                                        </div>
                                        <div class="footer" align="center">
                                            <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</div>
</div>
</div>
</div>
<script>
    $(document).ready(function() {
        $('#state_id').on('change', function() {
            //$('#allteam').hide(300);
            var state_id = $(this).val();


            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#district_id').html(response);

                }
            });
        });
        $('#district_id').on('change', function() {
            //$('#allteam').hide(300);
            var district_id = $(this).val();


            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getDistrictsTahshil') }}",
                type: "POST",
                data: {
                    "district_id": district_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#tahshil_id').html(response);

                }
            });
        });
    });
   
</script>
<!--  ADD USER MODAL -->



@endsection