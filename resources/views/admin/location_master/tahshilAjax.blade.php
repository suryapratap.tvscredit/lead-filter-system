@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->

                                            <div class="container-fluid ">
                                                <div class="row ">
                                                    <div class="col-sm-6 ">

                                                        <button class="buttonplay" data-target="#addModal"
                                                            data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i>
                                                            Add Tahshil
                                                            </span> </button>
                                                    </div>
                                                    {{-- <div class="col-sm-4">
                                                        <button class="btn btn-success" data-target="#addModal"
                                                            data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i>
                                                            Import Data </span> </button>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <table id="empTable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    {{-- <th>State ID</th> --}}
                                                    <th>State</th>
                                                    <th>District</th>
                                                    <th>Tahshil</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('createTahshil') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add </h4>
                    </div>
                    <div class="modal-body" id="form">
                        @csrf
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">State:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="state_id" id="state_id" required>
                                    <option value=""> Select State</option>
                                    <?php foreach ($states as $b) { ?>
                                    <option value="{{ $b->id }}">{{ $b->name }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">District:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="district_id" id="district_id" required>
                                    <option value=""> Select District</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">Tahshil Name</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" name="tahshil" id="tahshil"
                                    autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- MODAL END -->

    <!-- END -->

    <!-- Edit USER MODAL -->
    <div id="edit-modal" name="edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="edit_modal" id="edit_modal" action="{{ route('updateTahshil') }}"
                enctype="multipart/form-data" onsubmit="return confirm('Do you really want to update the form?');"
                data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf
                        <input type="hidden" id="uid" name="uid">
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">State:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="ustate_id" id="ustate_id" required>
                                    <option value=""> Select State</option>
                                    <?php foreach ($states as $b) { ?>
                                    <option value="{{ $b->id }}">{{ $b->name }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">District:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="udistrict_id" id="udistrict_id" required>
                                    <option value=""> Select District</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">Tahshil Name</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" name="utahshil"
                                    id="utahshil" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" id="esubmit" name="esubmit" class="btn btn-primary">UPDATE</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        function getDistrict(state_id, district_id) {
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "district_id": district_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#district_id').html(response);

                }
            });
        }

        function getDistrictUpdate(state_id, district_id) {
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "district_id": district_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#udistrict_id').html(response);

                }
            });
        }

        function editDetails(id) {
            var edit = "{{ url('/admin/tahshil/edit/') }}";
            $.get(edit + '/' + id, function(tahshilData) {
                $('#uid').val(tahshilData.id);
                //$('#ustateid').val(state.stateid);
                $('#ustate_id').val(tahshilData.state_id);
                $('#utahshil').val(tahshilData.tahshil);
                //getDistrict(state_id, tahshilData.district_id)
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('getStateDistricts') }}",
                    type: "POST",
                    data: {
                        "state_id": tahshilData.state_id,
                        "district_id": tahshilData.district_id,
                        "_token": CSRF_TOKEN
                    },
                    dataType: "json",
                    success: function(response) {
                        $('#udistrict_id').html(response);

                    }
                });
                $('#edit-modal').modal('toggle');
                // getSubprocess(categories.process_id, categories.sub_process_id, 'update');
            })
        }
    </script>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('importDistrict') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" name="select_file"
                                    id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#state_id ').on('change', function() {
                //$('#allteam').hide(300);
                var state_id = $(this).val();
                getDistrict(state_id, 0)

            });
            $('#ustate_id').on('change', function() {
                //$('#allteam').hide(300);
                var state_id = $(this).val();
                getDistrictUpdate(state_id, 0)

            });
        });
        $(document).ready(function() {

            // DataTable
            $('#empTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('getTahshil') }}",
                columns: [{
                        data: 'id'
                    },
                    {
                        data: 'state_title'
                    },
                    {
                        data: 'district_title'
                    },
                    {
                        data: 'tahshil'
                    },
                    {
                        data: 'action'
                    },
                ]
            });

        });
    </script>
@endsection
