@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <!-- /.card-header -->

                                            <div class="container-fluid ">
                                                <div class="row ">
                                                    <div class="col-sm-6 ">

                                                        <button class="buttonplay" data-target="#addModal"
                                                            data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i>
                                                            Add District
                                                            </span> </button>
                                                    </div>
                                                    {{-- <div class="col-sm-4">
                                                        <button class="btn btn-success" data-target="#addModal"
                                                            data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i>
                                                            Import Data </span> </button>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    {{-- <th>State ID</th> --}}
                                                    <th>State</th>
                                                    <th>District</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($district as $cat)
                                                    <tr class="gradeX">
                                                        <td>{{ $sno++ }}</td>

                                                        {{-- <td>{!! $cat->stateid !!}</td> --}}
                                                        <td>{!! $cat->state_title !!}</td>
                                                        <td>{!! $cat->district !!}</td>

                                                        <td>
                                                            <a href="javascript:void(0)"
                                                                onclick="editDetails(<?= $cat->id ?>)"> <i
                                                                    class=" fa fa-edit" style="padding:5px"></i></a>
                                                            <a href="{{ route('deleteDistrict', $cat->id) }}"><i
                                                                    class="fa fa-trash"
                                                                    style="color: #b91010;padding:5px"></i></a>


                                    </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('createDistrict') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">State:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="state_id" id="state_id" required>
                                    <option value=""> Select State</option>
                                    <?php foreach ($states as $b) { ?>
                                    <option value="{{ $b->id }}">{{ $b->name }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">District Name</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" name="district" id="district"
                                    autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- MODAL END -->

    <!-- END -->

    <!-- Edit USER MODAL -->
    <div id="edit-modal" name="edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="edit_modal" id="edit_modal" action="{{ route('updateDistrict') }}"
                enctype="multipart/form-data" onsubmit="return confirm('Do you really want to update the form?');"
                data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf
                        <input type="hidden" id="uid" name="uid">
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">State:</label><span class="err" id="err_title"></span>
                                <select class="form-control select2bs4" name="ustate_id" id="ustate_id" required>
                                    <option value=""> Select State</option>
                                    <?php foreach ($states as $b) { ?>
                                    <option value="{{ $b->id }}">{{ $b->name }}</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="controls">
                                <label for="title">District Name</label><span class="err" id="err_title"></span>
                                <input type="text" class="form-control" class="text" name="udistrict"
                                    id="udistrict" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" id="esubmit" name="esubmit" class="btn btn-primary">UPDATE</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        function editDetails(id) {
            var edit = "{{ url('/admin/district/edit/') }}";
            $.get(edit + '/' + id, function(districtData) {
                $('#uid').val(districtData.id);
                //$('#ustateid').val(state.stateid);
                $('#ustate_id').val(districtData.state_id);
                $('#udistrict').val(districtData.district);
                $('#edit-modal').modal('toggle');
                // getSubprocess(categories.process_id, categories.sub_process_id, 'update');
            })
        }
    </script>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('importDistrict') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" name="select_file"
                                    id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $('#datatable').dataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'excelHtml5',
                title: 'Models'
            }]
        });
    </script>
@endsection
