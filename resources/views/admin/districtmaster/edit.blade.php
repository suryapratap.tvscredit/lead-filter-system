@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Edit Error Value</h3>
                                        </div>
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('success') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endif
                                        @if (Session::has('update'))
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('update') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if (Session::has('error'))
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('error') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        <div class="container-fluid ">

                                        </div>
                                        </br>
                                        <form method="post" role="form" action="{{ route('districtmaster.update') }}"
                                            name="frmlead" id="frmlead" enctype="multipart/form-data"
                                            data-parsley-validate>

                                            <div class="body" id="form">
                                                @csrf

                                                <input type="hidden" value="{{ $districtmaster->id }}" name="id"
                                                    id="id">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title">State Name:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4 stateValue"
                                                                name="state_name" id="state_name" required>
                                                                <option value=""> Select State</option>

                                                                @foreach ($resultDataState as $cat)
                                                                    <option value="{{ $cat->name }}"
                                                                        {{ $districtmaster->state_name == $cat->name ? 'selected' : '' }}>
                                                                        {{ $cat->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title">District Name:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4 stateValue"
                                                                name="district_name" id="district_name" required>
                                                                <option value=""> Select District</option>


                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Title:</label><span class="err"
                                                                id="err_title"></span>
                                                            <input type="text" class="form-control" class="text"
                                                                placeholder="enter error_value"
                                                                value="{{ $districtmaster->error_value }}"
                                                                name="error_value">
                                                        </div>
                                                    </div>




                                                </div>



                                                <div class="footer" align="center">
                                                    <button type="submit" id="submit" name="submit"
                                                        class="btn btn-primary">Submit</button>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>

    <script>
        $(document).on("change", ".stateValue", function(e) {
            e.preventDefault();

            var state_id = $(this).val();



            getdata(state_id, 0);

        });

        function getdata(state_id, district_id) {
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistrictsbyName') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "district_id": district_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#district_name').html(response);

                }
            });
        }
        <?php if(isset($districtmaster->state_name)&& ($districtmaster->state_name !="")){?>
        getdata(<?php echo "'" . $districtmaster->state_name . "'"; ?>, <?php echo "'" . $districtmaster->district_name . "'"; ?>)
        <?php } ?>
    </script>
    <!--  ADD USER MODAL -->
@endsection
