@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        Series Details
                                    </div>
                                    <div class="container-fluid ">
                                        <div class="row ">
                                            <div class="col-sm-12 ">

                                                <button class="buttonplay" data-target="#addModal" data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i> Add Series </span> </button>
                                            </div>
                                        </div>
                                    </div>
                                    </br>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sr no.</th>
                                                <th>Brand</th>
                                                <th>Serices</th>

                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php $sno = 1 ?>
                                            @foreach($sericesDetails as $team)
                                            <tr class="gradeX">
                                                <td>{{$sno++}}</td>
                                                <td>{{$team->brand_name}}</td>
                                                <td>{{$team->title}}</td>
                                                <td>
                                                    <div class="btn-group" style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                        <a href="javascript:void(0)" onclick="editDetails(<?= $team->id ?>)"> <i class=" fa fa-edit" style="padding:5px"></i></a>
                                                        <a href="{{ route('seriesDetails.delete',$team->id)}}"><i class="fa fa-trash" style="color: #b91010;padding:5px"></i></a>

                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body>

</div>
</div>
</div>
</div>
<script>
    $('#datatable').dataTable();
</script>
<!--  ADD USER MODAL -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data" action="{{route('seriesDetails.create')}}" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add </h4>
                </div>

                <div class="modal-body" id="form">
                    @csrf
                    <input type="hidden" name="brand_id" id="brand_id" value="{{$brand_id}}">
                    <div class="form-group">
                        <div class="controls">
                            <label for="title"> Serices Name</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="Serices Name" name="title" id="title" autocomplete="off" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="controls">
                            <label for="title"> Description</label><span class="err" id="err_title"></span>
                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="description" id="description" autocomplete="off"> </textarea>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="controls">
                            <label for="title"> Meta Title:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="enter meta title" name="meta_title" id="meta_title" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="controls">
                            <label for="title"> Meta Keyword:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="enter meta keyword" name="meta_keyword" id="meta_keyword" autocomplete="off">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="controls">
                                <label for="title"> Meta Description:</label><span class="err" id="err_title"></span>
                                <textarea type="text" class="form-control" class="text" placeholder="meta Description" name="meta_description" id="meta_description" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>

</div>
<!-- END -->
<!-- Edit USER MODAL -->
<div id="edit-modal" name="edit_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="post" role="form" name="edit_modal" id="edit_modal" action="{{route('seriesDetails.update')}}" enctype="multipart/form-data" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit </h4>
                </div>

                <div class="modal-body" id="form">
                    @csrf
                    <input type="hidden" id="uid" name="uid">
                    <input type="hidden" name="ubrand_id" id="ubrand_id" value="{{$brand_id}}">
                    <div class="form-group">
                        <div class="controls">
                            <label for="title"> Series Name</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="Name" name="utitle" id="utitle" autocomplete="off" required>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="controls">
                            <label for="title"> Description</label><span class="err" id="err_title"></span>
                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="udescription" id="udescription" autocomplete="off"> </textarea>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="controls">
                            <label for="title"> Meta Title:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="enter meta title" name="umeta_title" id="umeta_title" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="controls">
                            <label for="title"> Meta Keyword:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="enter meta keyword" name="umeta_keyword" id="umeta_keyword" autocomplete="off">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="controls">
                                <label for="title"> Meta Description:</label><span class="err" id="err_title"></span>
                                <textarea type="text" class="form-control" class="text" placeholder="meta Description" name="umeta_description" id="umeta_description" autocomplete="off"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="submit" id="esubmit" name="esubmit" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function editDetails(id) {
        var edit = "{{url('/admin/seriesDetails/edit/')}}";
        $.get(edit + '/' + id, function(series) {
            $('#uid').val(series.id);
            $('#utitle').val(series.title);
            $('#umeta_title').val(series.meta_title);
            $('#umeta_keyword').val(series.meta_keyword);
            $('#umeta_description').val(series.meta_description);
            // alert(series.description)
            $('#udescription').summernote('code', series.description);
            $('#udescription').html(series.description);
            $('#edit-modal').modal('toggle');
            // getSubprocess(categories.process_id, categories.sub_process_id, 'update');
        })

    }
</script>
<script>
    $(document).ready(function() {
        $('#description').summernote({
            height: 300,
        });
        $('#udescription').summernote({
            height: 300,
        });

    });
</script>
@endsection