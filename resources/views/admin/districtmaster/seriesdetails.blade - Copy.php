@extends('admin.include.master')
@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h3>Add Series Details</h3>
                                    </div>
                                    <form method="POST" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data" action="{{route('seriesDetailsSubmit.submit')}}" data-parsley-validate>
                                        @csrf
                                        <input type="hidden" class="form-control" class="text" placeholder="Title" name="brand_id" value="{{$brand_id}}">
                                        <div class="row">
                                            <div class="form-group col-md-4">
                                                <label>Title</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Add More</label>
                                            </div>

                                        </div>
                                        <div id="addmoreDiv">

                                            <?php if (isset($sericesDetails) && count($sericesDetails) > 0) { ?>
                                                <?php foreach ($sericesDetails as $aw) { ?>
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <input type="text" class="form-control" class="text" placeholder="Title" name="title[]" id="title" autocomplete="off" value="{{$aw->title}}">
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <button type="button" id="reoveMore" class="btn btn-danger removeOptions"><i class="fa fa-minus"> </i></button>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } ?>
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <input type="text" class="form-control" class="text" placeholder="Title" name="title[]" id="title" autocomplete="off">
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <button type="button" id="addMore" class="btn btn-success"><i class="fa fa-plus"> </i></button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" align="center">
                                            <button type="submit" id="submit" name="submit" class="buttonplay">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</div>
<script>
    $("#addMore").click(function() {
        $("#addmoreDiv").append('<div class="row"><div class="form-group col-md-4"><input type="text" class="form-control" class="text" placeholder="Title" name="title[]" id="title" autocomplete="off"></div><div class="form-group col-md-4"><button type="button" id="reoveMore" class="btn btn-danger removeOptions"><i class="fa fa-minus"> </i></button></div></div>');
    });

    $(document).on('click', '.removeOptions', function() {
        $(this).parent().parent().remove();
    });
    $('#datatable').dataTable({
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
        ],
        // scrollX: 300,
        responsive: true,

    });
</script>
<!--  ADD USER MODAL -->

@endsection