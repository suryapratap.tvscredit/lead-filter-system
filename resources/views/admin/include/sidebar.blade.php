<div class="col-md-3 left_col menu_fixed">
    <div class=" left_col scroll-view ">

        <div class="profile clearfix" style="background-color: #fff;">
            <div class="navbar nav_title" style="border: 0;">
                <a href="{{ route('admin.dashboard') }}" class="site_title"><i class="fa fa-home"></i> <span>Lead
                        Filter System </span></a>
            </div>

            <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->
        <hr>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">

                    <li>
                        <a href="{{ route('admin.dashboard') }}" class=""> <i
                                class="fa fa-tachometer"></i>Dashboard</a>
                    </li>

                    {{-- <li>
                        <a href="{{ route('admin.usermanagement') }}" class=""> <i class="fa fa-users"></i> User
                            Management</a>
                    </li> --}}
                    {{-- <li>
                        <a href="{{ route('uploaddump') }}" class=""> <i class="fa fa-upload"></i> Upload
                            Dump</a>
                    </li>
                    <li>
                        <a href="{{ route('mapState') }}" class=""> <i class="fa fa-handshake"></i> Map State
                        </a>
                    </li> --}}
                    <li>
                        <a href="{{ route('filterData') }}" class=""> <i class="fa fa-list"></i> Filter
                            Data</a>
                    </li>
                    {{-- <li>
                        <a href="{{ route('statedistrictbrandmapping') }}" class=""> <i class="fa fa-list"></i>
                            State District Brand Mapping
                            Data</a>
                    </li> --}}
                    <li>
                        <a><i class="fa fa-link"></i> Masters <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            {{-- <li>
                                <a href="{{ route('location_master') }}" class=""> <i class="fa fa-list"></i>
                                    Location</a>
                            </li> --}}
                            <li>
                                <a href="{{ route('states') }}" class=""> <i class="fa fa-list"></i>
                                    States</a>
                            </li>
                            <li>
                                <a href="{{ route('districts') }}" class=""> <i class="fa fa-list"></i>
                                    Districts</a>
                            </li>
                            <li>
                                <a href="{{ route('brand') }}" class=""> <i class="fa fa-list"></i>
                                    Brands</a>
                            </li>
                            {{-- <li>
                                <a href="{{ route('tahshil') }}" class=""> <i class="fa fa-list"></i>
                                    Tehsil</a>
                            </li> --}}


                        </ul>
                    </li>
                    <li>
                        <a><i class="fa fa-link"></i> Error Mapping Masters <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            {{-- <li>
                                <a href="{{ route('location_master') }}" class=""> <i class="fa fa-list"></i>
                                    Location</a>
                            </li> --}}
                            <li>
                                <a href="{{ route('statemaster') }}" class=""> <i class="fa fa-list"></i>
                                    States</a>
                            </li>
                            <li>
                                <a href="{{ route('districtmaster') }}" class=""> <i class="fa fa-list"></i>
                                    District</a>
                            </li>
                            <li>
                                <a href="{{ route('brandmaster') }}" class=""> <i class="fa fa-list"></i>
                                    Brand</a>
                            </li>



                        </ul>
                    </li>
                    <!-- <li>
                        <a><i class="fa fa-link"></i> Products <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">

                            <li>
                                <a href="{{ route('models') }}" class=""> <i class="fa fa-list"></i> Tractors</a>
                            </li>

                            <li>
                                <a href="{{ route('impelments') }}" class=""> <i class="fa fa-list"></i> Implements</a>
                            </li>
                        </ul>
                    </li> -->



                </ul>
            </div>


        </div>
        <!-- /sidebar menu -->

    </div>
</div>
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">

                        <li><a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
