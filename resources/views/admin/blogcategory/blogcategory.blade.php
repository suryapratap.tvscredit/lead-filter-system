@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <!-- /.card-header -->
                                        @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('success')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>
                                        @endif
                                        @if(Session::has('update'))
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('update')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        @if(Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('error')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        <div class="container-fluid ">
                                            <div class="row ">
                                                <div class="col-sm-12 ">

                                                    <button class="buttonplay" data-target="#addModal" data-toggle="modal"> <i class="fa fa-plus icon-white"><span></i> Add Blog Category </span> </button>
                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Blog Category</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1 ?>
                                                @foreach($Blogcategory as $cat)
                                                <tr class="gradeX">
                                                    <td>{{$sno++}}</td>
                                                    <td>{{$cat->blogcategory}}</td>
                                                    <td> <?php if ($cat->status == 'active') {  ?>
                                                            <label class="label label-success"> {{ucfirst($cat->status)}}</label> <?php } else { ?>
                                                            <label class="label label-warning"> {{ucfirst($cat->status)}}</label>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <div class="btn-group" style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                            <a href="javascript:void(0)" onclick="editDetails(<?= $cat->id ?>)"> <i class=" fa fa-edit" style="padding:5px"></i></a>
                                                            <a href="{{ route('BlogCategory.delete',$cat->id)}}"><i class="fa fa-trash" style="color: #b91010;padding:5px"></i></a>

                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</div>
</div>
</div>
</div>
<script>
    $('#datatable').dataTable();
</script>
<!--  ADD USER MODAL -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to submit the form?');" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Category</h4>
                </div>

                <div class="modal-body" id="form">
                    <div class="form-group">
                        <div class="controls">
                            <label for="title"> Category:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="blog Category" name="blogcategory" id="blogcategory" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="controls">
                            <label for="title">Status:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="status" id="status" required>
                                <option value=""> Select Status</option>
                                <option value="active"> Active</option>
                                <option value="inactive"> Inactive</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- MODAL END -->
    <!-- ADD SCRIPT -->
    <script>
        $(document).ready(function() {
            $("form[name='frmlead']").validate({
                rules: {
                    // type: {
                    //     required: true,
                    // },

                },
                messages: {
                    // emp_id: "Please enter Employe id",
                    // type: "Please enter User Type",
                    // process: "Please enter Process",
                },
                submitHandler: function(form) {
                    saveData();
                }
            });
        });


        function saveData() {
            //alert("asdasdad");
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            var fd = new FormData();
            fd.append('_token', CSRF_TOKEN);
            fd.append('blogcategory', $('#blogcategory').val());
            fd.append('status', $('#status').val());

            $.ajax({
                url: "{{ route('BlogCategory.create') }}",
                method: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {

                    if (response) {
                        new PNotify({
                            title: 'Data Succesfully inserted',
                            type: 'success',
                            styling: 'bootstrap3'
                        });

                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }
    </script>
</div>
<!-- END -->
<!-- Edit USER MODAL -->
<div id="edit-modal" name="edit_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="post" role="form" name="edit_modal" id="edit_modal" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to update the form?');" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>

                <div class="modal-body" id="form">
                    <input type="hidden" id="uid" name="uid">

                    <div class="form-group">
                        <div class="controls">
                            <label for="title">Blog Category:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="blog Category" name="ublogcategory" id="ublogcategory" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="controls">
                            <label for="title">Status:</label><span class="err" id="err_title"></span>
                            <select class="form-control select2bs4" name="ustatus" id="ustatus" required>
                                <option value=""> Select Status</option>
                                <option value="active"> Active</option>
                                <option value="inactive"> Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="esubmit" name="esubmit" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function editDetails(id) {
        var edit = "{{url('/admin/blogcategory/edit/')}}";
        $.get(edit + '/' + id, function(Blogcategory) {
            $('#uid').val(Blogcategory.id);
            $('#ublogcategory').val(Blogcategory.blogcategory);
            $('#ustatus').val(Blogcategory.status);
            $('#edit-modal').modal('toggle');
            // getSubprocess(categories.process_id, categories.sub_process_id, 'update');
        })
    }
</script>
<script>
    $(document).ready(function() {
        $("form[name='edit_modal']").validate({
            // Specify validation rules
            rules: {

                ucategory: {
                    required: true,
                },
            },
            // Specify validation error messages
            messages: {
                // uemp_id: "Please enter EMPLOYEE ID",
                // utype: "Please enter USER TYPE ",
                ucategory: "Please enter CATEGORIES",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                updateData();
            }
        });
    });

    function updateData() {
        var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

        var fd = new FormData();

        // Append data 
        fd.append('_token', CSRF_TOKEN);
        fd.append('id', $('#uid').val());
        fd.append('blogcategory', $('#ublogcategory').val());
        fd.append('status', $('#ustatus').val());


        // AJAX request 
        $.ajax({
            url: "{{route('BlogCategory.update')}}",
            method: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response) {

                if (response) {
                    new PNotify({
                        title: 'Succesfully Updated',
                        type: 'success',
                        styling: 'bootstrap3'
                    });

                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                    // $('#edit')[0].reset();
                }
            }
        });
    }
</script>
@endsection