@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css" />
        <link rel="stylesheet" type="text/css"
            href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3><b>Models</b></h3>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <a href="{{ route('models.add') }}">
                                                    <button class="buttonplay"> <i class="fa fa-plus icon-white"><span></i>
                                                        Add Model </span> </button></a>
                                            </div>
                                            <div class="col-sm-4">
                                                <button class="btn btn-success" data-target="#addModal" data-toggle="modal">
                                                    <i class="fa fa-plus icon-white"><span></i> Import Data </span>
                                                </button>
                                            </div>
                                            {{-- <!--  <div class="col-sm-4">
                                                            <a href="{{ asset('public/tractor.csv') }}" target="_BLANK"> <button class="btn btn-warning"> <i class="fa fa-download icon-white"><span></i> Download Upload Format </span> </button> </a>
                                    </div>
                                    --> --}}
                                        </div>

                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Brand</th>
                                                    <!-- <th>Serise</th> -->
                                                    <th>Model</th>
                                                    <th>Order</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1; ?>
                                                @foreach ($models as $cat)
                                                    <tr class="row1" data-id="{{ $cat->id }}"
                                                        data-brandid="{{ $cat->brand_id }}">
                                                        <td>{{ $sno++ }}</td>

                                                        <td>{!! $cat->brand_title !!}</td>
                                                        <!-- <td>{!! $cat->series_title !!}</td> -->
                                                        <td>{!! $cat->title !!}</td>
                                                        <td>

                                                            <?php $total = $brandTotal[$cat->id]; ?>
                                                            <select class="form-control select2bs4 order" name="status"
                                                                id="status" brand_id="{{ $cat->brand_id }}"
                                                                model_id="{{ $cat->id }}"
                                                                current_order="{{ $cat->order }}">

                                                                <?php for ($i = 1; $i <= $total; $i++) { ?>
                                                                <option value=" {{ $i }}"
                                                                    {{ $cat->order == $i ? 'selected' : '' }}>
                                                                    {{ $i }}
                                                                </option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <div class="btn-group"
                                                                style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                                <a href="{{ route('models.edit', $cat->id) }}"> <i
                                                                        class=" fa fa-edit" style="padding:5px"></i></a>
                                                                <a href="{{ route('models.delete', $cat->id) }}"><i
                                                                        class="fa fa-trash"
                                                                        style="color: #b91010;padding:5px"></i></a>
                                                                {{-- <a href="{{ route('gallery.images', $cat->id) }}"><i class="fa fa-picture-o" style="color: #b91010;padding:5px"></i></a> --}}


                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data"
                action="{{ route('models.import') }}" data-parsley-validate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add </h4>
                    </div>

                    <div class="modal-body" id="form">
                        @csrf

                        <div class="form-group">
                            <div class="controls">
                                <label for="title"> Select File</label><span class="err" id="err_title"></span>
                                <input type="file" class="form-control" class="text" placeholder="Serices Name"
                                    name="select_file" id="select_file" autocomplete="off" required>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script>
        $(document).on("change", ".order", function() {

            var brand_id = $(this).attr('brand_id');
            var model_id = $(this).attr('model_id');
            var current_order = $(this).attr('current_order');
            var neworder = $(this).val();
            //alert(brand_id + "?" + model_id);
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('updateOrder') }}",
                type: "POST",
                data: {
                    "brand_id": brand_id,
                    "model_id": model_id,
                    "current_order": current_order,
                    "neworder": neworder,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    if (response == "success") {
                        new PNotify({
                            title: "Order Update Successfully",
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                    }

                }
            });
        })

        $(function() {
            $('#datatable').dataTable({
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Models'
                }]
            });
            $("#datatable1").sortable({
                items: "tr",
                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    //alert($(this).attr('data-brandid'));
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {
                var order = [];
                var brand_id = [];
                var token = $('meta[name="csrf-token"]').attr('content');
                $('tr.row1').each(function(index, element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        brand_id: $(this).attr('data-brandid'),
                        position: index + 1,

                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('/admin/modal-sortable') }}",
                    data: {
                        order: order,
                        _token: token,
                        brand_id: brand_id
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });
            }
        });
    </script>
@endsection
