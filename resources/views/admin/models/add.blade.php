@extends('admin.include.master')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Add Model</h3>
                                        </div>
                                        <form method="post" role="form" action="{{ route('models.create') }}"
                                            name="frmlead" id="frmlead" enctype="multipart/form-data"
                                            onsubmit="return confirm('Do you really want to submit the form?');"
                                            data-parsley-validate>

                                            <div class="body" id="form">
                                                @csrf


                                                <div class="row">

                                                    <div class="form-group col-md-4">
                                                        <label for="title">Brand:</label><span class="err"
                                                            id="err_title"></span>
                                                        <select class="form-control select2bs4" name="brand_id"
                                                            id="brand_id" required>
                                                            <option value=""> Select brand</option>
                                                            <?php foreach ($brand as $b) { ?>
                                                            <option value="{{ $b->id }}">{{ $b->title }}</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">Year:</label><span class="err"
                                                            id="err_title"></span>
                                                        <select class="form-control select2bs4" name="year_id"
                                                            id="year_id">
                                                            <option value=""> Select Year</option>
                                                            <?php foreach ($year as $b) { ?>
                                                            <option value="{{ $b->id }}">{{ $b->year }}</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <!-- <div class="form-group col-md-4" id="series_Div">
                                                            <label for="title">Series:</label><span class="err" id="err_title"></span>
                                                            <select class="form-control select2bs4" name="series_id" id="series_id">
                                                                <option value='NA'> Select series</option>
                                                            </select>
                                                        </div> -->

                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Model name:</label><span class="err"
                                                                id="err_title"></span>
                                                            <input type="text" class="form-control" class="text"
                                                                placeholder="Enter Model name" name="title" id="title"
                                                                autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">Status:</label><span class="err"
                                                            id="err_title"></span>
                                                        <select class="form-control select2bs4" name="status"
                                                            id="status" required>
                                                            <option value=""> Select Status</option>
                                                            <option value="active"> Active</option>
                                                            <option value="inactive"> Inactive</option>
                                                        </select>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <div class="controls">
                                                            <label for="title"> Description:</label><span class="err"
                                                                id="err_title"></span>
                                                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="description"
                                                                id="description" autocomplete="off"></textarea>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="footer" align="center">
                                                    <button type="submit" id="submit" name="submit"
                                                        class="btn btn-primary">Submit</button>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            // $("#series_Div").hide();
            // $('#model_type').on('change', function() {
            //     //$('#allteam').hide(300);
            //     var model_type = $(this).val();
            //     if (model_type == "tractor") {
            //         $("#series_Div").show(300)
            //     } else {
            //         $("#series_Div").hide(300)
            //     }

            // });
            $('#brand_id').on('change', function() {
                //$('#allteam').hide(300);
                var brand_id = $(this).val();


                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('getbrandSerices') }}",
                    type: "POST",
                    data: {
                        "brand_id": brand_id,
                        "_token": CSRF_TOKEN
                    },
                    dataType: "json",
                    success: function(response) {
                        $('#series_id').html(response);

                    }
                });
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#description').summernote({
                height: 300,
            });

        });
    </script>
    <!--  ADD USER MODAL -->
@endsection
