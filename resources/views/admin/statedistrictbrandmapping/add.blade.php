@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Add State District Brand Mapping</h3>
                                        </div>
                                        @if (Session::has('success'))
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('success') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="clearfix"></div>
                                        @endif
                                        @if (Session::has('update'))
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('update') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if (Session::has('error'))
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>{{ Session::get('error') }}</strong>
                                                <button type="button" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        <div class="container-fluid ">

                                        </div>
                                        </br>
                                        <form method="post" role="form"
                                            action="{{ route('statedistrictbrandmapping.create') }}" name="frmlead"
                                            id="frmlead" enctype="multipart/form-data" data-parsley-validate>

                                            <div class="body" id="form">
                                                @csrf


                                                <div class="row">

                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Select State:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4 stateValue"
                                                                name="states">
                                                                <option value=""> Select State</option>
                                                                @foreach ($resultDataState as $cat)
                                                                    <option value="{{ $cat->id }}">
                                                                        {{ $cat->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title"> Select District:</label><span class="err"
                                                            id="err_title"></span>
                                                        <select class="form-control select2bs4 districtvalue"
                                                            name="districts[]" id="districts" multiple="multiple">
                                                            <option value=""> Select District</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Select Brand:</label><span class="err"
                                                                id="err_title"></span>
                                                            <select class="form-control select2bs4" name="brands[]"
                                                                multiple="multiple">
                                                                <option value=""> Select Brand</option>
                                                                @foreach ($brands as $b)
                                                                    <option value="{{ $b->brand_name }}">
                                                                        {{ $b->brand_name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="footer" align="center">
                                                <button type="submit" id="submit" name="submit"
                                                    class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).on("change", ".stateValue", function(e) {
            e.preventDefault();

            var state_id = $(this).val();
            //alert(state_id);
            var uid = $(this).attr("uid");

            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('getStateDistricts') }}",
                type: "POST",
                data: {
                    "state_id": state_id,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#district').html(response);

                }
            });
        });
    </script>
    <!--  ADD USER MODAL -->
@endsection
