@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Edit {{ $faq_page->title }} Page FAQ</h3>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>

                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <div class="row">
                                                <div class="form-group col-md-4">
                                                    <div class="controls">
                                                        <label for="title"> Question</label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Answer</label>

                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="title">Action</label>
                                                </div>
                                            </div>
                                            <div id="edit">
                                                @foreach ($faq as $cat)
                                                    <div class="row" id="row_{{ $cat->id }}">
                                                        <div class="form-group col-md-4">
                                                            <div class="controls">
                                                                <input type="text" class="form-control" class="text"
                                                                    placeholder="Enter Question" name="question[]"
                                                                    value="{{ $cat->question }}"
                                                                    id="question_{{ $cat->id }}" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <textarea type="text" class="form-control editanswers" class="text" placeholder="Description" name="answer[]"
                                                                id="description_{{ $cat->id }}" autocomplete="off" disabled>{{ $cat->answer }}</textarea>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <button type="button" id="editBtn_{{ $cat->id }}"
                                                                name="submit" class="btn btn-warning editBtn"
                                                                faq_id={{ $cat->id }}><i
                                                                    class="fa fa-edit"></i></button>
                                                            <button type="button" id="saveBtn_{{ $cat->id }}"
                                                                name="submit" class="btn btn-primary saveBtn "
                                                                faq_id={{ $cat->id }} style="display:none"><i
                                                                    class="fa fa-save"></i></button>
                                                            <button type="button" id="deleteBtn_{{ $cat->id }}"
                                                                name="submit" faq_id={{ $cat->id }}
                                                                class="btn btn-danger deleteBtn"><i
                                                                    class="fa fa-trash"></i></button>
                                                        </div>
                                                    </div>
                                                    <script>
                                                        $(document).ready(function() {
                                                            $('.editanswers').summernote({
                                                                toolbar: [
                                                                    ['style', ['style']],
                                                                    ['font', ['bold', 'underline', 'clear']],
                                                                    ['fontname', ['fontname']],
                                                                    ['color', ['color']],
                                                                    ['para', ['ul', 'ol', 'paragraph']],
                                                                    ['table', ['table']],
                                                                    ['insert', ['link', ]],
                                                                    ['view', ['fullscreen']],
                                                                ],
                                                            });

                                                            $("#description_{{ $cat->id }}").summernote("disable");

                                                        });
                                                    </script>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Add More {{ $faq_page->title }} Page FAQ</h3>
                                            <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>

                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="x_content">
                                            <form method="post" role="form" action="{{ route('faqCreate') }}"
                                                name="frmlead" id="frmlead" enctype="multipart/form-data"
                                                data-parsley-validate>

                                                <div class="body" id="form">
                                                    @csrf
                                                    <input type="hidden" value="{{ $faq_page->id }}" name="faq_page_id" />
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <div class="controls">
                                                                <label for="title"> Question</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Answer</label>

                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <label for="title">Action</label>
                                                        </div>
                                                    </div>

                                                    <div id="add">
                                                        <div class="row">
                                                            <div class="form-group col-md-4">
                                                                <div class="controls">
                                                                    <input type="text" class="form-control"
                                                                        class="text" placeholder="Enter Question"
                                                                        name="question[]" id="title" autocomplete="off"
                                                                        required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <textarea type="text" class="form-control answersArea" class="text" placeholder="Description" name="answer[]"
                                                                    id="description" autocomplete="off"></textarea>
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <button type="button" id="addMorebtn" name="submit"
                                                                    class="btn btn-success addMorebtn">+</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer" align="center">
                                                    <button type="submit" id="submit" name="submit"
                                                        class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {

            $(".editBtn").on("click", function() {
                var faq_id = $(this).attr("faq_id");
                $("#question_" + faq_id).prop("disabled", false);
                $("#saveBtn_" + faq_id).show();
                $("#editBtn_" + faq_id).hide();
                $("#description_" + faq_id).summernote("enable");
            });
            $(".saveBtn").on("click", function() {
                //alert($(this).attr("faq_id"))
                var faq_id = $(this).attr("faq_id");
                var qusValue = $("#question_" + faq_id).val();
                var ansValue = $("#description_" + faq_id).val();
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('faqUpdate') }}",
                    type: "POST",
                    data: {
                        "id": faq_id,
                        "qus": qusValue,
                        "ans": ansValue,
                        "_token": CSRF_TOKEN
                    },
                    dataType: "json",
                    success: function(response) {
                        //$('#district_id').html(response);
                        if (response = "success") {
                            $("#editBtn_" + faq_id).show();
                            $("#saveBtn_" + faq_id).hide();
                            new PNotify({
                                title: "Faq update Successfully",
                                type: 'success',
                                styling: 'bootstrap3'
                            });
                            $("#question_" + faq_id).prop("disabled", true);
                            $("#description_" + faq_id).summernote("disable");
                        }

                    }
                });

            });
            $(".deleteBtn").on("click", function() {
                //alert($(this).attr("faq_id"))
                var faq_id = $(this).attr("faq_id");

                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('faqDelete') }}",
                    type: "POST",
                    data: {
                        "id": faq_id,
                        "_token": CSRF_TOKEN
                    },
                    dataType: "json",
                    success: function(response) {
                        //$('#district_id').html(response);
                        if (response = "success") {
                            $("#row_" + faq_id).remove();

                            new PNotify({
                                title: "Faq Delete Successfully",
                                type: 'success',
                                styling: 'bootstrap3'
                            });

                        }

                    }
                });

            });

            $("#add").on("click", '.addMorebtn', function() {
                $("#add").append(
                    '<div class="row" id="row"><div class="form-group col-md-4"><div class="controls"><input type="text" class="form-control" class="text"  placeholder="Enter Question" name="question[]"id="title" autocomplete="off" required></div></div><div class="form-group col-md-6"><textarea type="text" class="form-control answerText" class="text" placeholder="Description" name="answer[]" id="" autocomplete="off"></textarea></div><div class="form-group col-md-2"><button type="button" id="DeleteRow"  name="submit" class="btn btn-danger removbtn">-</button><button type="button" id="addMorebtn" name="submit" class="btn btn-success addMorebtn">+</button> </div></div>'
                )
                $('.answerText').summernote({
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', ]],
                        ['view', ['fullscreen']],
                    ],
                });
            });
            $("body").on("click", "#DeleteRow", function() {
                $(this).parents("#row").remove();
            })
        });
        $(document).ready(function() {
            $('.answersArea').summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', ]],
                    ['view', ['fullscreen']],
                ],
            });



        });
    </script>
@endsection
