@extends('admin.include.master')

@section('content')
    <div class="content-wrapper">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Content Header (Page header) -->
        <section class="content-header">
        </section>

        <body class="nav-md">
            <div class="container body">
                <div class="main_container">
                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="">
                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h3>Add {{ $faq_page->title }} Page FAQ</h3>
                                        </div>
                                        <form method="post" role="form" action="{{ route('faqCreate') }}" name="frmlead"
                                            id="frmlead" enctype="multipart/form-data" data-parsley-validate>

                                            <div class="body" id="form">
                                                @csrf
                                                <input type="hidden" value="{{ $faq_page->id }}" name="faq_page_id" />
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Question</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Answer</label>

                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <label for="title">Action</label>
                                                    </div>
                                                </div>
                                                <div id="add">
                                                    <div class="row">
                                                        <div class="form-group col-md-4">
                                                            <div class="controls">
                                                                <input type="text" class="form-control" class="text"
                                                                    placeholder="Enter Question" name="question[]"
                                                                    id="title" autocomplete="off" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="answer[]" id="description"
                                                                autocomplete="off"></textarea>
                                                        </div>
                                                        <div class="form-group col-md-2">
                                                            <button type="button" id="addMorebtn" name="submit"
                                                                class="btn btn-primary">+</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer" align="center">
                                                <button type="submit" id="submit" name="submit"
                                                    class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </body>

    </div>
    </div>
    </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#addMorebtn").on("click", function() {
                $("#add").append(
                    '<div class="row" id="row"><div class="form-group col-md-4"><div class="controls"><input type="text" class="form-control" class="text"  placeholder="Enter Question" name="question[]"id="title" autocomplete="off" required></div></div><div class="form-group col-md-6"><textarea type="text" class="form-control answerText" class="text" placeholder="Description" name="answer[]" id="" autocomplete="off"></textarea></div><div class="form-group col-md-2"><button type="button" id="DeleteRow"  name="submit" class="btn btn-danger removbtn">-</button></div></div>'
                )
                $('.answerText').summernote({
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', ]],
                        ['view', ['fullscreen']],
                    ],
                });
            });
            $("body").on("click", "#DeleteRow", function() {
                $(this).parents("#row").remove();
            })
        });
        $(document).ready(function() {
            $('#description').summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', ]],
                    ['view', ['fullscreen']],
                ],
            });



        });
    </script>
    <!--  ADD USER MODAL -->
@endsection
