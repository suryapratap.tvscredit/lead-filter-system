@extends('admin.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <!-- /.card-header -->
                                        @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('success')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>
                                        @endif
                                        @if(Session::has('update'))
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('update')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        @if(Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('error')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        <div class="container-fluid ">

                                        </div>
                                        </br>
                                        <form method="post" role="form" action="{{route('category.update')}}" name="frmlead" id="frmlead" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to submit the form?');" data-parsley-validate>

                                            <div class="body" id="form">
                                                @csrf
                                                <h3 align="center"><b>Section 1</b></h3>
                                                <input type="hidden" value="{{$category->id}}" name="id" id="id">
                                                <div class="row">

                                                    <div class="form-group col-md-4">
                                                        <label>Banner Image</label>
                                                        <input class="form-control " type="file" name="bannerimage" id="bannerimage" placeholder="File">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">Banner Title:</label><span class="err" id="err_title"></span>
                                                        <input type="text" value="{{$category->banner_titile}}" class="form-control" class="text" placeholder="title" name="banner_titile" id="banner_titile" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title"> Speciality:</label><span class="err" id="err_title"></span>
                                                            <input type="text" class="form-control" class="text" placeholder="Speciality" value="{{$category->category}}" name="category" id="category" autocomplete="off" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <div class="controls">
                                                            <label for="title">Status:</label><span class="err" id="err_title"></span>
                                                            <select class="form-control select2bs4" name="status" id="status" required>
                                                                <option value=""> Select Status</option>
                                                                <option value="active" {{($category->status=="active") ? 'selected': ''}}> Active</option>
                                                                <option value="inactive" {{($category->status=="inactive") ? 'selected': ''}}> Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label for="title">Type:</label><span class="err" id="err_title"></span>
                                                        <select class="form-control select2bs4" name="type" id="type" required>
                                                            <option value=""> Select Type</option>
                                                            <option value="centreofex" {{($category->type=="centreofex") ? 'selected': ''}}> Centres of Excellence
                                                                (Super Specialities)</option>
                                                            <option value="multispec" {{($category->type=="multispec") ? 'selected': ''}}> Multi Specialities</option>
                                                            <option value="suppser" {{($category->type=="suppser") ? 'selected': ''}}> Support Services</option>
                                                        </select>
                                                    </div>



                                                    <div class="form-group col-md-4">
                                                        <label>Menu-Icon</label>
                                                        <input class="form-control " type="file" name="menuicon" id="menuicon" placeholder="File">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label>Category-Icon</label>
                                                        <input class="form-control " type="file" name="caticon" id="caticon" placeholder="File">
                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <div class="controls">
                                                            <label for="title"> Speciality Description:</label><span class="err" id="err_title"></span>
                                                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="description" id="description" autocomplete="off" required>{{$category->description}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <div class="controls">
                                                            <label for="title"> Description:</label><span class="err" id="err_title"></span>
                                                            <textarea type="text" class="form-control" class="text" placeholder="Description" name="description2" id="description2" autocomplete="off" required>{{$category->description2}}</textarea>
                                                        </div>
                                                    </div>
                                                    <h3 align="center"><b>Section 2</b></h3>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">Title:</label><span class="err" id="err_title"></span>
                                                        <input type="text" value="{{$category->title}}" class="form-control" class="text" placeholder="title" name="title" id="title" autocomplete="off">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">Sub-Title:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="Sub-Title" name="sub_title" id="sub_title" value="{{$category->sub_title}}" autocomplete="off">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">Content:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" class="text" placeholder="content" name="section2_content" id="section2_content" value="{{$category->section2_content}}">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="title">List Content:</label><span class="err" id="err_title"></span>
                                                        <input type="text" class="form-control" value="{{$category->content}}" class="text" placeholder="content" name="content" id="content" autocomplete="off">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label>Image</label>
                                                        <input class="form-control " type="file" name="image" id="image" placeholder="File">
                                                    </div>
                                                </div>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="footer" align="center">
                                                    <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</div>

<!-- <script>
    $(document).ready(function() {
        $('#description').summernote({
            height: 300,
        });
        $('#description2').summernote({
            height: 300,
        });
    });
</script> -->
<!--  ADD USER MODAL -->



@endsection