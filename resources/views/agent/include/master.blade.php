<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Cogent E-services | CogentKB </title>

  <!-- Bootstrap -->
  <link href="{{asset('public/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{asset('public/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{asset('public/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
  <!-- Animate.css -->
  <link href="{{asset('public/vendors/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('public/css/select2.min.css')}}" rel="stylesheet" />

  <!-- Custom Theme Style -->
  <link href="{{asset('public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('public/css/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/buttons.dataTables.min.css')}}">
  <link href="{{asset('public/build/css/custom.css')}}" rel="stylesheet">
  <script src="{{asset('public/vendors/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap -->
  <script src="{{asset('public/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>


  <!-- Datatables -->
  <script src="{{asset('public/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/js/dataTables.buttons.min.js')}}"></script>
  <script src="{{asset('public/js/jszip.min.js')}}"></script>
  <script src="{{asset('public/js/buttons.html5.min.js')}}"></script>
  <!-- NProgress -->
  <script src="{{asset('public/vendors/nprogress/nprogress.js')}}"></script>

  <script src="{{asset('public/js/jquery.validate.min.js')}}"></script>

  <!-- pNotify -->

  <link href="{{asset('public/vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
  <link href="{{asset('public/vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
  <link href="{{asset('public/vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">

  <style>
    .error {
      color: red;
      font-size: 10px;
    }

    .buttonplay {
      white-space: nowrap;
      display: inline-block;
      border-radius: 4px;
      background-color: #19adc3;
      border-color: #5a738e;
      color: #FFFFFF;
      text-align: center;
      font-size: 12px;
      padding: 7px;
      /* width: 100px; */
      transition: all ease-in 0.1s;
      cursor: pointer;
      margin: 5px;

    }

    .buttonplay span {
      cursor: pointer;
      display: inline-block;
      position: relative;
      transition: 0.5s;
    }

    .buttonplay span:after {
      content: '';
      position: absolute;
      opacity: 0;
      top: 0;
      right: -20px;
      transition: 0.5s;
    }

    .buttonplay:hover span {
      padding-right: 15px;
    }

    .buttonplay:hover span:after {
      opacity: 1;
      right: 0;
    }


    #data-container {
      display: block;
      background: #2a3f54;
      float: right;
      max-height: 250px;
      overflow-y: auto;
      z-index: 9999999;
      position: absolute;
      width: 100%;

    }

    #data-container li {
      list-style: none;
      padding: 5px;
      border-bottom: 1px solid #fff;
      color: #fff;
    }

    #data-container li:hover {
      background: #26b99a;
      cursor: pointer;
    }
  </style>
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      @include('Agent.include.sidebar')
      <!-- sidebar-end -->


      <!-- page-content-start -->
      @yield('content')
      <!-- page-content-end -->


      <!-- footer -->
      @include('admin.include.footer')
      <!-- /.content-wrapper -->