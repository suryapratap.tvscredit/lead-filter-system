<div class="col-md-3 left_col">
    <div class=" left_col scroll-view ">

        <div class="profile clearfix">
            <div class="" align="center">
                <h6><img src="{{asset('public/vendors/images/logo.png')}}" alt="Logo" height="80px" width="160px" /></h6>
            </div></br>

            <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->

        <br />
        <hr>
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">

                <ul class="nav side-menu">


                    <li>
                        <a href="#" class=""> <i class="fa fa-clipboard"></i>Sub-Category</a>
                    </li>

                </ul>
            </div>


        </div>
        <!-- /sidebar menu -->

    </div>
</div>
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">

                        <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>