<!-- footer content -->
<footer>
  <div align="center">
    <p>Cogent E Services Pvt Ltd &copy; <?php echo date("Y"); ?> - <?php echo date("Y") + 1; ?> All Rights Reserved.</p>
  </div>
  <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>



<!-- NProgress -->
<script src="{{asset('public/vendors/nprogress/nprogress.js')}}"></script>

<!-- Custom Theme Scripts -->
<!-- <script src="{{asset('public/build/js/custom.min.js')}}"></script> -->
<script src="{{asset('public/build/js/custom.js')}}"></script>
<script src="{{asset('public/vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('public/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('public/js/select2.min.js')}}"></script>
<script src="{{asset('public/js/summernote.js')}}"></script>
<!-- pNotify -->
<script src="{{asset('public/vendors/pnotify/dist/pnotify.js')}}"></script>
<script src="{{asset('public/vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
<script src="{{asset('public/vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>



<script>
  $(document).ready(function() {
    //$('.ui-pnotify ').remove();
  });

  // new PNotify({
  //                                 title: 'Regular Success',
  //                                 text: 'That thing that you were trying to do worked!',
  //                                 type: 'success',
  //                                 styling: 'bootstrap3'
  //                             });
</script>
@if ($message = Session::get('success'))
<script>
  new PNotify({
    title: "{{ $message }}",
    type: 'success',
    styling: 'bootstrap3'
  });
</script>
@endif


@if ($message = Session::get('error'))
<script>
  new PNotify({
    title: "{{ $message }}",
    type: 'error',
    styling: 'bootstrap3'
  });
</script>
@endif


</body>