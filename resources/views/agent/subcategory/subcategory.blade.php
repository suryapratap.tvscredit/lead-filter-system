@extends('agent.include.master')

@section('content')
<div class="content-wrapper">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <!-- /.card-header -->
                                        <!-- @if(Session::has('success'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('success')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>
                                        @endif
                                        @if(Session::has('update'))
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('update')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        @if(Session::has('error'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <strong>{{Session::get('error')}}</strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif -->
                                        <div class="container-fluid ">
                                            <div class="row ">
                                            </div>
                                        </div>
                                        </br>
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Category</th>
                                                    <th>subcategory</th>
                                                    <!-- <th>Action</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php $sno = 1 ?>
                                                @foreach($subcategory as $cat)
                                                <tr class="gradeX">
                                                    <td>{{$sno++}}</td>
                                                    <td>{{$cat->category}}</td>
                                                    <td>{{$cat->subcategory}}</td>
                                                    <!-- <td>
                                                        <div class="btn-group" style="padding:10px;align-items: center;justify-content: center;margin-left: 25%;">
                                                            <a href="javascript:void(0)" onclick="editDetails(<?= $cat->id ?>)"> <i class=" fa fa-edit" style="padding:5px"></i></a>
                                                            <a href="{{ route('category.delete',$cat->id)}}"><i class="fa fa-trash" style="color: #b91010;padding:5px"></i></a>
                                                        </div>
                                                    </td> -->
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</div>
</div>
</div>
</div>
<script>
    // $('#datatable').dataTable({
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'excelHtml5',
    //     ],
    //     scrollX: 300,
    //     responsive: true,
    // })
    $('#datatable').dataTable({
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
        ],
        // scrollX: 300,
        responsive: true,

    });
</script>
<!--  ADD USER MODAL -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="post" role="form" name="frmlead" id="frmlead" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to submit the form?');" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Category</h4>
                </div>

                <div class="modal-body" id="form">
                    <div class="form-group">
                        <label>Category:</label>
                        <select class="form-control select2bs4" name="category" id="category" required>
                            <option value=""> Select Category</option>
                            @foreach($category as $cat)
                            <option value="{{$cat->id}}"> {{$cat->category}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="modal-body" id="form">
                    <div class="form-group">
                        <div class="controls">
                            <label for="title"> Sub-Category:</label><span class="err" id="err_title"></span>
                            <input type="text" class="form-control" class="text" placeholder="Sub-Category" name="subcategory" id="subcategory" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- MODAL END -->
    <!-- ADD SCRIPT -->
    <script>
        $(document).ready(function() {
            $("form[name='frmlead']").validate({
                rules: {
                    // type: {
                    //     required: true,
                    // },

                },
                messages: {
                    // emp_id: "Please enter Employe id",
                    // type: "Please enter User Type",
                    // process: "Please enter Process",
                },
                submitHandler: function(form) {
                    saveData();
                }
            });
        });


        function saveData() {
            //alert("asdasdad");
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            var fd = new FormData();
            fd.append('_token', CSRF_TOKEN);
            fd.append('category', $('#category').val());
            fd.append('subcategory', $('#subcategory').val());

            $.ajax({
                url: "{{ route('subcategory.create') }}",
                method: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(response) {

                    if (response) {
                        new PNotify({
                            title: 'Data Succesfully inserted',
                            type: 'success',
                            styling: 'bootstrap3'
                        });

                        setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }
    </script>
</div>
<!-- END -->
<!-- Edit USER MODAL -->
<div id="edit-modal" name="edit_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <form method="post" role="form" name="edit_modal" id="edit_modal" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to update the form?');" data-parsley-validate>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>

                <input type="hidden" id="uid" name="uid">
                <div class="modal-body" id="form">
                    <div class="form-group">
                        <label>Category:</label>
                        <select class="form-control select2bs4" name="ucategory" id="ucategory" required>
                            <option value=""> Select Category</option>
                            @foreach($category as $cat)
                            <option value="{{$cat->id}}"> {{$cat->category}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <div class="form-group ">
                            <div class="controls">
                                <label for="title">Sub-Category:</label><span class="err" id="err_title"></span>
                                <input class="form-control select2bs4" name="usubcategory" id="usubcategory" required>

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" id="esubmit" name="esubmit" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function editDetails(id) {
        var edit = "{{url('/subcategory/edit/')}}";
        $.get(edit + '/' + id, function(subcategory) {
            $('#uid').val(subcategory.id);
            $('#usubcategory').val(subcategory.subcategory);
            $('#ucategory').val(subcategory.category_id);
            $('#edit-modal').modal('toggle');
            // getSubprocess(categories.process_id, categories.sub_process_id, 'update');
        })
    }
</script>

<script>
    $(document).ready(function() {
        $("form[name='edit_modal']").validate({
            // Specify validation rules
            rules: {

                ucategory: {
                    required: true,
                },
            },
            // Specify validation error messages
            messages: {
                // uemp_id: "Please enter EMPLOYEE ID",
                // utype: "Please enter USER TYPE ",
                ucategory: "Please enter CATEGORIES",
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function(form) {
                updateData();
            }
        });
    });

    function updateData() {
        var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");

        var fd = new FormData();

        // Append data 
        fd.append('_token', CSRF_TOKEN);
        fd.append('id', $('#uid').val());
        fd.append('subcategory', $('#usubcategory').val());
        fd.append('category', $('#ucategory').val());


        // AJAX request 
        $.ajax({
            url: "{{route('subcategory.update')}}",
            method: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(response) {

                if (response) {
                    new PNotify({
                        title: 'Data Succesfully inserted',
                        type: 'success',
                        styling: 'bootstrap3'
                    });

                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                    // $('#edit')[0].reset();
                }
            }
        });
    }
</script>
@endsection