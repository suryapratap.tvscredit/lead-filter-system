@extend('fronts.header')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dashboard</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <h3> Welcome <span><strong>
                                    </strong>
                                </span>
                            </h3>
                            <div class="row top_tiles">
                                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                                        <div class="count">179</div>
                                        <h3>New Sign ups</h3>
                                        <p>Lorem ipsum psdea itgum rixt.</p>
                                    </div>
                                </div>
                                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="icon"><i class="fa fa-comments-o"></i></div>
                                        <div class="count">179</div>
                                        <h3>New Sign ups</h3>
                                        <p>Lorem ipsum psdea itgum rixt.</p>
                                    </div>
                                </div>
                                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                                        <div class="count">179</div>
                                        <h3>New Sign ups</h3>
                                        <p>Lorem ipsum psdea itgum rixt.</p>
                                    </div>
                                </div>
                                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="tile-stats">
                                        <div class="icon"><i class="fa fa-check-square-o"></i></div>
                                        <div class="count">179</div>
                                        <h3>New Sign ups</h3>
                                        <p>Lorem ipsum psdea itgum rixt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
