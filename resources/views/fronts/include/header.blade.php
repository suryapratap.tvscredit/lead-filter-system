<html class="no-js" lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="csrf-token" content="WkRGVsbZW14PEbPNaxLgKhZ1chNMnL8gOs7B3rF7">
    <meta name="robots" content="index,follow" />
    <meta name="author" content="Tractor Karvan">
    <link rel="preconnect" href="https://www.googletagmanager.com">
    <link rel="dns-prefetch" href="https://www.googletagmanager.com">
    <title> Lead Filter </title>
    <meta name="description" content=" ">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5 ,user-scalable=no">
    <meta property="og:type" content="website">
    <meta property="og:title" content=" ">
    <meta property="og:description" content=" ">
    <meta property="og:site_name" content="TractorKarvan">
    <meta property="og:image" content="https://tractorpaisa.com/assets/tractorpaisa_Logo1200x1200.png">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@Username">
    <meta name="twitter:title" content=" ">
    <meta name="twitter:image" content="https://tractorpaisa.com/assets/tractorpaisa_Logo1200x1200.png">
    <meta name="twitter:description" content="">

    <link rel="canonical" href="https://tractorpaisa.com">

    <link href="{{ asset('public/front/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/front/css/main.css') }}">
    <script src="{{ asset('public/front/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/front/js/bootstrap.min.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--
<link rel="stylesheet" href="{{ asset('public/front/css/slick-theme.css') }}">

    <script src="{{ asset('public/front/js/bootstrap.min.js') }}"></script> -->
</head>

<body>
    <!-- nav bar start-->

    <div class="nb-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-8 col-8">
                    <nav class="navbar navbar-expand-lg navbar-dark custome-nav menu"
                        aria-label="Offcanvas navbar large">

                        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasNavbar2" aria-controls="offcanvasNavbar2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="#000"
                                class="bi bi-list" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                            </svg>
                        </button>
                        <a class="navbar-brand" href="#">TractorPaisa</a>


                        <div class="offcanvas offcanvas-start text-bg-light" tabindex="-1" id="offcanvasNavbar2"
                            aria-labelledby="offcanvasNavbar2Label">
                            <div class="offcanvas-header">
                                <h5 class="offcanvas-title" id="offcanvasNavbar2Label">TractorPaisa</h5>
                                <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas"
                                    aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                                <ul class="navbar-nav justify-content-start flex-grow-1">
                                    <li class="nav-item">
                                        <a class="nav-link active" aria-current="page"
                                            href="{{ route('home') }}">Home</a>
                                    </li>

                                    <li class="nav-item"><a class="nav-link" href="{{ route('evaluation') }}">Online
                                            Evaluation</a></li>
                                    <li class="nav-item"><a class="nav-link"
                                            href="{{ route('freedoorstepevaluation') }}">Doorstep Evaluation</a></li>
                                    <li class="nav-item"><a class="nav-link"
                                            href="{{ route('personalloanfrount') }}">Personal loan</a></li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ URL('about-us') }}">About Us</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('contactUs') }}">Contact us</a>
                                    </li>
                                    {{-- <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Dropdown
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#">Action</a></li>
                                            <li><a class="dropdown-item" href="#">Another action</a></li>
                                            <li>
                                                <hr class="dropdown-divider">
                                            </li>
                                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                                        </ul>
                                    </li> --}}
                                </ul>
                            </div>

                        </div>

                    </nav>
                    <!-- nav End bar start-->
                </div>



                <div class="col-lg-2 col-md-4 col-4 pt-3 search-icons text-end">
                    <!-- <input type="text" placeholder="Search" class="search-input">
     <i class="search-icn bi bi-search"></i>
<i class="bi bi-bell"></i>  -->
                </div>
            </div>
        </div>
    </div>
    </div>
