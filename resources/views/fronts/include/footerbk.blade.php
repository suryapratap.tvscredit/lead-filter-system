<div class="footer">
    <div class="container pt-4 pb-3">
        <h2>Tractor Paisa</h2>
        <div class="row">
            <div class="col-6 col-md-4 col-xs-4 g-3">
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('aboutUs') }}">About us</a></li>
                    <li><a href="{{ route('termsAndConditions') }}">Terms & conditions</a></li>
                    <li><a href="{{ route('contactUs') }}">Contact us</a></li>
                </ul>
            </div>
            <div class="col-6 col-md-4 col-xs-4 g-3">
                <ul>
                    <li><a href="{{ route('evaluation') }}">Online Evaluation</a></li>
                    <li><a href="{{ route('freedoorstepevaluation') }}">Doorstep Evaluation</a></li>
                    <li><a href="#">Personal loan</a></li>
                    {{-- <li><a href="#">FAQs</a></li> --}}
                </ul>
            </div>
            <div class="copyright">® {{ date('Y') }} www.tractorpaisa.com All rights reserved </div>
        </div>

    </div>
    <!-- footer-->

    <!--  chat box-->
    <button class="open-button shadow-lg" onclick="openForm()"> <i class="bi bi-chat-left"></i> </button>

    <div class="chat-popup" id="myForm">
        <form action="/action_page.php" class="chat-container shadow">
            <h4>Live Chat</h4>

            <label for="msg">Message</label>
            <textarea placeholder="Type message.." name="msg" required></textarea>

            <button type="submit" class="btn mb-2">Send</button>
            <button type="button" class="close-support" onclick="closeForm()">x</button>
        </form>
    </div>
    <!--  chat box-->


    <script type="text/javascript">
        // $(".search-icn").click(function() {
        //     $(".search-input").toggleClass("active").focus;
        //     $(this).toggleClass("animate");
        //     $(".search-input").val("");
        // });

        function openForm() {
            document.getElementById("myForm").style.display = "block";
        }

        function closeForm() {
            document.getElementById("myForm").style.display = "none";
        }
    </script>
    <script src="{{ asset('public/front/js/purecookie.js') }}"></script>
    </body>

    </html>
