@extends('fronts.include.master')

@section('content')
    {{-- <div class="top-banner pt-4 p-3 pb-4">
        <div class="container">
            <h3>Get A Loan</h3>
            <p>Fulfill all your multiple needs with a single choice</p>
            <div class="get-loan-btn pt-4">
                <a href="" class="mt-2">Apply Now</a>
            </div>
        </div>
    </div> --}}

    <div class="contact-bg">
        <div class="container pt-4 emi-page-outer static-page">
            <div class="step-app emi-page mb-3" id="">
                <h1>Get in Touch</h1>

                <div class="getintouch p-3 bg-white">
                    <h3>Have a query? Ask our team</h3>
                    <form enctype="multipart/form-data" method="POSt" id="myform" action="{{ route('contactusSubmit') }}">
                        @csrf
                        <div class="form-group pt-3">
                            <select class="form-control" id="query_type" name="query_type">
                                <option>Type of Query</option>
                                <option value="query1">query1</option>
                                <option value="query2">query2</option>
                            </select>
                        </div>

                        <div class="form-group pt-3">
                            <label for="comment">Message</label>
                            <textarea class="form-control" rows="3" id="comment" name="message"></textarea>
                        </div>

                        <div class="form-group pt-3">
                            <label>Enter Your Name</label>
                            <input type="text" max=" " class="form-control" id="" aria-describedby=""
                                placeholder="Enter your Name" required name="name">
                        </div>

                        <div class="form-group pt-3">
                            <label>Enter Mobile No.</label>
                            <input type="number" max=" " class="form-control" id="" aria-describedby=""
                                placeholder="Enter Mobile No." required name="mobile">
                        </div>

                        <div class="form-group pt-3">
                            <label>Enter Your Email</label>
                            <input type="email" max=" " class="form-control" id="" aria-describedby=""
                                placeholder="Enter Your Email" required name="email">
                        </div>

                        <button type="submit" id="ContactSubmit" class="btn checkbtn knowmore-btn mt-4 p-2">Submit</button>
                    </form>

                </div>


                <div class="getintouch p-3 mt-5 mb-5 pt-4 bg-white">
                    <h3>Reach us at</h3>
                    <h4>Contact / Mail </h4>
                    <p>+91 9876 789 657<Br />
                        <a href="mailto:tractorpaisa@gmail.com">tractorpaisa@gmail.com</a><Br />
                        <a href="mailto:tractorpaisa@gmail.com">reachus@tractorpaisa.com</a>
                    </p>

                    <h4>Office </h4>
                    <p>Lorem ipsum dolor sit amet consectetur. Neque urna tortor amet viverra habitasse nibh.
                        Tempor pretium feugiat tempus 401009</p>

                    <div class="mt-2">

                        <iframe class="rounded-3"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7007.472656977334!2d77.31052312543713!3d28.57767937975777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce45b7e24b733%3A0xb8dd905f45fbfcd5!2sSector%2016%2C%20Noida%2C%20Uttar%20Pradesh%20201301!5e0!3m2!1sen!2sin!4v1670926724874!5m2!1sen!2sin"
                            width="100%" height="180" style="border:0;" allowfullscreen="" loading="lazy"
                            referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>

                </div>

            </div>


            <div class="pb-4">
                <h3>Comfortable Way to Fulfill your Dreams</h3>
                Lorem ipsum dolor sit amet consectetur. Neque urna tortor amet viverra habitasse nibh. Tempor pretium
                feugiat tempus a id habitant ultricies id malesuada. Blandit condimentum ac malesuada pharetra. Turpis proin
                id et a sodales arcu massa consequat molestie. Turpis fringilla nibh ante morbi enim eget vestibulum. Sociis
                sagittis suscipit ac suspendisse. Sit lectus cursus aenean ut.
                Massa in tellus quam tristique orci habitant. Viverra mollis pellentesque mauris sit facilisis. Tortor
                consequat pellentesque velit .


            </div>
        </div>
    </div>




    <!-- faq-->
    <div class="questions">
        <div class="container pt-4 pb-3">
            <h2 class="mb-3">Have a question? Here to help.</h2>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <div class="accordion-item mb-2">
                    <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed rounded-sm" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Lorem ipsum dolor sit amet consectetur?
                        </button>
                    </h2>
                    <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body pt-0">Lorem ipsum dolor sit amet consectetur. Tincidunt elementum erat
                            diam venenatis in tincidunt risus odio. Porttitor sed pulvinar accumsan pharetra dui quis quam
                            neque consequat. </div>
                    </div>
                </div>
                <div class="accordion-item mb-2">
                    <h2 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed " type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Lorem ipsum dolor sit amet consectetur?
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo"
                        data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body pt-0">Lorem ipsum dolor sit amet consectetur. Tincidunt elementum erat
                            diam venenatis in tincidunt risus odio. Porttitor sed pulvinar accumsan pharetra dui quis quam
                            neque consequat. </div>
                    </div>
                </div>
                <div class="accordion-item mb-2">
                    <h2 class="accordion-header" id="flush-headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-collapseThree" aria-expanded="false"
                            aria-controls="flush-collapseThree">
                            Lorem ipsum dolor sit amet consectetur?
                        </button>
                    </h2>
                    <div id="flush-collapseThree" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body pt-0">Lorem ipsum dolor sit amet consectetur. Tincidunt elementum erat
                            diam venenatis in tincidunt risus odio. Porttitor sed pulvinar accumsan pharetra dui quis quam
                            neque consequat.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end faq-->
@endsection
