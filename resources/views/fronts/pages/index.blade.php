@extends('fronts.include.master')

@section('content')
    {{-- <div class="top-banner pt-4 p-3 pb-4">
        <div class="container">
            <h3>Get A Loan</h3>
            <p>Fulfill all your multiple needs with a single choice</p>
            <div class="get-loan-btn pt-4">
                <a href="" class="mt-2">Apply Now</a>
            </div>
        </div>
    </div> --}}

    <div class="container pt-4 emi-page-outer">
        <div class="step-app emi-page mb-3" id="">
            <h1>{{ $page->title }}</h1>
            <div class="static-page-content">
                {!! $page->description !!}
            </div>

        </div>




    </div>



    <!-- faq-->
    <div class="questions">
        <div class="container pt-4 pb-3">
            <h2 class="mb-3">Have a question? Here to help.</h2>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <?php $show = 0; ?>
                @foreach ($faqs as $cat)
                    <?php
                    if ($show == 0) {
                        $show = 'show';
                    } else {
                        $show = '';
                    }
                    ?>
                    <div class="accordion-item mb-2">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed rounded-sm" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapse{{ $cat->id }}" aria-expanded="false"
                                aria-controls="flush-collapseOne">
                                {!! $cat->question !!}
                            </button>
                        </h2>
                        <div id="flush-collapse{{ $cat->id }}" class="accordion-collapse collapse <?php echo $show; ?>"
                            aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body pt-0">{!! $cat->answer !!} </div>
                        </div>
                    </div>
                    <?php $show = 1; ?>
                @endforeach

            </div>
        </div>
    </div>
    <!-- end faq-->
@endsection
