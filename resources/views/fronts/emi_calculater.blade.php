@extends('fronts.include.master')

@section('content')

<div class="container pt-4 emi-page-outer">
  <div class="step-app emi-page mb-3" id="">
    <h1>Tractor Loan EMI Calculator</h1>
    <p class="emi-para pt-2">It is very easy to calculate the EMI for your tractor loan. You will get EMI as soon as you
      enter the
      required loan amount and the interest rate. Installment in EMI calculator is calculated on reducing balance.
      As per the rules of financing institutions, processing fee or possible charges may be applicable which are not
      shown in the EMI we calculate.</p>
    <div class="row pt-0 pb-4 emi-para">
      <div class="rounded-4">

        <div class="row">
          <div class="col-lg-8 col-md-4 col-12">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-12">
                <!-- row 1-->
                <div class="row emi-row mb-4">
                  <div class="col-lg-12 col-md-4 col-6">
                    <p>Loan Amount</p>
                  </div>
                  <div class="col-lg-12 col-md-4 col-6">
                    <form>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text border-end-0 shadow-sm" id="LoanAmount"> ₹ </span>
                        </div>
                        <input id="emi-amount" type="number" class="form-control border-start-0 shadow-sm" value="300000" min="0" max="200" onfocusout="onInputChange('emi-amount','amount')" />
                        <!-- <input type="number" class="form-control border-start-0 shadow-sm" id="LoanAmount"
                            placeholder="1,00,000" aria-describedby=""> -->
                      </div>
                  </div>
                  <div class="col-sm-12 range-select mt-3">
                    <input id="amount" type="range" min="50000" max="1000000" step="1000" value="300000" />
                  </div>
                  </form>
                </div>
              </div>
              <!-- row 1-->





              <div class="col-lg-4 col-md-4 col-12">
                <!-- row 3-->
                <div class="row emi-row mb-4">
                  <div class="col-lg-12 col-md-4 col-6">
                    <p>Rate of Interest (p.a)</p>
                  </div>

                  <div class="col-lg-12 col-md-4 col-6">
                    <form>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text border-end-0 shadow-sm" id="LoanAmount"> % </span>
                        </div>
                        <!-- <input id="amount" type="number" class="form-control border-start-0 shadow-sm" value="7.82" min="0.2" max="9.2" oninput="rangeInput.value=amount.value" /> -->
                        <input id="emi-rate" type="text" class="form-control  border-start-0 shadow-sm" value="12" onfocusout="onInputChange('emi-rate','rate')">
                      </div>
                  </div>
                  <div class="col-sm-12 range-select mt-3">
                    <input type="range" id="rate" min="12" max="30" value="12">
                  </div>
                  </form>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-12">
                <!-- row 2-->
                <div class="row emi-row mb-4">
                  <div class="col-lg-12 col-md-4 col-6">
                    <p>Loan Tenure (Years)</p>
                  </div>

                  <div class="col-lg-12 col-md-4 col-6">
                    <form>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text border-end-0 shadow-sm" id="LoanAmount"> &nbsp; </span>
                        </div>
                       
                        <input id="emi-months" type="text" class="form-control border-start-0 shadow-sm" value="36" onfocusout="onInputChange('emi-months','months')">
                      </div>
                  </div>
                  <div class="col-sm-12 range-select mt-3">
                  <input type="range" id="months" min="6" max="72" step="3" value="36">
                  </div>
                  </form>
                </div>
              </div>
              <!-- row 2-->
              <!-- row 3-->
              <div class="col-sm-12">
                <button type="submit" class="btn checkbtn knowmore-btn mt-1 p-2 shadow-sm">Check</button>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 col-12">
            <!-- <img src="images/icons/map.webp" class="img-fluid" alt="Apply Now "> -->
            <div class="img-fluid">
              <div id="donut-chart"></div>
            </div>
          </div>

        </div>




        <div class="calculated-emi p-3 rounded-3 mt-5 mb-0">
          <div class="row form-row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-6 mb-3">
              <p class="mb-0">Loan EMI</p>
              <div class="Emi-value" id="monthly-payment"><i class="bi bi-currency-rupee"></i> 7,891</div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-6 mb-3">
              <p class="mb-0">Total Interest Payable</p>
              <div class="Emi-value" id="total-interest"><i class="bi bi-currency-rupee"></i> 3,00,000</div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-6 mb-3">
              <p class="mb-0">Total Payment</p>
              <div class="Emi-value" id="total-payment"><i class="bi bi-currency-rupee"></i> 78,783</div>
            </div>


          </div>
        </div>



      </div>

    </div>
  </div>

  <!-- what our customer say-->

  <h3>Amortization details</h3>
  <div class="p-0 mb-5 pb-3 mt-3 border rounded-3 monthwise_emi">
    <div class="accordion accordion-flush" id="accordionFlushExample">
      <div class="accordion-item mb-2">
        <h2 class="accordion-header" id="flush-headingOne">
          <button class="accordion-button collapsed rounded-sm" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
            2023
          </button>
        </h2>
        <div id="flush-collapseOne" class="accordion-collapse collapse show" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
          <div class="pt-0">
            <div class="bd-example">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Month</th>
                    <th scope="col">Principal Paid</th>
                    <th scope="col">Interest Charged</th>
                    <th scope="col">Total Payment</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Jan</td>
                    <td>₹17,123</td>
                    <td>₹2,123</td>
                    <td>₹19,123</td>
                  </tr>

                  <tr>
                    <td>Feb</td>
                    <td>₹17,123</td>
                    <td>₹2,123</td>
                    <td>₹19,123</td>
                  </tr>

                  <tr>
                    <td>March</td>
                    <td>₹17,123</td>
                    <td>₹2,123</td>
                    <td>₹19,123</td>
                  </tr>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="accordion accordion-flush" id="accordionFlushExample">
      <div class="accordion-item mb-2">
        <h2 class="accordion-header" id="flush-headingOne">
          <button class="accordion-button collapsed rounded-sm" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
            2024
          </button>
        </h2>
        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
          <div class="pt-0">
            <div class="bd-example">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th scope="col">Month</th>
                    <th scope="col">Principal Paid</th>
                    <th scope="col">Interest Charged</th>
                    <th scope="col">Total Payment</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Jan</td>
                    <td>₹17,123</td>
                    <td>₹2,123</td>
                    <td>₹19,123</td>
                  </tr>

                  <tr>
                    <td>Feb</td>
                    <td>₹17,123</td>
                    <td>₹2,123</td>
                    <td>₹19,123</td>
                  </tr>

                  <tr>
                    <td>March</td>
                    <td>₹17,123</td>
                    <td>₹2,123</td>
                    <td>₹19,123</td>
                  </tr>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- end what our customer say-->

  <div class="mb-5">
    <h3>How can home loan EMI calculator help you?</h3>
    <p>Equated Monthly Instalment or EMI is a fixed amount that a borrower must pay back to the lender every month till their tenure ends.

      Calculating the EMI and its components can be a cumbersome exercise for first-time financers. A loan EMI calculator can do these complex calculations in no time, and save you from the trouble of doing it manually.

    <ul>
      <li>Manually performing such complicated home loan EMI calculations can be both time-consuming and inaccurate. A home loan monthly EMI calculator saves the valuable time of prospective home buyers. </li>
      <li> It gives you an accurate estimate, which is pivotal for financial planning. There is no probability of any inaccuracies or ambiguity.
        The EMI calculation procedure for each type of loan is different. For example, home loan EMI calculation is different from personal loan EMI. The housing loan EMI calculator is specific only for home loans. </li>
      <li>
        The online calculator is free for unlimited use. You can check the EMI for various loan amounts and determine which amount suits your financial situation. </li>
    </ul>

    </p>

  </div>

</div>
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
  previousValue = 0;
  $(function() {
    // $(window).resize(function() {
    // 	window.donutChart.redraw();
    // });

    sliderAction('amount', 'emi-amount')
    sliderAction('rate', 'emi-rate')
    sliderAction('months', 'emi-months')

    calculateEMI()

    $(window).keydown(function(event) {
      if (event.keyCode == 13) { // enter key
        event.preventDefault()
        return false
      } else if ([112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123].includes(event.keyCode)) { // function Keys
        return true
      } else if ([8, 9, 27, 37, 38, 39, 40, 127].includes(event.keyCode)) { // backspace, tab, escape, left, up, right, bottom key, delete
        return true
      } else if (event.ctrlKey || event.altKey || event.metaKey) {
        return true
      }

      if (![48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105].includes(event.keyCode)) { // number range
        event.preventDefault()
        return false
      }
    });
  });

  $("input[type=range]").on("change", function() {
    calculateEMI()
  })

  $("#emi-amount,#emi-rate,#emi-months").on("focusin", function() {
    previousValue = this.value
  })

  sliderAction = (slider, input) => {
    var slider = document.getElementById(slider)
    var input = document.getElementById(input)
    input.value = slider.value

    // Update the current slider value (each time you drag the slider handle)
    slider.oninput = function() {
      input.value = this.value
    }
  }

  onInputChange = (i, s) => {
    var input = document.getElementById(i)
    var slider = document.getElementById(s)

    if (i == "emi-amount") {
      if (input.value % 1000 != 0) {
        $.notify({
          message: "Loan Amount Should be in Multiple of 1000."
        }, {
          type: "danger",
          placement: {
            from: "top",
            align: "right"
          },
          animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutUp'
          },
          offset: {
            x: 0,
            y: 50
          }
        });
        input.value = previousValue
        return false
      }
      if (input.value < 50000 || input.value > 1000000) {
        $.notify({
          message: "Loan Amount Should be in the Range of Rs 50000 - 1000000."
        }, {
          type: "danger",
          placement: {
            from: "top",
            align: "right"
          },
          animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutUp'
          },
          offset: {
            x: 0,
            y: 50
          }
        });
        input.value = previousValue
        return false
      }
    } else if (i == "emi-rate") {
      if (input.value < 12 || input.value > 30) {
        $.notify({
          message: "other.Interest Rate Should be in the Range of 12 - 30."
        }, {
          type: "danger",
          placement: {
            from: "top",
            align: "right"
          },
          animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutUp'
          },
          offset: {
            x: 0,
            y: 50
          }
        });
        input.value = previousValue
        return false
      }
    } else if (i == "emi-months") {
      if (input.value % 3 != 0) {
        $.notify({
          message: "Loan Tenure Should be in Multiple of 3."
        }, {
          type: "danger",
          placement: {
            from: "top",
            align: "right"
          },
          animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutUp'
          },
          offset: {
            x: 0,
            y: 50
          }
        });
        input.value = previousValue
        return false
      }
      if (input.value < 6 || input.value > 72) {
        $.notify({
          message: "Loan Tenure Should be in the Range of 6 - 72 Months."
        }, {
          type: "danger",
          placement: {
            from: "top",
            align: "right"
          },
          animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutUp'
          },
          offset: {
            x: 0,
            y: 50
          }
        });
        input.value = previousValue
        return false
      }
    }

    slider.value = input.value
    calculateEMI()
  }

  formatToCurrency = (amount) => {
    return new Intl.NumberFormat('en-IN').format(amount)
  }

  calculateEMI = () => {
    var totalAmount = document.getElementById("amount").value
    var InterestRate = document.getElementById("rate").value
    var LoanTenure = document.getElementById("months").value

    if ([0, ''].includes(totalAmount) || [0, ''].includes(InterestRate) || [0, ''].includes(LoanTenure)) {
      $.notify({
        message: "Please Fill Details"
      }, {
        type: "danger",
        placement: {
          from: "top",
          align: "right"
        },
        animate: {
          enter: 'animated bounceInDown',
          exit: 'animated bounceOutUp'
        },
        offset: {
          x: 0,
          y: 50
        }
      });
      return false
    }

    var principal = parseFloat(totalAmount);
    var CalculateInterest = parseFloat(InterestRate) / 100 / 12;
    var calculatedPayments = parseFloat(LoanTenure);

    var x = Math.pow(1 + CalculateInterest, calculatedPayments);
    var monthly = (principal * x * CalculateInterest) / (x - 1);
    monthlyPayment = monthly.toFixed(2);

    totalInterest = (monthly * calculatedPayments - principal).toFixed(2);

    totalPayment = (monthly * calculatedPayments).toFixed(2);

    document.getElementById("monthly-payment").innerHTML = "Rs " + formatToCurrency(monthlyPayment)
    document.getElementById("total-interest").innerHTML = "Rs " + formatToCurrency(totalInterest)
    document.getElementById("total-payment").innerHTML = "Rs " + formatToCurrency(totalPayment)

    EMIChart(totalPayment, monthlyPayment, totalInterest)
  }


  EMIChart = (amount, monthly, interest) => {
    var chart = new CanvasJS.Chart("donut-chart", {
      exportEnabled: true,
      animationEnabled: true,
      title: {
        text: "Emi Results"
      },
      legend: {
        cursor: "pointer",
        itemclick: explodePie
      },
      data: [{
        type: "pie",
        showInLegend: true,
        toolTipContent: "{name}: <strong>{y}</strong>",
        indexLabel: "{name} - {y}",
        dataPoints: [{
            y: monthly,
            name: "Monthly Payment",
            exploded: true
          },
          {
            y: interest,
            name: "Total Interest"
          },
          {
            y: amount,
            name: "Total Amount"
          },
        ]
      }]
    });
    chart.render();
  }

  function explodePie(e) {
    if (typeof(e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
      e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
    } else {
      e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
    }
    e.chart.render();
  }
</script>
@endsection