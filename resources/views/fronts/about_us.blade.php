@extends('fronts.include.master')

@section('content')
    {{-- <div class="top-banner pt-4 p-3 pb-4">
        <div class="container">
            <h3>Get A Loan</h3>
            <p>Fulfill all your multiple needs with a single choice</p>
            <div class="get-loan-btn pt-4">
                <a href="" class="mt-2">Apply Now</a>
            </div>
        </div>
    </div> --}}

    <div class="container pt-4 emi-page-outer">
        <div class="step-app emi-page mb-3" id="">
            <h1>About TractorPaisa</h1>
            <p class="emi-para pt-2">Lorem ipsum dolor sit amet consectetur. Porttitor aenean quis sem et vitae posuere arcu.
                Proin aliquet egestas odio sed pharetra porttitor. In nisl phasellus mi tristique sed diam diam magnis
                aenean. Leo nunc at est sem nibh.
                In ipsum dictum id quam ultrices purus quis. Adipiscing risus tellus pellentesque amet malesuada ac congue
                massa. Mi viverra diam mi velit est. Amet purus ultrices elit mattis sit massa magna in. Cursus suscipit
                morbi posuere quis sit dictumst. Quam arcu quis et id massa vitae praesent in bibendum. Augue ipsum est
                massa eu etiam eget ligula varius ut. Sit ipsum lectus commodo lacinia semper donec sit commodo.</p>

        </div>


        <div class="mb-5">
            <h3>Comfortable Way to Fulfill your Dreams</h3>
            <p>Lorem ipsum dolor sit amet consectetur. Neque urna tortor amet viverra habitasse nibh. Tempor pretium feugiat
                tempus a id habitant ultricies id malesuada. Blandit condimentum ac malesuada pharetra. Turpis proin id et a
                sodales arcu massa consequat molestie. Turpis fringilla nibh ante morbi enim eget vestibulum. Sociis
                sagittis suscipit ac suspendisse. Sit lectus cursus aenean ut.
                Massa in tellus quam tristique orci habitant. Viverra mollis pellentesque mauris sit facilisis. Tortor
                consequat pellentesque velit massa aliquam molestie laoreet at metus. Mattis donec molestie ut diam. Magna
                duis duis volutpat eget. Ornare venenatis libero consequat id sit nulla bibendum mauris purus. Ullamcorper
                mauris nunc placerat enim orci. Consequat pharetra aliquam interdum justo velit euismod. Cras lectus nec
                ridiculus a proin. Urna aliquam convallis ultrices accumsan. Vel venenatis quam rhoncus nibh. Sed integer
                nulla aenean commodo nibh ut molestie augue. Aenean nunc id turpis in mattis et commodo.
                Ultrices blandit ultrices suspendisse semper bibendum augue ultrices. Dui ullamcorper nisl et mauris massa
                purus turpis. Mattis quis nec gravida tortor. Non massa placerat volutpat aliquam. Ac at blandit semper sed
                feugiat velit eget. Sed urna dignissim eget aliquet sed tempor mattis urna. Ut nulla non vel ultrices sit
                metus. Eu aliquet est quam non in amet ornare.


            </p>

        </div>

        <div class="mb-5">
            <h3>Services Provided by TractorPaisa</h3>
            <ul>
                <li>Manually performing such complicated home loan EMI calculations can be both time-consuming and
                    inaccurate. A home loan monthly EMI calculator saves the valuable time of prospective home buyers. </li>
                <li> It gives you an accurate estimate, which is pivotal for financial planning. There is no probability of
                    any inaccuracies or ambiguity.
                    The EMI calculation procedure for each type of loan is different. For example, home loan EMI calculation
                    is different from personal loan EMI. The housing loan EMI calculator is specific only for home loans.
                </li>
                <li>
                    The online calculator is free for unlimited use. You can check the EMI for various loan amounts and
                    determine which amount suits your financial situation. </li>
            </ul>

        </div>
    </div>




    <!-- faq-->
    <div class="questions">
        <div class="container pt-4 pb-3">
            <h2 class="mb-3">Have a question? Here to help.</h2>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <?php $show = 0; ?>
                @foreach ($faqs as $cat)
                    <?php
                    if ($show == 0) {
                        $show = 'show';
                    } else {
                        $show = '';
                    }
                    ?>
                    <div class="accordion-item mb-2">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed rounded-sm" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapse{{ $cat->id }}" aria-expanded="false"
                                aria-controls="flush-collapseOne">
                                {!! $cat->question !!}
                            </button>
                        </h2>
                        <div id="flush-collapse{{ $cat->id }}" class="accordion-collapse collapse <?php echo $show; ?>"
                            aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body pt-0">{!! $cat->answer !!} </div>
                        </div>
                    </div>
                    <?php $show = 1; ?>
                @endforeach

            </div>
        </div>
    </div>
    <!-- end faq-->
@endsection
