@extends('fronts.include.master')

@section('content')
    <div class="container pt-3 mb-5">
        <div class="laon-form step-content">
            <div class="alert thanksmsg text-center p-3" role="alert">
                <h2 class="alert-heading">Thank you! Our team will get back in touch with you soon</h2>
                <!--  <p>Your Evaluation appointment has been requested</p> -->

            </div>
            <div class="thanks-details">
                <?php $imageurl = ''; ?>
                @if ($userlead->front_image != '')
                    <?php
                    $file = explode('/', $userlead->front_image);
                    $fileurl = $file['2'];
                    $imageurl = env('APP_URL') . '/storage/app/public/upload/' . $fileurl; ?>
                @elseif ($userlead->back_image != '')
                    <?php
                    $file = explode('/', $userlead->back_image);
                    $fileurl = $file['2'];
                    $imageurl = env('APP_URL') . '/storage/app/public/upload/' . $fileurl; ?>
                @elseif ($userlead->leftside_image != '')
                    <?php
                    $file = explode('/', $userlead->leftside_image);
                    $fileurl = $file['2'];
                    $imageurl = env('APP_URL') . '/storage/app/public/upload/' . $fileurl; ?>
                @elseif ($userlead->rightside_image != '')
                    <?php
                    $file = explode('/', $userlead->rightside_image);
                    $fileurl = $file['2'];
                    $imageurl = env('APP_URL') . '/storage/app/public/upload/' . $fileurl; ?>
                @endif
                <?php if($imageurl!=""){?>
                <img src="{{ $imageurl }}">
                <?php } ?>
            </div>

            <div class="container">
                <div class="row pt-0 mt-3 thanks-tractor-details">
                    <div class="tractor-bg tractor-details-bx rounded-3 mt-0  pt-2 pb-2 mb-2">
                        <div class="container p-2">
                            <h4 class="mb-2 mt-0">{{ $userlead->brand_title }} | {{ $userlead->models_title }} </h4>
                            <div class="row">

                                <div class="col-lg-3 col-md-4 col-4 tractor-det">
                                    <div class="evalation-row">
                                        Year <h5>{{ $userlead->year }} </h5>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-4 col-4 tractor-det">
                                    <div class="evalation-row">
                                        Ownership <h5>{{ $userlead->owners_title }}</h5>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-4 col-4 tractor-det">
                                    <div class="evalation-row">
                                        Hours driven <h5>{{ $userlead->hours_title }}</h5>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-4 col-4 tractor-det">
                                    <div class="evalation-row">
                                        Engine <h5>{{ $userlead->engine }}</h5>
                                    </div>
                                </div>


                                <div class="col-lg-3 col-md-4 col-4 tractor-det">
                                    <div class="evalation-row">
                                        Tyres <h5>{{ $userlead->tyre }}</h5>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-md-6 col-12 tractor-det">
                                    <div class="evalation-row">
                                        Location <h5>
                                            {{ $userlead->state_title }}
                                            <?php if($userlead->district!=""){?>
                                            ,{{ $userlead->district }}
                                            <?php } ?>
                                            <?php if($userlead->tahshil!=""){?>
                                            ,{{ $userlead->tahshil }}
                                            <?php } ?>

                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="{{ route('home') }}" class="btn checkbtn knowmore-btn mt-3 p-2">Back to Home page </a>

        </div>
    </div>
@endsection
