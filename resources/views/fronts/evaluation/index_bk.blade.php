@extends('fronts.include.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <Style>
        .open-button {
            display: none;
        }
    </style>

    <div class="container pt-3">
        <a href="{{ route('home') }}" class="close-steps">x</a>
        <div class="step-app" id="demo">
            <ul class="step-steps" id="steps">
                <li data-step-target="brand" class="brandFirst" id="brandli">Brand</li>
                <li data-step-target="model" id="modelli">Model</li>
                <li data-step-target="year" id="yearli">Year</li>
                <li data-step-target="location" id="locationli">Location</li>
                <li data-step-target="ownership" id="ownershipli">Ownership</li>
                <li data-step-target="hours" id="hourli">Hours</li>
                <li data-step-target="engine" id="engineli">Condition</li>
                {{-- <li data-step-target="tyre">Tyre</li> --}}
                <li data-step-target="images">Images</li>

            </ul>

            <div class="step-content">
                <div class="step-tab-panel" data-step="brand">
                    <h3 class="pt-2 mb-3">Select the brand of your tractor </h3>
                    <div class="container">
                        <div class="step-footer row pt-0 pb-5 brand-section">

                            <!-- brand logo-->
                            @foreach ($brand as $cat)
                                <div class="col-lg-2 col-md-4 col-4 g-2">
                                    <button data-step-action="next"
                                        class="inner-btn step-btn p-2 bg-white shadow-sm border border-grey rounded-3 text-center sbrand"
                                        brandName="{!! $cat->title !!}" brandId="{{ $cat->id }}">

                                        <div class="brand-log">
                                            <?php if (isset($cat->logo)) { ?>
                                            @if ($cat->logo != '')
                                                <?php
                                                $file = explode('/', $cat->logo);
                                                $fileurl = $file['2']; ?>
                                                <img src="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                                    alt="{!! $cat->title !!}">
                                            @endif
                                            <?php } ?>

                                        </div>
                                        <p>{!! $cat->title !!}</p>
                                    </button>
                                </div>
                            @endforeach

                            {{-- <button data-step-action="prev" class="step-btn">Previous</button> --}}

                        </div>

                    </div>
                </div>

                <div class="step-tab-panel" data-step="model">
                    <h3 class="pt-2 mb-0">Please Select The Model Of Your Tractor </h3>
                    <div class="container">
                        <div class="step-footer row pt-2 pb-5">
                            <div class="step-footer row pb-5 " id="modelList"></div>

                            <a id="modelButton" data-step-action="next" style="display: none;"></a>
                            {{-- <div class="col-sm-12 mb-5">
                                <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                        src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                        alt="back">Back</button>
                            </div> --}}
                        </div>
                    </div>
                </div>

                <div class="step-tab-panel" data-step="year">
                    <h3 class="pt-2 mb-0">Select the Year of your tractor </h3>
                    <div class="container">
                        <div class="step-footer row pt-3 pb-5">
                            <?php foreach ($year as $b) { ?>
                            <div class="col-lg-2 col-md-3 col-12 g-2">
                                <button data-step-action="next"
                                    class="inner-btn step-btn border-0 year-border p-2 border-bottom bg-white text-left text-start syear"
                                    yearId="{{ $b->id }}" yearName="{{ $b->year }}">
                                    <p>{{ $b->year }}</p>
                                </button>
                            </div>
                            <?php } ?>
                            {{-- <div class="col-sm-12 mb-5">
                                <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                        src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                        alt="back">Back</button>
                            </div> --}}


                        </div>
                    </div>
                </div>
                <!--  series Model starts-->


                <!--  series Model end starts-->

                <div class="step-tab-panel" data-step="location">
                    <h3 class="pt-2 mb-0">Select your location </h3>
                    <div class="form-group pt-3">
                        <label for="State">State <span class="err">*</span></label>
                        <select class="form-control" name="state_id" id="state_id">
                            <option value=""> Select State</option>
                            <?php foreach ($states as $b) { ?>
                            <option value="{{ $b->id }}">{{ $b->name }}</option>
                            <?php } ?>
                        </select>
                        <div class="error-msg stateerror" style="display:none"> Please select State</div>
                    </div>
                    <div class="form-group pt-3">
                        <label for="State">District </label>
                        <select class="form-control" name="district_id" id="district_id">
                            <option value=""> Select District</option>
                        </select>
                        <div class="error-msg districterror" style="display:none"> Please select District</div>
                    </div>
                    <div class="form-group pt-3">
                        <label for="State">Tehsil</label>
                        <select class="form-control" name="tahshil_id" id="tahshil_id">
                            <option value=""> Select Tehsil</option>
                        </select>
                        <div class="error-msg tahshilerror" style="display:none"> Please select Tehsil</div>
                    </div>

                    <div class="step-footer pt-3 mt-2 mb-3">
                        <div id="LocationDone"> <button data-step-action="next"
                                class="step-btn btn checkbtn knowmore-btn p-2 shadow  locationNext"
                                style="display: none !important;">Proceed</button>
                        </div>
                        <button class="step-btn btn checkbtn knowmore-btn p-2 shadow checkLocation">Proceed</button>
                        {{-- <div class="col-sm-12 mb-5">
                            <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                    src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                    alt="back">Back</button>
                        </div> --}}

                    </div>



                </div>

                <div class="step-tab-panel" data-step="ownership">
                    <h3 class="pt-2 mb-0">Select tractor ownership </h3>
                    <div class="container">
                        <div class="step-footer row pt-3 pb-5">

                            <!-- brand logo-->
                            @foreach ($owners as $o)
                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button data-step-action="next"
                                        class="inner-btn step-btn p-3 shadow-sm bg-white border border-grey rounded-3 text-center sowner"
                                        ownerId="{{ $o->id }}" ownerTitle="{{ $o->title }}">
                                        <p>{{ $o->title }}</p>
                                        <div class="owner-img">
                                            <img src="{{ asset('public/front/images/icons/owner' . $o->id . '.webp') }}"
                                                class="" alt="1st owner">
                                        </div>
                                    </button>
                                </div>
                            @endforeach
                            <!-- end brand logo-->



                            <!-- end brand logo-->
                            {{--
                            <div class="col-sm-12 mb-5">
                                <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                        src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                        alt="back">Back</button>
                            </div> --}}

                        </div>
                    </div>
                </div>

                <div class="step-tab-panel" data-step="hours">
                    <h3 class="pt-2 mb-0">Select hours driven</h3>
                    <div class="container">
                        <div class="step-footer row pt-3 pb-5">
                            <?php foreach ($hours as $b) { ?>
                            <div class="col-lg-2 col-md-3 col-12 g-2">

                                <button data-step-action="next"
                                    class="inner-btn step-btn border-0 year-border p-2 border-bottom bg-white text-left text-start shour"
                                    hourId="{{ $b->id }}" hourName="{{ $b->title }}">
                                    <p>{{ $b->title }}</p>
                                </button>
                            </div>
                            <?php } ?>
                            {{-- <div class="col-sm-12">
                                <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                        src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                        alt="back">Back</button>
                            </div> --}}


                        </div>
                    </div>
                </div>

                <div class="step-tab-panel" data-step="engine">
                    <h3 class="pt-2 mb-0">Engine Condition </h3>
                    <div class="error-msg engineerror" style="display:none"> Please select Engine Condition</div>
                    <div class="step-footer  pt-2 pb-3">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button
                                        class="inner-btn shadow-sm step-btn p-2 bg-white border border-grey rounded-3 text-center sengine"
                                        enginevalue="76-100% (Very Good)">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/EngineVeryGood.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>76-100% (Very Good)</p>
                                    </button>
                                </div>
                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button
                                        class="inner-btn shadow-sm step-btn p-2 bg-white border border-grey rounded-3 text-center sengine"
                                        enginevalue="51-75% (Good)">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/EngineGood.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>51-75% (Good)</p>
                                    </button>
                                </div>
                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button
                                        class="inner-btn shadow-sm step-btn p-2 bg-white border border-grey rounded-3 text-center sengine"
                                        enginevalue="26-50% (Average)">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/EngineAverage.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>26-50% (Average)</p>
                                    </button>
                                </div>
                                <div class="col-lg-2 col-md-4 col-6 g-2">

                                    <button
                                        class="inner-btn shadow-sm step-btn p-2 bg-white border border-grey rounded-3 text-center sengine"
                                        enginevalue="0-25% (Poor)">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/EnginePoor.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>0-25% (Poor)</p>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row pt-2 pb-5">
                                <h3 class="pt-2 mt-2 mb-1">Tyre Condition </h3>
                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button
                                        class="inner-btn step-btn p-2 bg-white border border-grey rounded-3 text-center checkengine"
                                        tyrevalue="76-100% (Very Good)" ttype="vgood">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/TyreVeryGood.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>76-100% (Very Good)</p>
                                    </button>
                                    <button data-step-action="next" id="vgood"
                                        class="inner-btn step-btn  styre hidden-btn" tyrevalue="76-100% (Very Good)"
                                        style="display: none;">
                                    </button>
                                </div>
                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button
                                        class="inner-btn step-btn p-2 shadow-sm bg-white border border-grey rounded-3 text-center checkengine"
                                        tyrevalue="51-75% (Good)" ttype="good">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/TyreGood.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>51-75% (Good)</p>
                                    </button>
                                    <button data-step-action="next" id="good"
                                        class="inner-btn step-btn  styre hidden-btn" tyrevalue="51-75% (Good)"
                                        style="display: none;">
                                    </button>
                                </div>

                                <div class="col-lg-2 col-md-4 col-6 g-2">
                                    <button
                                        class="inner-btn step-btn p-2 shadow-sm bg-white border border-grey rounded-3 text-center checkengine"
                                        tyrevalue="26-50% (Average)" ttype="average">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/TyreAverage.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>26-50% (Average)</p>
                                    </button>
                                    <button data-step-action="next" id="average"
                                        class="inner-btn step-btn  styre hidden-btn" tyrevalue="26-50% (Average)"
                                        style="display: none;">
                                    </button>
                                </div>
                                <div class="col-lg-2 col-md-4 col-6 g-2">

                                    <button
                                        class="inner-btn step-btn p-2 shadow-sm bg-white border border-grey rounded-3 text-center checkengine"
                                        tyrevalue="0-25% (Poor)" ttype="poor">
                                        <div class="engine-img">
                                            <img src="{{ asset('public/front/images/icons/TyrePoor.svg') }}"
                                                class="" alt=" ">
                                        </div>
                                        <p>0-25% (Poor)</p>
                                    </button>
                                    <button data-step-action="next" id="poor"
                                        class="inner-btn step-btn  styre hidden-btn" tyrevalue="26-50% (Average)"
                                        style="display: none;">
                                    </button>
                                </div>
                                {{-- <div class="col-sm-12 mb-5">
                                        <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                                src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                                alt="back">Back</button>
                                    </div> --}}


                            </div>
                        </div>
                    </div>


                </div>

                <div class="step-tab-panel" data-step="images">
                    <h3 class="pt-2 mb-0">Upload Images</h3>
                    <p class="upload-img-txt"> Please upload atleast one image</p>


                    <div class="container">
                        <div class="step-footer row pt-3 pb-5">
                            <div class="col-lg-2 col-md-4 col-6 g-2">
                                <div
                                    class="uploadimg inner-btn step-btn p-2 bg-white border shadow-sm border-grey rounded-3 text-center">
                                    <div class="">
                                        <img src="" class="img-fluid" id="showfrontSide">
                                        <div id="iconfrontSide">

                                            <img src="{{ asset('public/front/images/icons/tractor-front-icon.webp') }}"
                                                class="" alt="">
                                        </div>
                                        <p> Front Side</p>
                                    </div>
                                    <input type="file" id="frontSide" name="file" />
                                </div>

                            </div>

                            <div class="col-lg-2 col-md-4 col-6 g-2">
                                <div
                                    class="uploadimg inner-btn step-btn p-2 bg-white border shadow-sm border-grey rounded-3 text-center">
                                    <div class="">
                                        <img src="" class="img-fluid" id="showbackSide">
                                        <div id="iconbackSide">
                                            <img src="{{ asset('public/front/images/icons/tractor-back-icon.webp') }}"
                                                class="" alt="">
                                        </div>
                                        <p>Back Side</p>
                                    </div>

                                    <input type="file" id="backSide" name="file" />
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-4 col-6 g-2">
                                <div
                                    class="uploadimg inner-btn step-btn p-2 bg-white border shadow-sm border-grey rounded-3 text-center">
                                    <div class="">
                                        <img src="" class="img-fluid" id="showleftSide">
                                        <div id="iconleftSide">
                                            <img src="{{ asset('public/front/images/icons/tractor-left-icon.webp') }}"
                                                class="" alt="">
                                        </div>
                                        <p>Left Side</p>
                                    </div>

                                    <input type="file" id="leftSide" name="file" />
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-4 col-6 g-2">
                                <div
                                    class="uploadimg inner-btn step-btn p-2 bg-white border shadow-sm border-grey rounded-3 text-center">
                                    <div class="">
                                        <img src="" class="img-fluid" id="showrightSide">
                                        <div id="iconrightSide">
                                            <img src="{{ asset('public/front/images/icons/tractor-right-icon.webp') }}"
                                                class="" alt="">
                                        </div>
                                        <p>Right Side</p>
                                    </div>

                                    <input type="file" id="rightSide" name="file" />
                                </div>
                            </div>
                            <div class="error-msg imageerror" style="display:none"> Please upload minimum one image
                            </div>
                            <input type="button" data-step-action="next" id="uploadImages1" value="Next"
                                class="btn checkbtn knowmore-btn mt-3 p-2 hidden-btn" />
                            <button type="button" id="uploadImages" value="Next"
                                class="btn checkbtn knowmore-btn mt-3 p-2">Next</button>
                            {{--

                        <div class="col-sm-12 mb-5">
                            <button data-step-action="prev" class="step-btn btn back-btn rounded-4 mt-4"><img
                                    src="{{ asset('public/front/images/icons/back-arrow.svg') }}"
                                    alt="back">Back</button>
                        </div> --}}

                        </div>
                    </div>

                </div>

                <div class="step-tab-panel" data-step="login">
                    <h3 class="pt-2 mb-0">Enter mobile number to see your tractor’s valuation </h3>
                    <div class="step-footer row pt-1 pb-3">
                        <div class="pt-2 pb-2 login">
                            <form>
                                <input type="text" maxlength="10" class="form-control Number" id="mobileNumber"
                                    aria-describedby="emailHelp" placeholder="Enter your mobile number">
                                <div class="error-msg mobileerror" style="display:none"> </div>
                                <input type="button" id="sendOtp" value="Send OTP"
                                    class="btn checkbtn knowmore-btn mt-3 p-2" />
                                <div id="evaluate" class="form-row" style="display:none;">
                                    <div class="otpsend mt-2 mb-3">
                                        <div class="otpsend mt-1 mb-3"> OTP has been sent to
                                            <span id="mNumber"></span> <a href="javascript:void(0)"
                                                id="changeMobile">Edit</a>
                                        </div>
                                        <input type="text" maxlength="4" id="mobileOtp" class="form-control Number"
                                            placeholder="Enter the OTP">
                                        <div class="error-msg otperror" style="display:none"> </div>
                                        <div class="text-end otpsend mt-2">Didn’t receive? please wait for 52 sec...
                                            <br>
                                            <a href="javascript:void(0)">Resend OTP</a>
                                        </div>
                                        <input type="button" id="checkEvaluate" value="Evaluate"
                                            class="btn checkbtn knowmore-btn mt-3 p-2" />
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="mt-3" id="editTractorDetails">
                            <div class="pt-0">
                                <div class="tractor-bg tractor-details-bx rounded-3 mt-0 p-3 pt-4 pb-4 mb-4">
                                    <div class="container">
                                        <h4 class="mb-2">Tractor details</h4>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Brand
                                                    <h5> <span id="brandDetail"></span></h5>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Model <h5><span id="modelDetail"></span> </h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-12 tractor-det">
                                                {{-- <div class="evalation-row">
                                                    Location <h5><span id="tahshilDetails"></span> <span
                                                            id="districtDetails"></span><span id="stateDetails"></span>
                                                    </h5>
                                                </div> --}}
                                                <div class="evalation-row">
                                                    Location <h5><span id="locationDetails"></span>
                                                    </h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Year <h5><span id="yearDetail"></span> </h5>
                                                </div>
                                            </div>





                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Ownership <h5><span id="ownerDetail"></span></h5>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Hours driven <h5><span id="hourDetail"></span></h5>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Engine <h5><span id="engineDetail"></span></h5>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                                <div class="evalation-row">
                                                    Tyres <h5><span id="tyreDetail"></span></h5>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-6 col-6 g-2">
                                                <a href="javascript:void(0)"
                                                    class="btn btn-sm rounded-3 edit-btn shadow editStart"
                                                    id="editDetail">Edit</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <form id="finalSubmit" name="finalSubmit" enctype="multipart/form-data" method="POSt"
                action="{{ route('submitdetail') }}" style="display:none">
                @csrf
                <input type="text" id="fbrand_id" name="brand_id" />
                <input type="text" id="fyear_id" name="year_id" />
                <input type="text" id="fmodel_id" name="model_id" />
                <input type="text" id="fstate_id" name="state_id" />
                <input type="text" id="fdistrict_id" name="district_id" />
                <input type="text" id="ftahshil_id" name="tahshil_id" />
                <input type="text" id="fowner_id" name="owner_id" />
                <input type="text" id="fhour_id" name="hour_id" />
                <input type="text" id="fengine" name="engine" />
                <input type="text" id="ftyre" name="tyre" />
                <input type="file" id="ffrontSide" name="frontSide" />
                <input type="file" id="fbackSide" name="backSide" />
                <input type="file" id="fleftSide" name="leftSide" />
                <input type="file" id="frightSide" name="rightSide" />
                <input type="text" id="userMobile" name="userMobile" />
                <input type="submit" name="finalSubmitbtn" id="finalSubmitbtn">
            </form>

        </div>
    </div>

    <script src="{{ asset('public/front/js/jquery-steps.js') }}"></script>
    <script type="text/javascript">
        $('#mobileOtp').keyup(function() {
            if ($("#mobileOtp").val() != "") {
                $(".otperror").hide();
            }
        });
        $("#uploadImages").on("click", function() {
            var imageError = 0;
            if ($("#frontSide").val() == "" && $("#backSide").val() == "" && $("#leftSide").val() == "" && $(
                    "#rightSide").val() == "") {
                var imageError = 1;
                //return false;
            } else {
                imageError == 0;
                //return false;
            }
            // if ($("#frontSide").val() == "") {
            //     //alert("not slected");
            //     var imageError = 1;
            //     //$('.imageerror').show();
            //     //return false;
            // } else if ($("#backSide").val() == "") {
            //     //alert("not slected");
            //     var imageError = 1;

            // } else if ($("#leftSide").val() == "") {
            //     //alert("not slected");
            //     var imageError = 1;
            //     $('.imageerror').show();

            // } else if ($("#rightSide").val() == "") {
            //     //alert("not slected");
            //     var imageError = 1;
            //     $('.imageerror').show();
            //     return false;
            // } else {
            //     imageError == 0;
            // }


            if (imageError == 1) {
                $('.imageerror').show();
                return false;
            } else {
                $('.frontimageerror').hide();
                $("#uploadImages1").click();
            }

        });

        $('#demo').steps({
            onFinish: function() {
                alert('Wizard Completed');
            }
        });
        $("#LocationDone").hide();
        $(".checkLocation").on('click', function() {
            var error = 0;
            if ($("#state_id").val() == "") {
                //alert()
                error = 1
                $(".stateerror").show();
                return false

            } else {
                $(".stateerror").hide();
            }
            // if ($("#district_id").val() == "") {

            //     error = 1
            //     $(".districterror").show();
            //     return false

            // } else {
            //     $(".districterror").hide();
            // }
            // if ($("#tahshil_id").val() == "") {

            //     error = 1
            //     $(".tahshileerror").show();
            //     return false

            // } else {
            //     $(".tahshileerror").hide();
            // }
            if (error == 1) {
                return false
            } else {
                $(".locationNext").click();
            }
        });

        $(function() {
            // $(".inner-btn").click(function() {
            //     // $(this).addClass("active-step");
            // });
            $(".editStart").click(function() {
                $(".brandFirst").click();
                //$('#steps').scrollLeft = 10;
                //$('#steps').scrollLeft($('#steps').scrollLeft() - 20);
                $(".brandFirst").focus()
            });
        });
        $(function() {
            $('.step-steps li').click(function(event) {
                event.preventDefault();
                $('.step-steps li.active').animate({
                    scrollLeft: "+=50px"
                }, "slow");
            });



            $(document).on('click', 'inner-btn', function() {
                $('html, body').animate({
                    scrollTop: $('html, body').offset().top,
                });
            });
        });
        $(document).on('click', '.inner-btn', function() {
            document.getElementById('steps').scrollLeft += 20;
        });

        $(document).on('click', '.step-steps li', function() {
            document.getElementById('steps').scrollLeft -= 60;
        });
        $("#editDetail").click(function() {
            document.getElementById('steps').scrollLeft -= 60;
        });
        // $(".active").click(function() {
        //     document.getElementById('steps').scrollLeft -= 60;
        // });

        function showDiv() {
            document.getElementById('evaluate').style.display = "block";
            document.getElementById('sendOtp').style.display = "none";
            document.getElementById('editTractorDetails').style.display = "none";
        }
    </script>
    <script>
        $(document).on("click", ".sbrand", function(e) {
            $(".sbrand").removeClass("active-step");
            $(this).addClass("active-step");
            e.preventDefault();
            // alert($(this).attr("brandName"));
            //alert($(this).attr("brandId"));
            var brand_id = $(this).attr("brandId");
            var brandDetail = $(this).attr("brandName");
            $("#fbrand_id").val(brand_id)
            $("#brandDetail").html(brandDetail)
            $("#brandli").html('<span>' + brandDetail + '</span>')
            $("#brandli").addClass("completed");
            getModelbyBrand(brand_id, 0);
            $('html, body').animate({
                scrollTop: $('html, body').offset().top,
            });
        });


        $(document).on("click", ".syear", function(e) {
            e.preventDefault();
            // alert($(this).attr("brandName"));
            //alert($(this).attr("brandId"));
            $(".syear").removeClass("active-step");
            $(this).addClass("active-step");
            var yearId = $(this).attr("yearId");
            var yearName = $(this).attr("yearName");
            var brand_id = $("#fbrand_id").val();
            //alert(yearName);
            $("#fyear_id").val(yearId)
            $("#yearDetail").html(yearName)
            $("#yearli").html('<span>' + yearName + '</span>')
            $("#yearli").addClass("completed")
            //getModelbyBrand(brand_id, yearId)

        });


        function getModelbyBrand(brand_id, year_id) {

            $.ajax({
                url: "{{ route('getModelbyBrand') }}",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: "POST",
                data: {
                    "brand_id": brand_id,
                    "year_id": year_id,

                },
                dataType: "json",
                success: function(response) {
                    $('#modelList').html(response);
                }
            });
        }
        $(document).on('click', '#modelList .getModelId', function() {
            // alert($(this).attr("model_id"));
            var model_id = $(this).attr("model_id");
            var modelName = $(this).attr("modelName");
            $("#fmodel_id").val(model_id)
            $("#modelDetail").html(modelName)
            $("#modelli").html('<span>' + modelName + '</span>')
            $("#modelli").addClass("completed")
            $(".getModelId").removeClass("active-step");
            $(this).addClass("active-step");
            $("#modelButton").click();
            $('html, body').animate({
                scrollTop: $('html, body').offset().top,
            });
        });
        $(document).on('click', '.sowner', function() {
            // alert($(this).attr("model_id"));
            $(".sowner").removeClass("active-step");
            var owner_id = $(this).attr("ownerId");
            var ownerTitle = $(this).attr("ownerTitle");
            $("#fowner_id").val(owner_id)
            $("#ownerDetail").html(ownerTitle)
            $("#ownershipli").html('<span>' + ownerTitle + '</span>')
            $("#ownershipli").addClass("completed")
            $(this).addClass("active-step");

        });
        $(document).on('click', '.shour', function() {
            // alert($(this).attr("model_id"));
            $(".shour").removeClass("active-step");
            var hour_id = $(this).attr("hourId");
            var hourName = $(this).attr("hourName");
            $("#fhour_id").val(hour_id)
            $("#hourDetail").html(hourName)
            $("#hourli").html('<span>' + hourName + '</span>')
            $("#hourli").addClass("completed")
            $(this).addClass("active-step");

        });
        $(document).on('click', '.sengine', function() {
            // alert($(this).attr("model_id"));
            var engine = $(this).attr("enginevalue");
            $("#fengine").val(engine)
            $("#engineDetail").html(engine)
            $("#engineli").html('<span>' + engine + '</span>')
            $("#engineli").addClass("completed")
            $(".sengine").removeClass("active-step");
            $(this).addClass("active-step");
        });
        $(document).on('click', '.checkengine', function() {
            $(".checkengine").removeClass("active-step");
            var enginValue = $("#fengine").val();
            if (enginValue != "") {
                $(".engineerror").hide();
                var ttype = $(this).attr("ttype");
                $(this).addClass("active-step");
                $("#" + ttype).click();
            } else {
                $(".engineerror").show();
            }
        });
        $(document).on('click', '.styre', function() {
            // alert($(this).attr("model_id"));
            var tyre = $(this).attr("tyrevalue");
            $("#ftyre").val(tyre)
            $("#tyreDetail").html(tyre)
            $(".styre").removeClass("active-step");
            $(this).addClass("active-step");
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#state_id').on('change', function() {
                $(".stateerror").hide();
                var state_id = $(this).val();
                var stateDetails = $("#state_id option:selected").text();

                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('frontgetStateDistricts') }}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {
                        "state_id": state_id,

                    },
                    dataType: "json",
                    success: function(response) {
                        $('#district_id').html(response);
                        $("#fstate_id").val(state_id)
                        // $("#stateDetails").html(stateDetails)
                        $("#locationDetails").html(stateDetails)
                        $("#locationli").html('<span>' + stateDetails + '</span>')
                        $("#locationli").addClass("completed")
                        $('#tahshil_id').html("<option> Select Tahshil</option>");
                    }
                });
            });
            $('#district_id').on('change', function() {
                $(".districterror").hide();
                var district_id = $(this).val();
                var districtDetails = $("#district_id option:selected").text();
                var stateDetails = $("#state_id option:selected").text();
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('frontgetDistrictsTahshil') }}",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    data: {
                        "district_id": district_id,

                    },
                    dataType: "json",
                    success: function(response) {
                        $('#tahshil_id').html(response);
                        $("#fdistrict_id").val(district_id)
                        $("#districtDetails").html(districtDetails)
                        $("#locationDetails").html(districtDetails + ',' + stateDetails)
                        $("#locationli").html('<span>' + districtDetails + ',' + stateDetails +
                            '</span>')
                        $("#locationli").addClass("completed")
                    }
                });
            });

            $('#tahshil_id').on('change', function() {
                $(".tahshilerror").hide();
                var tahshil_id = $(this).val();
                var tahshilDetails = $("#tahshil_id option:selected").text();
                var districtDetails = $("#district_id option:selected").text();
                var stateDetails = $("#state_id option:selected").text();
                $("#ftahshil_id").val(tahshil_id)
                $("#locationDetails").html(tahshilDetails + ',' + districtDetails + ',' + stateDetails)
                // $("#tahshilDetails").html(tahshilDetails + ',' + districtDetails + ',' + stateDetails)
                $("#locationli").html('<span>' + tahshilDetails + ',' + districtDetails + ',' +
                    stateDetails + '</span>')
                $("#locationli").addClass("completed")
            });
        });

        $("#showfrontSide").hide();
        $("#showbackSide").hide();
        $("#showleftSide").hide();
        $("#showrightSide").hide();
        $("#frontSide").on("change", function(event) {
            let fileInputElement = document.getElementById('ffrontSide');
            fileInputElement.files = event.target.files;
            var image = document.getElementById('showfrontSide');
            image.src = URL.createObjectURL(event.target.files[0]);
            $("#iconfrontSide").hide();
            $("#showfrontSide").show();
            $('.imageerror').hide();
        });
        $("#backSide").on("change", function(event) {
            let fileInputElement = document.getElementById('fbackSide');
            fileInputElement.files = event.target.files;
            var image = document.getElementById('showbackSide');
            image.src = URL.createObjectURL(event.target.files[0]);
            $("#iconbackSide").hide();
            $("#showbackSide").show();
            $('.imageerror').hide();
        });
        $("#leftSide").on("change", function(event) {
            let fileInputElement = document.getElementById('fleftSide');
            fileInputElement.files = event.target.files;
            var image = document.getElementById('showleftSide');
            image.src = URL.createObjectURL(event.target.files[0]);
            $("#iconleftSide").hide();
            $("#showleftSide").show();
            $('.imageerror').hide();
        });
        $("#rightSide").on("change", function(event) {
            let fileInputElement = document.getElementById('frightSide');
            fileInputElement.files = event.target.files;
            var image = document.getElementById('showrightSide');
            image.src = URL.createObjectURL(event.target.files[0]);
            $("#iconrightSide").hide();
            $("#showrightSide").show();
            $('.imageerror').hide();
        });

        $('.Number').keypress(function(e) {
            var arr = [];
            var kk = e.which;

            for (i = 48; i < 58; i++)
                arr.push(i);

            if (!(arr.indexOf(kk) >= 0))
                e.preventDefault();
        });
        $(document).on("click", "#sendOtp", function(e) {

            var mobileNumber = $("#mobileNumber").val();
            var phone_pattern = /^(0|91)?[6-9][0-9]{9}$/;
            if (!phone_pattern.test(mobileNumber)) {
                $(".mobileerror").html("Please Enter Valid Mobile Number");
                $(".mobileerror").show();
                return false
            } else {
                $(".mobileerror").hide();
            }

            if (mobileNumber.length == 10) {
                showDiv();
                $(".mobileerror").hide();
            } else {
                $(".mobileerror").html("Mobile Number Shuold be 10 Digit");
                $(".mobileerror").show();
                return false
            }
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{ route('submitmobile') }}",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: "POST",
                data: {
                    "mobileNumber": mobileNumber,

                },
                dataType: "json",
                success: function(response) {
                    $("#mobileNumber").hide();
                    $("#mNumber").html(mobileNumber);
                    //$("#checkEvaluate").show();
                    showDiv();

                }
            });
        });

        $(document).on("click", "#changeMobile", function(e) {
            $("#evaluate").hide();
            //$("#checkEvaluate").hide();
            $("#mobileNumber").show();
            $("#sendOtp").show();
            //$("#mobileNumber").val("");
            $("#mNumber").html("");
            $("#mobileOtp").val("");
            $(".otperror").html("");
            $(".otperror").hide();
            document.getElementById('editTractorDetails').style.display = "block";
        });
        $(document).on("click", "#checkEvaluate", function(e) {

            var mobileNumber = $("#mobileNumber").val();
            var mobileOtp = $("#mobileOtp").val();

            var validateMobNum = /^\d*(?:\.\d{1,2})?$/;
            if (validateMobNum.test(mobileOtp) && mobileOtp.length == 4) {
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('checkotp') }}",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    data: {
                        "mobileNumber": mobileNumber,
                        "mobileOtp": mobileOtp,

                    },
                    dataType: "json",
                    success: function(response) {
                        //alert(response);
                        if (response == "succcess") {
                            $("#userMobile").val(mobileNumber);
                            //alert(mobileNumber);
                            $("#finalSubmitbtn").click()
                            //$('form#myForm').submit();
                        } else {
                            // alert("Wrong OTP");
                            $(".otperror").html("Invalid OTP");
                            $(".otperror").show();
                        }
                    }
                });
            } else {
                $(".otperror").html("Please Insert Valid OTP");
                $(".otperror").show();
            }

        });
    </script>
@endsection
