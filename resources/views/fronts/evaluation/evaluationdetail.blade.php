@extends('fronts.include.master')

@section('content')
    <div class="pt-0 mb-3">

        <div class="container">
            <div class="tractor-detail-inner">
                <div class="mt-1 mb-1 evaluations">
                    {{-- <h3 class="pt-2 mb-1">Evaluation </h3> --}}
                    <h3 class="pt-2 mb-1">Rcommended price </h3>
                    <img src="{{ asset('public/front/images/icons/recommended-price.svg') }}" class="img-fluid"
                        alt="Rcommended price">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-6 text-center est-price">
                            <div class="estimated-pri pt-2">
                                <p>Fair condition</p>
                                <i class="bi bi-currency-rupee"></i> 6.16 Lakh
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-6 text-center est-price">
                            <div class="estimated-pri pt-2">
                                <p>Good condition</p>
                                <i class="bi bi-currency-rupee"></i> 7.16 Lakh
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <div class="mb-0 mt-0  p-3 pt-2 pb-5 text-center getupto">
            <h5 class="pt-2 mb-2">Get Loan upto <i class="bi bi-currency-rupee"></i>2,50,000 </h5>
            <a href="{{ route('getloan') }}" class="btn apply-btn mt-1 shadow-sm">Apply Now</a>
        </div>
        <div class="step-app" id="">

            <div class="pt-0">
                <div class="tractor-bg tractor-details-bx mt-0  pt-2 pb-2 mb-2">
                    <div class="container p-3">
                        <h4 class="mb-2 mt-0">Tractor details</h4>
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Brand
                                    <h5>{{ $userlead->brand_title }}</h5>
                                </div>
                            </div>


                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Model <h5>{{ $userlead->models_title }} </h5>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-12 tractor-det">
                                <div class="evalation-row">
                                    Location <h5>
                                        {{ $userlead->state_title }}
                                        <?php if($userlead->district!=""){?>
                                        ,{{ $userlead->district }}
                                        <?php } ?>
                                        <?php if($userlead->tahshil!=""){?>
                                        ,{{ $userlead->tahshil }}
                                        <?php } ?>

                                    </h5>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Year <h5>{{ $userlead->year }} </h5>
                                </div>
                            </div>



                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Ownership <h5>{{ $userlead->owners_title }}</h5>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Hours driven <h5>{{ $userlead->hours_title }}</h5>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Engine <h5>{{ $userlead->engine }}</h5>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-6 tractor-det">
                                <div class="evalation-row">
                                    Tyres <h5>{{ $userlead->tyre }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
