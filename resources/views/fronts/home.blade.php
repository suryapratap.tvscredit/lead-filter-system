@extends('fronts.include.master')

@section('content')
    <!-- evaluation section-->
    <div class="evaluate-section pt-4 pb-2 p-2 pb-4">
        <div class="container">

            <div class="row text-light">
                <h1>Evaluate Your Tractor</h1>

                <div class="col-lg-4 col-md-6 col-6 g-2">
                    <div class="evaluation-bx bg-white border border-gold rounded-3 text-left p-3">
                        <a href="{{ route('evaluation') }}">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <img src="{{ asset('public/front/images/icons/online-evaluation.webp') }}"
                                        class="img-fluid evaluation-icon" alt="Online Evaluation">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                    <h2>Online Evaluation</h2>
                                    <p>Just follow some steps, and done! </p>
                                    Evaluate Now <img src="{{ asset('public/front/images/icons/right-arrow.svg') }}"
                                        class="img-fluid" alt="Online Evaluation">

                                </div>
                            </div>
                        </a>

                    </div>
                </div>


                <div class="col-lg-4 col-md-6 col-6 g-2">
                    <div class="evaluation-bx bg-white border border-gold rounded-3 text-left p-3">
                        <a href="{{ route('freedoorstepevaluation') }}">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <img src="{{ asset('public/front/images/icons/free-doorstep.webp') }}"
                                        class="img-fluid evaluation-icon" alt="Free Doorstep Evaluation">
                                </div>
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                    <h2>Free Doorstep Evaluation</h2>
                                    <p>From the comfort of your home</p>
                                    Schedule Now <img src="{{ asset('public/front/images/icons/right-arrow.svg') }}"
                                        class="img-fluid" alt="Schedule Now">

                                </div>
                            </div>
                        </a>


                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-12 g-2">
                    <div class="evaluation-bx bg-white border border-gold rounded-3 text-left p-3">
                        <a href="{{ route('personalloanfrount') }}">
                            <div class="row">
                                <div class="col-sm-8 col-md-8 col-8">
                                    <h2>Personal Loan</h2>
                                    <p>Get loan within 24 Hours</p>
                                    Apply Now <img src="{{ asset('public/front/images/icons/right-arrow.svg') }}"
                                        class="img-fluid" alt="Apply Now ">
                                </div>
                                <div class="col-sm-4 col-md-4 col-4 float-start">
                                    <img src="{{ asset('public/front/images/icons/personal-loan.webp') }}"
                                        class="img-fluid evaluation-icon" alt="Personal Loan">
                                </div>
                            </div>
                        </a>

                    </div>
                </div>

                {{-- <div class="text-center">
                    <a href="" class="btn knowmore-btn mt-3 p-2">Know more</a>
                </div> --}}
            </div>

        </div>
    </div>
    </div>



    <!-- end evaluation section-->


    <div class="container pt-4 pb-3  pb-4">
        <div class="emi">
            <h2>EMI Calculator </h2>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <form class="needs-validation" novalidate>
                        <div class="row form-row">
                            <div class="col-lg-4 col-md-6 col-sm-12 mb-3">
                                <label for="validationTooltipUsername">Loan Amount</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text border-end-0 shadow-sm" id="LoanAmount"> ₹ </span>
                                    </div>
                                    <!-- <input type="number" class="form-control border-start-0 shadow-sm" id="LoanAmount" placeholder="1,00,000" aria-describedby=""> -->
                                    <input id="amount" type="number" class="form-control border-start-0 shadow-sm"
                                        placeholder="1,00,000" value="300000" min="0" max="200" required />

                                </div>
                            </div>


                            <div class="col-md-4 mb-3">
                                <label for="validationTooltipUsername">Interest Rate</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text border-end-0 shadow-sm" id="LoanAmount"> % </span>
                                    </div>
                                    <!-- <input type="number" class="form-control border-start-0 shadow-sm" id="panNo" placeholder="12" aria-describedby="" required> -->
                                    <input id="rate" type="number" class="form-control  border-start-0 shadow-sm"
                                        placeholder="12" value="12" required>

                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12 mb-3">
                                <label for="validationTooltipUsername">Loan Tenure</label>
                                <div class="input-group">

                                    <div class="input-group-prepend">
                                        <span class="input-group-text border-end-0 shadow-sm" id="LoanAmount"> Months
                                        </span>
                                    </div>
                                    <input id="months" type="text" class="form-control border-start-0 shadow-sm"
                                        value="36" placeholder="12">

                                </div>
                            </div>

                        </div>


                        <button type="button" class="btn checkbtn knowmore-btn mt-1 p-2 shadow-sm"
                            id="checkemibtn">Check</button>



                    </form>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12" id="checkEmi" style="display:none">
                    <div class="calculated-emi p-3 rounded-3 mt-4">
                        <div class="row form-row">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-3">
                                <p class="mb-0">Monthly Loan EMI</p>
                                <div class="Emi-value" id="monthly-payment"><i class="bi bi-currency-rupee"></i> 7,891
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-3">
                                <p class="mb-0">Total Interest Payable</p>
                                <div class="Emi-value" id="total-interest"><i class="bi bi-currency-rupee"></i> 3,00,000
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 mb-3">
                                <p class="mb-0">Total Payment</p>
                                <div class="Emi-value" id="total-payment"><i class="bi bi-currency-rupee"></i> 78,783
                                </div>
                            </div>



                        </div>
                    </div>
                    {{-- <div class="text-end emi-knowmore p-3 pt-2 ">
                        <a href="{{ route('emicalculater') }}">Know more</a>
                    </div> --}}

                </div>

            </div>
        </div>
    </div>


    <div class="whytractorworks">
        <div class="container pt-4 pb-4 why-tr">
            <h2>Why TractorPaisa?</h2>

            <div class="row">
                <div class="col-lg-3 col-md-4 col-6 g-3">
                    <div class="bg-white border rounded-2 text-left p-3 shadow whybx">
                        <h3>Over 5 million</h3>
                        <p>customers trust us & have bought their insurance on TractorPaisa </p>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-6 g-3">
                    <div class="bg-white border  rounded-2 text-left p-3 shadow whybx">
                        <h3>The Best Price</h3>
                        <p>for all insurance plans available online</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4 col-6 g-3">
                    <div class="bg-white border rounded-2 text-left p-3 shadow whybx">
                        <h3>50+ insurers</h3>
                        <p>partnered with us so that you can compare easily & transparently</p>
                    </div>
                </div>


                <div class="col-lg-3 col-md-4 col-6 g-3">
                    <div class="bg-white border  rounded-2 text-left p-3 shadow whybx">
                        <h3>Claims</h3>
                        <p>support built in with every policy for help, when you need it the most</p>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- end how tractor work-->

    <!-- what our customer say-->
    <div class="how-tractor-work">
        <div class="container pt-4 pb-2">
            <h2>Testimonials</h2>
            <div class="important-sec home-imgprtnt bb12">
                <div class="picup-add pd-15">
                    <div class="rc-rcbrand-container">
                        <div class="rc-rcbrand-inner">
                            <ul id="testimonail" class="rc-rcbrand-ul" style="left: 0px;">
                                <li class="testi rc-rcbrand-item">
                                    <div class="testimonial-bx bg-white">
                                        <i class="bi bi-quote quote-icon"></i>
                                        <p> I am happy and thankful for the support I have received from you people. Without
                                            you guys I can’t imagine such a fast & smooth process for my personal loan
                                            application.</p>
                                        <div class="testi-name text-end">Amit Kumar</div>
                                    </div>
                                </li>

                                <li class="testi rc-rcbrand-item">
                                    <div class="testimonial-bx bg-white">
                                        <i class="bi bi-quote quote-icon"></i>
                                        <p> I’d Like to thank you for all the efforts and leads you took to get in touch
                                            with me and having the entire procedure run through seemlessly. Thank You for
                                            all your support.</p>
                                        <div class="testi-name text-end">Devender Singh</div>
                                    </div>
                                </li>
                                <li class="testi rc-rcbrand-item">
                                    <div class="testimonial-bx bg-white">
                                        <i class="bi bi-quote quote-icon"></i>
                                        <p class="text-center">Happy and thankful for the support I have received from
                                            you people. Without
                                            you guys I can’t imagine such a fast & smooth process for my personal loan
                                            application. A Big Thank you for all the services.</p>
                                        <div class="testi-name text-end">Swati Singh</div>
                                    </div>
                                </li>
                            </ul>
                            <div class="rc-rcbrand-nav-left"></div>
                            <div class="rc-rcbrand-nav-right"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- end what our customer say-->

    <!-- what our customer say-->
    <div class="checkcibilscore">
        <div class="container pt-5 pb-5 pb-3">
            <div class="cibil-bx">
                <h2>Check CIBIL Score</h2>

                <form class="form-inline">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <div class="form-group mx-sm-3 mb-2 cibilscore-bx mt-2 shadow">
                                <input type="text" class="form-control form-control-lg" id="cibilScore"
                                    placeholder="Enter your PAN Number">
                                <button class="btn btn-cibil cibilscorebtn" type="button">Check</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
    <!-- end what our customer say-->


    <!-- what our customer say-->
    <div class="questions">
        <div class="container pt-4 pb-3">
            <h2 class="mb-3">Have a question? Here to help.</h2>

            <div class="accordion accordion-flush" id="accordionFlushExample">
                <?php $show = 0; ?>
                @foreach ($faqs as $cat)
                    <?php
                    if ($show == 0) {
                        $show = 'show';
                    } else {
                        $show = '';
                    }
                    ?>
                    <div class="accordion-item mb-2">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed rounded-sm" type="button"
                                data-bs-toggle="collapse" data-bs-target="#flush-collapse{{ $cat->id }}"
                                aria-expanded="false" aria-controls="flush-collapseOne">
                                {!! $cat->question !!}
                            </button>
                        </h2>
                        <div id="flush-collapse{{ $cat->id }}"
                            class="accordion-collapse collapse  <?php echo $show; ?>" aria-labelledby="flush-headingOne"
                            data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body pt-0">{!! $cat->answer !!} </div>
                        </div>
                    </div>
                    <?php $show = 1; ?>
                @endforeach
            </div>
        </div>
        <!-- end what our customer say-->



        <script src="{{ asset('public/front/js/jquery.carousel.js') }}"></script>
        <script>
            $(window).load(function() {

                $("#testimonail").rcbrand({
                    visibleItems: 2,
                    itemsToScroll: 1,
                    animationSpeed: 500,
                    loop: true,
                    navigationTargetSelector: null,
                    autoPlay: {
                        enable: true,
                        interval: 5000,
                        pauseOnHover: true,
                    },
                    responsiveBreakpoints: {
                        portrait: {
                            loop: true,
                            changePoint: 480,
                            visibleItems: 1,
                            itemsToScroll: 1
                        },
                        landscape: {
                            loop: true,
                            changePoint: 640,
                            visibleItems: 1,
                            itemsToScroll: 1
                        },
                        tablet: {
                            loop: true,
                            changePoint: 768,
                            visibleItems: 1,
                            itemsToScroll: 1
                        }
                    }
                });

                $("#howtractorpaisaworks").rcbrand({
                    visibleItems: 1,
                    itemsToScroll: 1,
                    animationSpeed: 200,
                    navigationTargetSelector: null,
                    autoPlay: {
                        enable: false,
                        interval: 5000,
                        pauseOnHover: true
                    },
                    responsiveBreakpoints: {
                        portrait: {
                            changePoint: 480,
                            visibleItems: 1,
                            itemsToScroll: 1
                        },
                        landscape: {
                            changePoint: 640,
                            visibleItems: 1,
                            itemsToScroll: 1
                        },
                        tablet: {
                            changePoint: 768,
                            visibleItems: 1,
                            itemsToScroll: 1
                        }
                    }
                });
                //callbackNextClick();
                //callbackPrevious();

            });
        </script>
        <script>
            previousValue = 0;
            $(function() {

                calculateEMI()

                $(window).keydown(function(event) {
                    if (event.keyCode == 13) { // enter key
                        event.preventDefault()
                        return false
                    } else if ([112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123].includes(event
                            .keyCode)) { // function Keys
                        return true
                    } else if ([8, 9, 27, 37, 38, 39, 40, 127].includes(event
                            .keyCode)) { // backspace, tab, escape, left, up, right, bottom key, delete
                        return true
                    } else if (event.ctrlKey || event.altKey || event.metaKey) {
                        return true
                    }

                    if (![48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105]
                        .includes(event.keyCode)) { // number range
                        event.preventDefault()
                        return false
                    }
                });
            });

            formatToCurrency = (amount) => {
                return new Intl.NumberFormat('en-IN').format(amount)
            }
            $("#checkemibtn").on("click", function() {
                $("#checkEmi").hide();
                calculateEMI()
                $("#checkEmi").show();
            })

            calculateEMI = () => {
                var totalAmount = document.getElementById("amount").value
                var InterestRate = document.getElementById("rate").value
                var LoanTenure = document.getElementById("months").value

                if ([0, ''].includes(totalAmount) || [0, ''].includes(InterestRate) || [0, ''].includes(LoanTenure)) {
                    $.notify({
                        message: "Please Fill Details"
                    }, {
                        type: "danger",
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        animate: {
                            enter: 'animated bounceInDown',
                            exit: 'animated bounceOutUp'
                        },
                        offset: {
                            x: 0,
                            y: 50
                        }
                    });
                    return false
                }

                var principal = parseFloat(totalAmount);
                var CalculateInterest = parseFloat(InterestRate) / 100 / 12;
                var calculatedPayments = parseFloat(LoanTenure);

                var x = Math.pow(1 + CalculateInterest, calculatedPayments);
                var monthly = (principal * x * CalculateInterest) / (x - 1);
                monthlyPayment = monthly.toFixed(2);

                totalInterest = (monthly * calculatedPayments - principal).toFixed(2);

                totalPayment = (monthly * calculatedPayments).toFixed(2);

                document.getElementById("monthly-payment").innerHTML = "Rs " + formatToCurrency(monthlyPayment)
                document.getElementById("total-interest").innerHTML = "Rs " + formatToCurrency(totalInterest)
                document.getElementById("total-payment").innerHTML = "Rs " + formatToCurrency(totalPayment)


            }
        </script>
    @endsection
