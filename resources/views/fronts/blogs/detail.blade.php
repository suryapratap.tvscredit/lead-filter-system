@extends('fronts.include.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="container pt-4 mb-0">
        <div class="common-section">
            <h1>{{ $blogs->title }}</h1>
            <p>{{ $blogs->shortdesc }}.</p>
            <div class="blog-date">Published - {{ date('j M Y', strtotime($blogs->blog_date)) }} <span class="seprator"> |
                </span>
                {{ date('g:i a', strtotime($blogs->blog_date)) }}<span class="seprator"> |
                </span> <a href="{{ route('categoryBlogs', $blogs->category_slug) }}"
                    class="categories-name">{{ $blogs->blogcategory }}</a></div>
            <div class="social-media pt-3 mt-3 text-right">
                <a class="twitter" href="#"><i class="bi bi-twitter"></i></a>
                <a class="youtube" href="#"><i class="bi bi-youtube"></i></a>
                <a class="facebook" href="#"><i class="bi bi-facebook"></i></a>
                <a class="whatsapp" href="#"><i class="bi bi-whatsapp"></i></a>
                <a class="linkedin" href="#"><i class="bi bi-linkedin"></i></a>
                <a class="instagram" href="#"><i class="bi bi-instagram"></i></a>
                <a class="telegram" href="#"><i class="bi bi-telegram"></i></a>
            </div>

        </div>




        <div class="pt-4">
            <div class="blog-inner-img mb-3">
                <?php if (isset($blogs->blog_image)) { ?>
                @if ($blogs->blog_image != '')
                    <?php
                    $file = explode('/', $blogs->blog_image);
                    $fileurl = $file['2']; ?>
                    <img src="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                        alt="{!! $blogs->title !!}" lass="img-fluid">
                @endif
                <?php } ?>

            </div>
            <p>{!! $blogs->longdesc !!}</p>
        </div>

        <div class="categories common-section pt-2 pb-4">
            @foreach ($blogcategory as $bc)
                <a href="{{ route('categoryBlogs', $bc->slug) }}">{{ $bc->blogcategory }}</a>
            @endforeach
        </div>


        <div class="nxt-prev-btn-container mb-5">
            <?php if($previous_record){?>
            <a href="{{ route('blogDetail', $previous_record->slug) }}" class="next-pre-btn float-start">Prev</a>
            <?php } ?>
            <?php if($next_record){?>
            <a href="{{ route('blogDetail', $next_record->slug) }}" class="next-pre-btn float-end">Next</a>
            <?php } ?>

        </div>


        <!-- blog listing box-->
        <div class="common-section">
            <h3>Similar Stories</h3>
            <div class="row">
                @foreach ($similer as $b)
                    <?php if ($b->id != $blogs->id){ ?>
                    <div class="blog-listing-bx mb-4 col-lg-4 col-md-6 col-12">
                        <a href="{{ route('blogDetail', $b->slug) }}">
                            <div class="blog-bx bg-white shadow-sm">
                                <div class="blog-img-bx">
                                    <?php if (isset($b->blog_image)) { ?>
                                    @if ($b->blog_image != '')
                                        <?php
                                        $file = explode('/', $b->blog_image);
                                        $fileurl = $file['2']; ?>
                                        <img src="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                            alt="{!! $b->title !!}">
                                    @endif
                                    <?php } ?>
                                </div>
                                <div class="blog-text p-3">
                                    <h2 class="blog-title">{{ $b->title }}
                                    </h2>
                                    <div class="blog-date">{{ date('j M Y', strtotime($b->blog_date)) }} <span
                                            class="seprator"> | </span> <a
                                            href="{{ route('categoryBlogs', $blogs->category_slug) }}"
                                            class="categories-name">{{ $b->blogcategory }}</a></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                @endforeach
            </div>
        </div>
        <!-- end blog listing box-->

        <!-- blog listing box-->
        <div class="common-section">
            <h3>Popular Stories</h3>
            <div class="row">
                @foreach ($populer as $b)
                    <div class="blog-listing-bx mb-4 col-lg-4 col-md-6 col-12">
                        <a href="{{ route('blogDetail', $b->slug) }}">
                            <div class="blog-bx bg-white shadow-sm">
                                <div class="blog-img-bx">
                                    <?php if (isset($b->blog_image)) { ?>
                                    @if ($b->blog_image != '')
                                        <?php
                                        $file = explode('/', $b->blog_image);
                                        $fileurl = $file['2']; ?>
                                        <img src="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                            alt="{!! $b->title !!}">
                                    @endif
                                    <?php } ?>
                                </div>
                                <div class="blog-text p-3">
                                    <h2 class="blog-title">{{ $b->title }}
                                    </h2>
                                    <div class="blog-date">{{ date('j M Y', strtotime($b->blog_date)) }} <span
                                            class="seprator"> | </span> <a
                                            href="{{ route('categoryBlogs', $blogs->category_slug) }}"
                                            class="categories-name">{{ $b->blogcategory }}</a></div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- end blog listing box-->
    </div>
@endsection
