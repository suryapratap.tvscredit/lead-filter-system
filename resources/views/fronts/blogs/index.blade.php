@extends('fronts.include.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="top-banner pt-4 p-3 pb-4">
        <div class="container">
            <h3>Get A Loan</h3>
            <p>Fulfill all your multiple needs with a single choice</p>
            <div class="get-loan-btn pt-4">
                <a href="" class="mt-2">Apply Now</a>
            </div>
        </div>
    </div>

    <div class="container pt-4 mb-0">
        <div class="common-section">
            <h1>Knowledge centre</h1>
            <p>The official blog of TractorPaisa featuring the latest developments in the agriculture world as well as
                helpful articles to help you navigate the agriculture market.</p>
        </div>


        <div class="categories common-section pt-4 pb-4">
            <h3>Browse Categories</h3>
            @foreach ($blogcategory as $bc)
                <a href="{{ route('categoryBlogs', $bc->slug) }}">{{ $bc->blogcategory }}</a>
            @endforeach
        </div>


        <!-- blog listing box-->
        <div class="row">
            @foreach ($blogs as $b)
                <div class="blog-listing-bx mb-4 col-lg-4 col-md-6 col-12">
                    <a href="{{ route('blogDetail', $b->slug) }}">
                        <div class="blog-bx bg-white shadow-sm">
                            <div class="blog-img-bx">
                                <?php if (isset($b->blog_image)) { ?>
                                @if ($b->blog_image != '')
                                    <?php
                                    $file = explode('/', $b->blog_image);
                                    $fileurl = $file['2']; ?>
                                    <img src="{{ env('APP_URL') }}/storage/app/public/upload/<?php echo $fileurl; ?>"
                                        alt="{!! $b->title !!}">
                                @endif
                                <?php } ?>

                            </div>
                            <div class="blog-text p-3">
                                <h2 class="blog-title">{{ $b->title }}</h2>
                                <div class="blog-date">{{ date('j M Y', strtotime($b->blog_date)) }} <span class="seprator">
                                        | </span> <a href="{{ route('categoryBlogs', $b->category_slug) }}"
                                        class="categories-name">{{ $b->blogcategory }}</a></div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach






        </div>

    </div>

    <div class="text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                {!! $blogs->links() !!}
                {{-- <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li> --}}
            </ul>
        </nav>
    </div>
@endsection
