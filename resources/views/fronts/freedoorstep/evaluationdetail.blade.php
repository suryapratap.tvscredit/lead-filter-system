@extends('fronts.include.master')

@section('content')
<div class="container pt-3 mb-3">

    <div class="step-app" id="">
        <div class="container">
            <div class="row pt-0">
                <div class="lightblue-bg shadow-sm tractor-details-bx rounded-3 mt-4 p-3 pt-4 pb-4 mb-4">
                    <h4 class="mb-2">Tractor details</h4>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Brand
                                <h5>{{$userlead->brand_title}}</h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Series <h5>{{$userlead->models_title}} </h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Year <h5>{{$userlead->year}} </h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Location <h5>{{$userlead->state_title}},{{$userlead->district}},{{$userlead->tahshil}}</h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Ownership <h5>{{$userlead->owners_title}}</h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Hours driven <h5>{{$userlead->hours_title}}</h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Engine <h5>{{$userlead->engine}}</h5>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 col-6 tractor-det">
                            <div class="evalation-row">
                                Tyres <h5>{{$userlead->tyre}}</h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="tractor-detail-inner">

        <div class="mt-1 mb-1 evaluations">
            <h3 class="pt-2 mb-1">Evaluation </h3>
            <p>Rcommended price</p>
            <img src="{{asset('public/front/images/icons/recommended-price.svg')}}" class="img-fluid" alt="Rcommended price">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-6 text-center est-price">
                    <div class="estimated-pri pt-2">
                        <p>Fair condition</p>
                        <i class="bi bi-currency-rupee"></i> 6.16 Lakh
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-6 text-center est-price">
                    <div class="estimated-pri pt-2">
                        <p>Good condition</p>
                        <i class="bi bi-currency-rupee"></i> 7.16 Lakh
                    </div>
                </div>

            </div>
        </div>

        <div class="mb-4 aooluloan-bx rounded-3 mt-4 p-2 pt-2 pb-3 shadow-lg text-center">
            <h5 class="pt-2 mb-2">Get Loan upto <i class="bi bi-currency-rupee"></i>2,50,000 </h5>
            <a href="{{route('getloan')}}" class="btn apply-btn mt-1 shadow-sm">Apply Now</a>
        </div>
    </div>
    @endsection