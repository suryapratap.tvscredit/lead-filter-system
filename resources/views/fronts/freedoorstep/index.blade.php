@extends('fronts.include.master')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <div class="container pt-4 mb-5">
        <div class="laon-form step-content">
            <h3 class="pt-2 mb-2"> Free Doorstep Evaluation </h3>
            <form id="finalSubmit" name="finalSubmit" enctype="multipart/form-data" method="POSt"
                action="{{ route('SubmitDetailFreeDoorstep') }}">
                @csrf
                <div class="step1" id="step1">
                    <div class="form-group pt-2">
                        <label class="mt-1">Full Name<span>*</span></label>
                        <input type="text" class="form-control " id="fullName" aria-describedby="emailHelp"
                            placeholder="Enter your Name" name="fullname" required>
                        <div class="error-msg nameerror" style="display:none"> Please Enter Full Name</div>
                    </div>

                    <div class="form-group pt-2">
                        <label class="mt-1">PAN Number</label>
                        <input type="text" class="form-control " id="panNumber" aria-describedby="emailHelp"
                            placeholder="Enter your pan Number" name="pan_number">
                        <div class="error-msg panNumbererror" style="display:none"> </div>
                    </div>

                    <div class="form-group pt-2">
                        <label for="State">State <span>*</span></label>
                        <select class="form-control " name="state_id" id="state_id" required>
                            <option value=""> Select State</option>
                            <?php foreach ($states as $b) { ?>
                            <option value="{{ $b->id }}">{{ $b->name }}</option>
                            <?php } ?>
                        </select>
                        <div class="error-msg stateerror" style="display:none"> Please Select State</div>
                    </div>

                    <div class="form-group pt-2">
                        <label for="State">District <span></span></label>
                        <select class="form-control " name="district_id" id="district_id">
                            <option value=""> Select District</option>
                        </select>
                        <div class="error-msg districterror" style="display:none"> Please Select District</div>
                    </div>

                    <div class="form-group pt-2">
                        <label for="State">Tehsil <span></span></label>
                        <select class="form-control " name="tahshil_id" id="tahshil_id">
                            <option value=""> Select Tehsil</option>
                        </select>
                        <div class="error-msg tahshilerror" style="display:none"> Please Select Tehsil</div>
                    </div>

                    <div class="form-group pt-2">
                        <label class="mt-1">Mobile No<span>*</span></label>
                        <input type="text" class="form-control Number " id="mobileNumber" placeholder="Enter Mobile No"
                            name="mobileNumber" maxlength="10">
                        <div class="error-msg mobileerror" style="display:none"> </div>
                    </div>
                </div>
                <div class="step2" id="step2" style="display: none;">
                    <div class="mt-2 mb-2">
                        <div class="otpsend mt-2 mb-3">
                            OTP has been sent to <span id="mNumber"></span><a href="javascript:void(0)"
                                onclick="backOTP()" id="changeMobile">Edit</a>
                        </div>

                        <input type="text" maxlength="4" id="mobileOtp" class="form-control Number"
                            placeholder="Enter the OTP">
                        <div class="error-msg otperror" style="display:none"> </div>
                        <div class="text-end otpsend mt-2">Didn’t receive? please wait for 52 sec...</br>
                            <a href="javascript:void(0)">Resend OTP</a>
                        </div>
                        <input type="button" id="checkEvaluate" value="Submit"
                            class="btn checkbtn knowmore-btn mt-4 p-2" />
                    </div>
                </div>
                <button type="button" id="getOTP" class="btn checkbtn knowmore-btn mt-4 p-2">Get OTP </button>
                <button type="submit" id="submitfrm" class="btn checkbtn knowmore-btn mt-4 p-2"
                    style="display: none;">Submit </button>

            </form>
        </div>
    </div>



    <script src="{{ asset('public/front/js/jquery-steps.js') }}"></script>
    <script type="text/javascript">
        $('#fullName').keyup(function() {
            if ($("#fullName").val() != "") {
                $(".nameerror").hide();
            }
        });
        $('#panNumber').keyup(function() {
            if ($("#panNumber").val() != "") {
                $(".panNumbererror").hide();
            }
        });
        $('#mobileNumber').keyup(function() {
            if ($("#mobileNumber").val() != "") {
                $(".mobileerror").hide();
            }
        });
        $('.Number').keypress(function(e) {
            var arr = [];
            var kk = e.which;

            for (i = 48; i < 58; i++)
                arr.push(i);

            if (!(arr.indexOf(kk) >= 0))
                e.preventDefault();
        });
        $('#demo').steps({
            onFinish: function() {
                alert('Wizard Completed');
            }
        });

        $(function() {
            $(".inner-btn").click(function() {
                $(this).addClass("active-step");
            });
        });

        // $(document).ready(function(){
        //   $('.step-footer').on('click', function(){
        //     $(this).addClass('active-step');
        //     $('.step-footer').not(this).removeClass('active-step');
        //   });
        // });

        $(".search-icn").click(function() {
            $(".search-input").toggleClass("active").focus;
            $(this).toggleClass("animate");
            $(".search-input").val("");
        });

        function nextOTP() {
            document.getElementById('step1').style.display = "none";
            document.getElementById('step2').style.display = "block";
            document.getElementById('getOTP').style.display = "none";
        }

        function backOTP() {
            document.getElementById('step1').style.display = "block";
            document.getElementById('step2').style.display = "none";
            document.getElementById('getOTP').style.display = "block";
        }


        $("#getOTP").on('click', function() {
            var error = 0;
            if ($("#fullName").val() == "") {
                error = 1;
                $(".nameerror").show();
                return false
            } else {
                $(".nameerror").hide();
            }

            if ($("#panNumber").val() != "") {
                var inputvalues = $("#panNumber").val();
                var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
                if (!regex.test(inputvalues)) {
                    // $("#panNumber").val("");
                    $(".panNumbererror").html("invalid PAN no");
                    $(".panNumbererror").show();
                    return false
                    error = 0;
                    //return regex.test(inputvalues);
                } else {
                    // alert("done")
                    $(".panNumbererror").hide();
                }
            }
            if ($("#state_id").val() == "") {
                //alert("Please select State");
                error = 1;
                $(".stateerror").show();
                return false
            } else {
                $(".stateerror").hide();
            }
            // if ($("#district_id").val() == "") {
            //     //alert("Please select district");
            //     error = 1;
            //     $(".districterror").show();
            //     return false
            // } else {
            //     $(".districterror").hide();
            // }
            // if ($("#tahshil_id").val() == "") {
            //     ////alert("Please select tahshil");
            //     error = 1;
            //     $(".tahshilerror").show();
            //     return false
            // } else {
            //     $(".tahshilerror").hide();
            // }
            var mobileNumber = $("#mobileNumber").val();
            var phone_pattern = /^(0|91)?[6-9][0-9]{9}$/;
            if (!phone_pattern.test(mobileNumber)) {
                $(".mobileerror").html("Invalid Mobile Number");
                $(".mobileerror").show();
                return false
            } else {
                $(".mobileerror").hide();
            }
            var validateMobNum = /^\d*(?:\.\d{1,2})?$/;
            if (validateMobNum.test(mobileNumber) && mobileNumber.length == 10) {
                error = 0;
            } else {
                // //alert("Please Enter Mobile number");
                $(".mobileerror").html("Please Enter valid Mobile number");
                $(".mobileerror").show();
                error = 1;
                return false
            }


            if (error == 0) {
                $.ajax({
                    url: "{{ route('submitmobileFreestep') }}",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    data: {
                        "mobileNumber": mobileNumber,

                    },
                    dataType: "json",
                    success: function(response) {
                        nextOTP();
                        $("#mNumber").html(mobileNumber);
                        //$("#checkEvaluate").show();
                    }
                });

            } else {
                return false
            }
        });
    </script>

    <script>
        $(document).ready(function() {
            $('#state_id').on('change', function() {
                $(".stateerror").hide();
                var state_id = $(this).val();
                var stateDetails = $("#state_id option:selected").text();

                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('frontgetStateDistricts') }}",
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    data: {
                        "state_id": state_id,

                    },
                    dataType: "json",
                    success: function(response) {
                        $('#district_id').html(response);
                        $("#fstate_id").val(state_id)
                        $("#stateDetails").html(stateDetails)
                        $('#tahshil_id').html("<option value=''> Select Tehsil</option>");

                    }
                });
            });
            $('#district_id').on('change', function() {
                //$('#allteam').hide(300);
                var district_id = $(this).val();
                var districtDetails = $("#district_id option:selected").text();

                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('frontgetDistrictsTahshil') }}",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    data: {
                        "district_id": district_id,

                    },
                    dataType: "json",
                    success: function(response) {
                        $('#tahshil_id').html(response);
                        $("#fdistrict_id").val(district_id)
                        $("#districtDetails").html(districtDetails)
                    }
                });
            });
        });



        $('#mobileOtp').keyup(function() {
            if ($("#mobileOtp").val() != "") {
                $(".otperror").hide();
            }
        });
        $(document).on("click", "#changeMobile", function(e) {
            $("#evaluate").hide();
            //$("#checkEvaluate").hide();
            $("#mobileNumber").show();
            $("#sendOtp").show();
            //$("#mobileNumber").val("");
            $("#mNumber").html("");
            $("#mobileOtp").val("");
            $(".otperror").html("");
            $(".otperror").hide();
        });
        $(document).on("click", "#checkEvaluate", function(e) {

            var mobileNumber = $("#mobileNumber").val();
            var mobileOtp = $("#mobileOtp").val();

            var validateMobNum = /^\d*(?:\.\d{1,2})?$/;
            if (validateMobNum.test(mobileOtp) && mobileOtp.length == 4) {
                var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
                $.ajax({
                    url: "{{ route('checkotpFreedoorstep') }}",
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    data: {
                        "mobileNumber": mobileNumber,
                        "mobileOtp": mobileOtp,

                    },
                    dataType: "json",
                    success: function(response) {
                        //alert(response);
                        if (response == "succcess") {

                            $("#submitfrm").click()
                            //$('form#myForm').submit();
                        } else {
                            $(".otperror").html("Invalid OTP");
                            $(".otperror").show();
                        }
                    }
                });
            } else {
                $(".otperror").html("Please Insert OTP");
                $(".otperror").show();
            }

        });
    </script>
@endsection
