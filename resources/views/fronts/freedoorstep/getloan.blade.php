@extends('fronts.include.master')

@section('content')
    <div class="container pt-4 mb-5">
        <div class="laon-form step-content">
            <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
            <h3 class="pt-2 mb-3"> Get Loan </h3>
            <div class="shadow-sm rounded-4 p-3 border">
                <form enctype="multipart/form-data" method="POSt" id="myform" action="{{ route('evaluationloansubmit') }}">
                    @csrf
                    <label class="mt-1">Full Name<span>*</span></label>
                    <input type="hidden" name="userlead_id" value="{{ $userlead->id }}">
                    <input type="text" max=" " class="form-control mt-2 shadow-sm" id="fullName"
                        aria-describedby="emailHelp" placeholder="Enter your Name" name="username">

                    <label class="mt-3">PAN Number</label>
                    <input type="text" max=" " class="form-control mt-2 shadow-sm" id="panNumber"
                        aria-describedby="emailHelp" placeholder="Enter your PAN Number" name="pan_number">

                    <button type="submit" id="loanSubmit" class="btn checkbtn knowmore-btn mt-4 p-3">Submit </button>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
            debug: true,
            success: "valid"
        });
        $("#myform").validate({
            rules: {
                fullName: {
                    required: true,
                    max: 1
                }
            }
        });
    </script>
@endsection
