@extends('fronts.include.master')

@section('content')
    <div class="container pt-5 mb-5">
        <div class="laon-form step-content">
            <div class="alert thanksmsg text-center p-4" role="alert">
                <h2 class="alert-heading">Thank you!</h2>
                <p>Your Evaluation appointment has been requested</p>
            </div>

            <a href="{{ route('home') }}" class="btn checkbtn knowmore-btn mt-3 p-2">Back to Home page </a>

        </div>
    </div>
@endsection
