@extends('fronts.include.master')

@section('content')
    <div class="container pt-4 mb-5">
        <div class="laon-form step-content">

            <h3 class="pt-2 mb-3"> Get Loan </h3>

            <form enctype="multipart/form-data" method="POSt" id="myform" action="{{ route('evaluationloansubmit') }}">
                @csrf
                <label class="mt-1">Full Name<span>*</span></label>
                <input type="hidden" name="userlead_id" value="{{ $userlead->id }}">
                <input type="text" class="form-control mt-2 AlphabetsOnly" id="fullName" placeholder="Enter your Name"
                    name="username">
                <div class="error-msg fullNameerror" style="display:none"> </div>
                <label class="mt-3">PAN Number</label>
                <input type="text" max=" " class="form-control mt-2" id="panNumber" aria-describedby="emailHelp"
                    placeholder="Enter your PAN Number" name="pan_number">
                <div class="error-msg panNumbererror" style="display:none"> </div>

                <button type="button" id="loanSubmit" class="btn checkbtn knowmore-btn mt-4 p-2">Submit </button>
            </form>

        </div>
    </div>

    <script>
        $("#loanSubmit").on("click", function() {
            if ($("#fullName").val() == "") {
                $(".fullNameerror").html("Please Enter Full Name");
                $(".fullNameerror").show();
                return false
            } else {
                $(".fullNameerror").hide();
            }

            if ($("#panNumber").val() != "") {
                var inputvalues = $("#panNumber").val();
                var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
                if (!regex.test(inputvalues)) {
                    //$("#panNumber").val("");
                    $(".panNumbererror").html("invalid PAN no");
                    $(".panNumbererror").show();
                    return false
                    //return regex.test(inputvalues);
                } else {
                    //alert("done")
                    $(".panNumbererror").hide();
                }
            }
            $("#myform").submit();
        })
        $('.AlphabetsOnly').keypress(function(e) {
            var regex = new RegExp(/^[a-zA-Z\s]+$/);
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            } else {
                e.preventDefault();
                return false;
            }
        });
    </script>
@endsection
