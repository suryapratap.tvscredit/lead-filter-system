@extends('layouts.app')

@section('content')

<body class="login">
    <div>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <div class="animate form login_form" style=" border: 1px solid #0057a9;padding: 21px;">
                    <section class="login_content">
                        <?php if (isset($msg)) { ?>
                            <div class="alert">
                                <?php echo $msg; ?>
                            </div>
                        <?php } ?>
                        <h2>
                            <div class="login-logo">
                                <!-- <img src="{{asset('public/images/logo.png')}}" alt="Logo" height="80px" width="160px" /> -->
                            </div>
                        </h2>
                        <br>


                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group">

                                <!-- <label for="exampleInputEmail1">Email address</label> -->
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter email" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert" style="color:red;">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="form-group">
                                <!-- <label for="exampleInputPassword1">Password</label> -->


                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="row offset-3" style="margin-top:35px">
                                <div class="col-8">

                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Login') }}
                                    </button>

                                </div>
                            </div>
                        </form>
                        <div class="clearfix"></div>

                    </section>
                </div>
            </div>
        </div>
    </div>
</body>

@endsection