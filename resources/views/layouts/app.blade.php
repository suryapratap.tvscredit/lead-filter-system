<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>USER | LOGIN</title>
    <!-- <title>{{ config('app.name', 'Laravel') }}</title> -->

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Bootstrap -->
    <link href="{{asset('public/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('public/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('public/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('public/vendors/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/select2.min.css')}}" rel="stylesheet" />

    <!-- Custom Theme Style -->
    <link href="{{asset('public/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/buttons.dataTables.min.css')}}">
    <link href="{{asset('public/build/css/custom.css')}}" rel="stylesheet">
    <script src="{{asset('public/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('public/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>


    <!-- Datatables -->
    <script src="{{asset('public/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('public/js/jszip.min.js')}}"></script>
    <script src="{{asset('public/js/buttons.html5.min.js')}}"></script>

    <!-- NProgress -->
    <script src="{{asset('public/vendors/nprogress/nprogress.js')}}"></script>
    <script src="{{asset('public/js/jquery.validate.min.js')}}"></script>
</head>


<div id="app">


    <main class="py-4">
        @yield('content')
    </main>
</div>
<!-- <script src="{{ asset('public/frontend/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/bootstrap5.bundle.js') }}"></script>
<script src="{{ asset('public/dist/js/adminlte.min.js') }}"></script> -->

<!-- gentela -->
<script src="{{ asset('public/vendors/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('public/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/vendors/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('public/vendors/nprogress/nprogress.js') }}"></script>
<script src="{{ asset('public/vendors/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('public/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('public/vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('public/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('public/vendors/pdfmake/build/vfs_fonts.js') }}"></script>



<!--  -->



</body>

</html>