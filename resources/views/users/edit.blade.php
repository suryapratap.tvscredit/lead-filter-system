@extends('admin.master')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h4>EDIT USER DETAILS </h4>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br />
                                        <form role="form" id="quickForm" action="{{route('users.update')}}" method="POST">
                                            <div class="card-body">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$users->id}}">
                                                <div class="form-group  col-md-6">
                                                    <label>Username</label>
                                                    <input type="text" class="form-control" id="name" name="name" value="{{ $users->name }}" placeholder="Enter email" required>
                                                </div>

                                                <div class="form-group  col-md-6">
                                                    <label>Email address</label>
                                                    <input type="email" class="form-control" name="email" id="email" value="{{ $users->email }}" placeholder="Enter email" required>
                                                </div>

                                                <div class="form-group  col-md-6">
                                                    <label>Password</label>
                                                    <input type="password" class="form-control" style="box-shadow: 0 0 5px rgb(139 41 68);" id="password" name="password" placeholder="Password (Leave Blank For old password )">
                                                </div>

                                                <div class="form-group  col-md-6">
                                                    <label>Category:</label>
                                                    <select class="form-control select2bs4" name="category" id="category" required>
                                                        <option value="" selected>Select Category</option>
                                                        @foreach($category as $proc)
                                                        <option value="{{$proc->id}}" {{($users->category_id==$proc->id) ? 'selected': ''}}> {{$proc->category}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group ">
                                                    <div class="controls col-md-6">
                                                        <label for="title">Process:</label><span class="err" id="err_title"></span>
                                                        <select class="form-control select2bs4" name="subcategory" id="subcategory" required>
                                                            <option value=""> Select Sub-category</option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <!-- /.card-body -->
                                                <button type="submit" class="btn btn-primary" style="margin-left: 40%;margin-top: 20px;">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </body>



    <script>
        function getSubprocess(category, subcategory) {
            var CSRF_TOKEN = document.querySelector('meta[name="csrf-token"]').getAttribute("content");
            $.ajax({
                url: "{{route('getSubprocess')}}",
                type: "POST",
                data: {
                    "category": category,
                    "subcategory": subcategory,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                success: function(response) {
                    $('#subcategory').html(response);
                }
            });
        }
        <?php if ($users->subcategory != "") { ?>
            getSubprocess(<?php echo $users->category ?>, <?php echo $users->subcategory ?>)
        <?php   } ?>

        $(document).ready(function() {
            var sub_id = 0;

            $('select[name="category"]').on('change', function() {
                var pro_id = $(this).val();
                getSubprocess(pro_id, 0)

            });
        });
    </script>
    @endsection