@extends('fronts.include.master')

@section('content')
    <div class="container pt-4 mb-5">
        <div class="not-found text-center">
            <!-- <img src="images/icons/404-icon.png" alt="404"> -->
            <h1>404</h1>
            <h5>OPPS! PAGE NOT FOUND </h5>
            <p>Sorry, the page you're looking for doesn't exits. if you think something is broken, report a problem.</p>
            <a href="{{ route('admin.dashboard') }}" class="btn checkbtn knowmore-btn mt-3 p-2">Back to Home page </a>
        </div>
    </div>
@endsection
